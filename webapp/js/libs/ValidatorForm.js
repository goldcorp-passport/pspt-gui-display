sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"sap/ui/base/Object",
	"com/gc/passportvisits/js/vendor/moment",
	"com/gc/passportvisits/js/libs/BaseUtility",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"com/gc/passportvisits/js/vendor/underscore"
], function(Object, momentjs, BaseUtility, MessageBox, MessageToast, underscore) {
	"use strict";
	return Object.extend("com.gc.passportvisits.js.libs.ValidatorForm", {
		constructor: function() {

		},

		specificValidation: function(_oController, _oValidations) {
			var oController = _oController;
			var oValidations = _oValidations;
			return new Promise(function(resolve, reject) {

				var uidData = oController.getView().byId(oValidations.item).getValue().toUpperCase();
				var oViewApp = oController.getView().getModel("appView");
				var oBaseUtility = new BaseUtility();
				var messageToast = oController.getResourceBundle().getText("messageFailFormatUidType");
				var showToast = true;
				var response;
				if (uidData.length !== 0) {
					if (oValidations.dependentFields.pathItem) {
						if (oController.getView().byId(oValidations.dependentFields.pathItem)[oValidations.dependentFields.prop]() ===
							0) {
							if (oBaseUtility.validateCurp(uidData)) {
								oViewApp.setProperty("/optionNal", "C");
								showToast = false;
							}
						} else {
							oViewApp.setProperty("/optionNal", "P");
							showToast = false;
						}
					}
				}
				if (showToast) {
					MessageToast.show(messageToast, {
						duration: 3000,
						width: "25%",
						animationTimingFunction: "ease-in",
						animationDuration: 800
					});
					reject("Failure to execute in ValidatorForm.js, may lack information.");
				} else {
					response = {
						"correct": true,
						"uidData": uidData,
						"optionNal": oViewApp.getProperty("/optionNal")
					};
					resolve(response);
				}
			});

		},
		validateFormProfile: function(oThis) {
			var oController = oThis;
			var baseUtility = new BaseUtility();
			var oViewApp = oController.getView().getModel("appView");
			var messageToast = oController.getResourceBundle().getText("messageFailValidation");
			var appVersion = oController.getResourceBundle().getText("appVersion");
			var nameSpaceValidation = "validatePolicyBasicInformation";

			// var evalueteItem = ;
			var validateFormComplete = {
				bool: true
			};

			if (appVersion === "1") {
				nameSpaceValidation = "validatePolicyBasicInformationVisits";
			}
			/* global _:true */
			_.each(oViewApp.getProperty("/" + nameSpaceValidation), function(value, key,
				list) {
				// console.log(value +" -- " + key +" -- "+ list);
				var objectValueItem;
				var objectItem;
				var oFormat = {
					bool: false
				};
				objectItem = oController.byId(value.item);
				// if (objectItem.isActive()) {
				if (value.type !== "Select") {
					if (value.type === "Option") {
						objectValueItem = objectItem.getSelectedIndex();
					} else {
						objectValueItem = objectItem.getValue();
						objectValueItem = objectValueItem.trim();

					}

				} else {
					objectValueItem = objectItem.getSelectedKey();
				}
				if (value.dependentFields) {
					if (oController.byId(value.dependentFields.pathItem)[value.dependentFields
							.prop]() !== value.dependentFields.value) {
						return;
					}
				}
				if (value.validateMaskFunction) {
					oFormat = baseUtility[value.validateMaskFunction](objectValueItem);
				}
				if (value.inputAcept) {
					oFormat = baseUtility["validate" + value.inputAcept](
						objectValueItem);
				}
				if (value.predefinedExpReg) {
					oFormat = baseUtility["validate" + value.inputAcept](
						objectValueItem, value.predefinedExpReg);
				}

				if (value.exceptValue) {
					if (value.exceptValue !== objectValueItem) {
						oFormat.bool = true;
					}
				}
				if (objectValueItem === "" || !oFormat.bool) {
					// objectItem.setValueState("Error");
					// if (value.type === "SegmentedButton") {
					// 	for (var i = 0; i < objectItem.getItems().length; i++) {
					// 		$("#" + objectItem.getItems()[i].oButton.getId()).css("border-color", "red");
					// 		// $("#" + objectItem.getItems()[2].oButton.getId()).css("border-color", "red");
					// 	}

					// } else {
					if (value.type !== "Select") {

						// } else {
						objectItem.setValueState("Error");
					} else {
						$("#" + objectItem.getId()).css("border-color", "red");
					}
					// }
					if (validateFormComplete.bool) {
						validateFormComplete.bool = false;
						// oController.getView().byId("detailIconTabBar").setSelectedKey(
						// "__component0---detail--" + value.tab);
						oController.getView().byId("detailIconTabBar").setSelectedKey(oController.byId(value.tab).getId());
						MessageToast.show(messageToast, {
							duration: 5000,
							// default
							width: "25%",
							// default
							animationTimingFunction: "ease-in",
							// default
							animationDuration: 800
						});
					}
				} else {

					if (value.type !== "Select") {

						// } else {
						objectItem.setValueState("None");
					} else {
						$("#" + objectItem.getId()).css("border-color", "rgb(191, 191, 191)");
					}
					// objectItem.setValueState("None");
				} // }
			});
			// console.log(validateFormComplete);
			return validateFormComplete.bool;
		},
		validateFormAccess: function(oThis) {
			var oController = oThis;
			var baseUtility = new BaseUtility();
			var oViewApp = oController.getView().getModel("appView");
			var messageToast = oController.getResourceBundle().getText("messageFailValidation");
			var appVersion = oController.getResourceBundle().getText("appVersion");
			// var evalueteItem = ;
			var validateFormComplete = {
				bool: true
			};
			var oEntityValidate = "validatePolicyAccessInformation";
			if (appVersion === "1") {
				if (oController.getModel("oModelVisaClone").getProperty("/Siteid") !== "PEN") {
					oEntityValidate = "validatePolicyAccessInformationVisitMxVan";
				} else {
					oEntityValidate = "validatePolicyAccessInformationVisit";
				}

			}
			/* global _:true */
			_.each(oViewApp.getProperty("/" + oEntityValidate), function(value, key,
				list) {
				// console.log(value +" -- " + key +" -- "+ list);
				var objectValueItem;
				var objectItem;
				var oFormat = {
					bool: false
				};
				objectItem = oController.byId(value.item);
				// if (objectItem.isActive()) {
				if (value.type !== "Select") {
					if (value.type === "Option") {
						objectValueItem = objectItem.getSelectedIndex();
					} else {
						if (value.type === "SegmentedButton") {
							objectValueItem = objectItem.getSelectedKey();
						} else {
							objectValueItem = objectItem.getValue();
							objectValueItem = objectValueItem.trim();

						}

					}

				} else {
					objectValueItem = objectItem.getSelectedKey();
				}
				if (value.dependentFields) {
					if (oController.byId(value.dependentFields.pathItem)[value.dependentFields
							.prop]() !== value.dependentFields.value) {
						return;
					}
				}
				if (value.validateMaskFunction) {
					oFormat = baseUtility[value.validateMaskFunction](objectValueItem);
				}
				if (value.inputAcept) {
					oFormat = baseUtility["validate" + value.inputAcept](
						objectValueItem);
				}
				if (value.predefinedExpReg) {
					oFormat = baseUtility["validate" + value.inputAcept](
						objectValueItem, value.predefinedExpReg);
				}

				if (value.exceptValue) {
					if (value.exceptValue !== objectValueItem) {
						oFormat.bool = true;
					}
				}
				if (objectValueItem === "" || !oFormat.bool) {
					// objectItem.setValueState("Error");
					if (value.type === "SegmentedButton") {
						for (var i = 0; i < objectItem.getItems().length; i++) {
							$("#" + objectItem.getItems()[i].oButton.getId()).css("border-color", "red");
							// $("#" + objectItem.getItems()[2].oButton.getId()).css("border-color", "red");
						}

					} else {
						if (value.type !== "Select") {

							// } else {
							objectItem.setValueState("Error");
						} else {
							$("#" + objectItem.getId()).css("border-color", "red");
						}
					}

					if (validateFormComplete.bool) {
						validateFormComplete.bool = false;
						// oController.getView().byId("detailIconTabBar").setSelectedKey(
						// "__component0---detail--" + value.tab);
						oController.getView().byId("detailIconTabBar").setSelectedKey(oController.byId(value.tab).getId());
						MessageToast.show(messageToast, {
							duration: 5000,
							// default
							width: "25%",
							// default
							animationTimingFunction: "ease-in",
							// default
							animationDuration: 800
						});
					}
				} else {

					if (value.type === "SegmentedButton") {
						// $("#" + objectItem.getItems()[0].oButton.getId()).css("border-color", "");
						// $("#" + objectItem.getItems()[2].oButton.getId()).css("border-color", "rgb(191, 191, 191)");
						for (var j = 0; j < objectItem.getItems().length; j++) {
							$("#" + objectItem.getItems()[j].oButton.getId()).css("border-color", "rgb(191, 191, 191)");
							// $("#" + objectItem.getItems()[2].oButton.getId()).css("border-color", "red");
						}
					} else {

						if (value.type !== "Select") {

							// } else {
							objectItem.setValueState("None");
						} else {
							$("#" + objectItem.getId()).css("border-color", "rgb(191, 191, 191)");
						}
						// objectItem.setValueState("None");
					}
					// objectItem.setValueState("None");

				} // }
			});
			// console.log(validateFormComplete);
			return validateFormComplete.bool;
		},
		validateFormProfileBulkData: function(oThis) {
			var oController = oThis;
			var baseUtility = new BaseUtility();
			var oViewApp = oController.getView().getModel("appView");
			var messageToast = oController.getResourceBundle().getText("messageFailValidation");
			// var appVersion = oController.getResourceBundle().getText("appVersion");
			// var _policy = (policy ? policy : "validatePolicyBasicInformation");
			var nameSpaceValidation = "validatePolicyBasicInformationBulkData";

			// var evalueteItem = ;
			var validateFormComplete = {
				bool: true
			};
			var lengthRows = oController.byId("tableBlulk").getRows().length;
			for (var i = 0; i < lengthRows; i++) {

				/* global _:true */
				_.each(oViewApp.getProperty("/" + nameSpaceValidation), function(value, key,
					list) {
					var objectValueItem;
					var objectItem;
					var oFormat = {
						bool: false
					};

					var column = value.item;

					var getTableColum = _.filter(oController.byId("tableBlulk").getRows()[i].getCells(), function(value, index) {
						if (value.getId().indexOf(column) !== -1) {
							return true;
						}
					}.bind(this));
					var getIndexTableColumn = _.indexOf(oController.byId("tableBlulk").getRows()[i].getCells(), getTableColum[0]);
					objectItem = oController.byId("tableBlulk").getRows()[i].getCells()[getIndexTableColumn];

					if (value.type !== "Select") {
						if (value.type === "Option") {
							objectValueItem = objectItem.getSelectedIndex();
						} else {
							objectValueItem = objectItem.getValue();
							objectValueItem = objectValueItem.trim();
						}

					} else {
						objectValueItem = objectItem.getSelectedKey();
					}

					// if (value.dependentFields) {
					// 	if (oController.byId(value.dependentFields.pathItem)._aTemplateClones[i][value.dependentFields.prop]() !==
					// 		value.dependentFields.value) {
					// 		return;
					// 	}
					// }

					if (value.dependentThanTwo) {

						var rowDependentThenTwo = value.dependentThanTwo.pathItem;

						var getDependentThanTwo = _.filter(oController.byId("tableBlulk").getRows()[i].getCells(), function(value,
							index) {
							if (value.getId().indexOf(rowDependentThenTwo) !== -1) {
								return true;
							}
						}.bind(this));

						var evalDependent = _.find(value.dependentThanTwo.value, function(element) {
							if (getDependentThanTwo[0][value.dependentThanTwo.prop]() === element) {
								return true;
							}
						}.bind(this));

						if (typeof evalDependent === "number") {
							oFormat.evalMask = value.dependentThanTwo.value.indexOf(evalDependent);
						} else {
							// if (objectItem.getValueState() === "Error") {
							// 	objectItem.setValueState("None");
							// }

							$("#" + objectItem.getId()).css("border-color", "rgb(191, 191, 191)");

							return;

						}
					}
					if (value.validateMaskFunction) {
						// _.find(value.validateMaskFunction, function(element) {
						var evalMask = oFormat.evalMask ? value.validateMaskFunction[oFormat.evalMask] : value.validateMaskFunction[
							0];
						oFormat = baseUtility[evalMask](objectValueItem);
						if (oFormat.bool) {

							if (value.type !== "Select") {
								objectItem.setValueState("None");
							} else {
								$("#" + objectItem.getId()).css("border-color", "rgb(191, 191, 191)");
							}
						} else {
							if (value.type !== "Select") {
								objectItem.setValueState("Error");
							} else {
								$("#" + objectItem.getId()).css("border-color", "red");
							}

							// return;
						}
					}
					if (value.inputAcept) {
						oFormat = baseUtility["validate" + value.inputAcept](
							objectValueItem);
					}
					if (value.predefinedExpReg) {
						oFormat = baseUtility["validate" + value.inputAcept](
							objectValueItem, value.predefinedExpReg);
					}

					if (value.exceptValue) {
						if (value.exceptValue !== objectValueItem) {
							oFormat.bool = true;
						}
					}
					if (value.aceptValues) {
						for (var m = 0; m < value.aceptValues.length; m++) {
							if (value.aceptValues[m] === objectValueItem) {
								oFormat.bool = true;
								m = value.aceptValues.length;
							} else {
								oFormat.bool = false;
							}
						}
						// if (value.exceptValue !== objectValueItem) {
						// 	oFormat.bool = true;
						// }
					}
					if (objectValueItem === "" || !oFormat.bool) {
						// objectItem.setValueState("Error");
						if (value.type !== "Select") {
							objectItem.setValueState("Error");
						} else {
							$("#" + objectItem.getId()).css("border-color", "red");
						}
						if (validateFormComplete.bool) {
							validateFormComplete.bool = false;
							// oController.getView().byId("detailIconTabBar").setSelectedKey(
							// 	"__component0---detail--" + value.tab);
							MessageToast.show(messageToast, {
								duration: 5000,
								// default
								width: "25%",
								// default
								animationTimingFunction: "ease-in",
								// default
								animationDuration: 800
							});
						}
					} else {
						// objectItem.setValueState("None");
						if (value.type !== "Select") {
							objectItem.setValueState("None");
						} else {
							$("#" + objectItem.getId()).css("border-color", "rgb(191, 191, 191)");
						}
					} // }
				}.bind(this));
			}
			// console.log(validateFormComplete);
			return validateFormComplete.bool;
		},

		// validateFormAccess: function(oThis) {
		// 	var oController = oThis;
		// 	var baseUtility = new BaseUtility();
		// 	var oViewApp = oController.getView().getModel("appView");
		// 	var messageToast = oController.getResourceBundle().getText("messageFailValidation");
		// 	var appVersion = oController.getResourceBundle().getText("appVersion");
		// 	// var evalueteItem = ;
		// 	var validateFormComplete = {
		// 		bool: true
		// 	};
		// 	var oEntityValidate = "validatePolicyAccessInformation";
		// 	if (appVersion === "1") {
		// 		oEntityValidate = "validatePolicyAccessInformationVisit";
		// 	}
		// 	/* global _:true */
		// 	_.each(oViewApp.getProperty("/" + oEntityValidate), function(value, key,
		// 		list) {
		// 		// console.log(value +" -- " + key +" -- "+ list);
		// 		var objectValueItem;
		// 		var objectItem;
		// 		var oFormat = {
		// 			bool: false
		// 		};
		// 		objectItem = oController.byId(value.item);
		// 		// if (objectItem.isActive()) {
		// 		if (value.type !== "Select") {
		// 			if (value.type === "Option") {
		// 				objectValueItem = objectItem.getSelectedIndex();
		// 			} else {
		// 				if (value.type === "SegmentedButton") {
		// 					objectValueItem = objectItem.getSelectedKey();
		// 				} else {
		// 					objectValueItem = objectItem.getValue();
		// 					objectValueItem = objectValueItem.trim();

		// 				}

		// 			}

		// 		} else {
		// 			objectValueItem = objectItem.getSelectedKey();
		// 		}
		// 		if (value.dependentFields) {
		// 			if (oController.byId(value.dependentFields.pathItem)[value.dependentFields
		// 					.prop]() !== value.dependentFields.value) {
		// 				return;
		// 			}
		// 		}
		// 		if (value.validateMaskFunction) {
		// 			oFormat = baseUtility[value.validateMaskFunction](objectValueItem);
		// 		}
		// 		if (value.inputAcept) {
		// 			oFormat = baseUtility["validate" + value.inputAcept](
		// 				objectValueItem);
		// 		}
		// 		if (value.predefinedExpReg) {
		// 			oFormat = baseUtility["validate" + value.inputAcept](
		// 				objectValueItem, value.predefinedExpReg);
		// 		}

		// 		if (value.exceptValue) {
		// 			if (value.exceptValue !== objectValueItem) {
		// 				oFormat.bool = true;
		// 			}
		// 		}
		// 		if (objectValueItem === "" || !oFormat.bool) {
		// 			// objectItem.setValueState("Error");
		// 			if (value.type === "SegmentedButton") {
		// 				for (var i = 0; i < objectItem.getItems().length; i++) {
		// 					$("#" + objectItem.getItems()[i].oButton.getId()).css("border-color", "red");
		// 					// $("#" + objectItem.getItems()[2].oButton.getId()).css("border-color", "red");
		// 				}

		// 			} else {
		// 				if (value.type !== "Select") {

		// 					// } else {
		// 					objectItem.setValueState("Error");
		// 				}
		// 			}

		// 			if (validateFormComplete.bool) {
		// 				validateFormComplete.bool = false;
		// 				// oController.getView().byId("detailIconTabBar").setSelectedKey(
		// 				// "__component0---detail--" + value.tab);
		// 				oController.getView().byId("detailIconTabBar").setSelectedKey(oController.byId(value.tab).getId());
		// 				MessageToast.show(messageToast, {
		// 					duration: 5000,
		// 					// default
		// 					width: "25%",
		// 					// default
		// 					animationTimingFunction: "ease-in",
		// 					// default
		// 					animationDuration: 800
		// 				});
		// 			}
		// 		} else {

		// 			if (value.type === "SegmentedButton") {
		// 				// $("#" + objectItem.getItems()[0].oButton.getId()).css("border-color", "");
		// 				// $("#" + objectItem.getItems()[2].oButton.getId()).css("border-color", "rgb(191, 191, 191)");
		// 				for (var j = 0; j < objectItem.getItems().length; j++) {
		// 					$("#" + objectItem.getItems()[j].oButton.getId()).css("border-color", "rgb(191, 191, 191)");
		// 					// $("#" + objectItem.getItems()[2].oButton.getId()).css("border-color", "red");
		// 				}
		// 			} else {

		// 				if (value.type !== "Select") {

		// 					// } else {
		// 					objectItem.setValueState("None");
		// 				}
		// 				// objectItem.setValueState("None");
		// 			}
		// 			// objectItem.setValueState("None");

		// 		} // }
		// 	});
		// 	// console.log(validateFormComplete);
		// 	return validateFormComplete.bool;
		// }

	});

});