sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"sap/ui/base/Object",
	"com/gc/passportvisits/js/vendor/moment",
	"com/gc/passportvisits/js/libs/BaseUtility",
	"sap/m/MessageBox",
	"com/gc/passportvisits/js/vendor/underscore"
], function(Object, momentjs, BaseUtility, MessageBox, underscore) {
	"use strict";
	return Object.extend("com.gc.passportvisits.js.libs.BaseUtility", {
		constructor: function() {

		},
		validateEmail: function(oEmailItem) {
			var _oEmail = oEmailItem.trim();
			_oEmail = _oEmail.toLowerCase();
			var regExp =
				/^(([^<>()\[\]\\.,;áéíóú´!"#$%&/()=?¡'¿*´+{}°|¬;~:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			// /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			var validado = _oEmail.match(regExp);
			if (!validado) {
				return {
					bool: false,
					oEmail: ""
				};
			}
			return {
				bool: true,
				oEmail: _oEmail
			};

		},

		validateString: function(oValueItem, oReExp) {
			var _oValueItem = oValueItem;
			var regExp = /^([a-zñáéíóú A-ZÑÁÉÍÓÚ]+[\s]*)+$/;
			if (oReExp) {
				regExp = oReExp;
			}

			var validado = _oValueItem.match(regExp);
			if (!validado) {
				return {
					bool: false,
					oString: _oValueItem
				};
			}
			return {
				bool: true,
				oString: _oValueItem
			};
		},
		validateAlfaNum: function(oValueItem, oReExp) {
			var _oValueItem = oValueItem;
			var regExp = /^([a-zñáéíóú A-ZÑÁÉÍÓÚ 0-9]+[\s]*)+$/;

			if (oReExp) {
				regExp = oReExp;
			}

			var validado = _oValueItem.match(regExp);
			if (!validado) {
				return {
					bool: false,
					oAlfaNum: _oValueItem
				};
			}
			return {
				bool: true,
				oAlfaNum: _oValueItem
			};
		},
		validateAlfaNumCode: function(oValueItem, oReExp) {
			var _oValueItem = oValueItem;
			var regExp = /^([a-zñáéíóú A-ZÑÁÉÍÓÚ 0-9 -/]*)+$/;

			if (oReExp) {
				regExp = oReExp;
			}

			var validado = _oValueItem.match(regExp);
			if (!validado) {
				return {
					bool: false,
					oAlfaNum: _oValueItem
				};
			}
			return {
				bool: true,
				oAlfaNum: _oValueItem
			};
		},
		validateNumber: function(oValueItem, oReExp) {
			var _oValueItem = oValueItem;
			var regExp = /^([0-9]*)+$/g;
			if (oReExp) {
				regExp = oReExp;
			}
			//  /^([0-9]{11})+$/g

			var validado = _oValueItem.match(regExp);
			if (!validado) {
				return {
					bool: false,
					oNum: _oValueItem
				};
			}
			return {
				bool: true,
				oNum: _oValueItem
			};
		},

		validateAdult: function(oValueItem) {
			/* global moment:true */
			var birthday = moment(oValueItem, "MM-DD-YYYY");
			var today = moment();
			var ageToday = today.diff(birthday, "years");

			if (isNaN(ageToday)) {
				birthday = moment(oValueItem, "YYYY-MM-DD");
				ageToday = today.diff(birthday, "years");
			}

			if (ageToday >= 18) {
				return {
					bool: true,
					oNum: oValueItem
				};
			}

			return {
				bool: false,
				oNum: oValueItem
			};

		},

		validateAfterToday: function(oValueItem) {
			/* global moment:true */
			var birthday = moment(oValueItem, "MM-DD-YYYY");
			var today = moment();
			var ageToday = today.diff(birthday, "years");

			if (isNaN(ageToday)) {
				birthday = moment(oValueItem, "YYYY-MM-DD");
				ageToday = today.diff(birthday, "years");
			}

			if (ageToday >= 18) {
				return {
					bool: true,
					oNum: oValueItem
				};
			}

			return {
				bool: false,
				oNum: oValueItem
			};

		},
		validateCurp: function(oCurp) {
			var re =
				/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0\d|1[0-2])(?:[0-2]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
			var _oCurp = oCurp.toUpperCase();
			var validado = _oCurp.match(re);
			if (!validado) {
				//Coincide con el formato general?
				return false;
			}
			return true; //Validado
		},

		showCustomMessageBox: function(_oThis, _oMessageType, _oMessageToUser, _oTitleMessage, _oFunction, _oValues) {
			var oController = _oThis;
			//type message information
			// if(_oValues.length > 0){

			// }
			// var extraMessage = "";
			// if (_oValues[2]) {
			// 	extraMessage = " visa id: " + _oValues[2];
			// }
			// if(){

			// }
			MessageBox[_oMessageType](_oMessageToUser, {
				title: _oTitleMessage,
				onClose: oController[_oFunction].bind(oController, _oValues),
				textDirection: sap.ui.core.TextDirection.Inherit
			});
		},

		findParameterWithUnderscore: function(_oParameter, _oCollection) {
			return new Promise(function(resolve, reject) {
				/* global _:true */
				var findThatNow = _.find(_oCollection.results, function(oEntity) {
					return oEntity.Parid === _oParameter;
				});
				switch (_oParameter) {
					case "YPSPT_LOG":
						if (findThatNow) {
							reject("--> Error not define role");
						} else {
							resolve(_oCollection);
						}
						// break;
					case "YPSPT_PEN_VIS":
						if (findThatNow) {
							resolve(_oCollection);
						} else {
							reject("--> Error not have a visit role");
						}
						// break;
					case "YPSPT_USERID":
						resolve(findThatNow.Parva);
						// break;
					default:
						reject("--> no valid parameter ");
				}
				// resolve(findThatNow);
			});
			// var even = _.find(response.results, function(_id) {
			// 	return _id.Parid === "YPSPT_LOG";
			// });

			// return findThatNow;

		},
		validateCniBulk: function(oCni) {
			var re = /^([0-9]{8})+$/;
			// /^([2]{1})+([0,3,4,7]{1})+([-]{1})+([0-9]{8})+([-]{1})+([0-9]{1})$/;
			var _oCni = oCni.toUpperCase();
			var validado = _oCni.match(re);
			if (!validado) {
				//Coincide con el formato general?
				return {
					bool: false,
					oNum: _oCni
				};
			}
			return {
				bool: true,
				oNum: _oCni
			}; //Validado
		},

		validateOtherIdBulk: function(oOtherId) {
			var re = /^([0-9]{8})+$/;
			// /^([2]{1})+([0,3,4,7]{1})+([-]{1})+([0-9]{8})+([-]{1})+([0-9]{1})$/;
			var _oOtherId = oOtherId.toUpperCase();
		
			if (_oOtherId === "") {
				//Coincide con el formato general?
				return {
					bool: false,
					oNum: _oOtherId
				};
			}
			return {
				bool: true,
				oNum: _oOtherId
			}; //Validado
		},
		validateCurpBulk: function(oCurp) {
			var re =
				/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0\d|1[0-2])(?:[0-2]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
			var _oCurp = oCurp.toUpperCase();
			var validado = _oCurp.match(re);
			if (!validado) {
				//Coincide con el formato general?
				return {
					bool: false,
					oNum: _oCurp
				};
			}
			return {
				bool: true,
				oNum: _oCurp
			}; //Validado
		},
	});

});