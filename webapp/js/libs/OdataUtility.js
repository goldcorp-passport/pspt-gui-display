sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"sap/ui/base/Object",
	"com/gc/passportvisits/js/vendor/moment",
	"com/gc/passportvisits/js/libs/BaseUtility",
	"sap/m/MessageBox",
	"com/gc/passportvisits/js/libs/ValidatorForm",
	"sap/ui/model/json/JSONModel"
], function(Object, momentjs, BaseUtility, MessageBox, ValidatorForm, JSONModel) {
	"use strict";
	return Object.extend("com.gc.passportvisits.js.libs.OdataUtility", {
		constructor: function() {

		},
		requestAreas: function(oThis, _oFilters) {
			var oController = oThis;
			var oFilters = _oFilters;
			var oModel = oController.getView().getModel("CATALOGS");
			var oSite = oController.getModel("oModelVisaClone").getProperty("/Siteid");
			return new Promise(function(resolve, reject) {

				// var filters = [new Filter("Siteid", sap.ui.model.FilterOperator.EQ, oSite)];
				oModel.read("/AreasSet", {
					filters: oFilters,
					success: jQuery.proxy(function(mResponse) {
						// console.log(mResponse);

						resolve(mResponse);
					}, oController),
					error: jQuery.proxy(function(mResponse) {
						console.log("error", mResponse);
						reject(mResponse);
					}, oController)
				});

			});

		},

		requestAreasByData: function(oThis) {
			var oController = oThis;
			var oModel = oController.getView().getModel("CATALOGS");
			var oSite = oController.getModel("oModelVisaClone").getProperty("/Siteid");
			var oVisa = oController.getModel("oModelVisaClone").getProperty("/Visaid");

			return new Promise(function(resolve, reject) {

				oModel.read("/AreasSet(Siteid='" + oSite + "',Visaid='" + oVisa + "')", {
					success: jQuery.proxy(function(mResponse) {
						// oController.onChanceValueBusyIndicator(false);
						// console.log(oContextAreas);

						resolve(mResponse);
					}, oController),
					error: jQuery.proxy(function(mResponse) {
						console.log("error", mResponse);
						reject(mResponse);
					}, oController)
				});

			});

		},
		requestFindUserByVisit: function(_oThis) {
			var oController = _oThis;
			return new Promise(function(resolve, reject) {
				// var sUrl = ;
				var oModel = oController.getView().getModel();
				oModel.read("/UserParamsSet('YPSPT_PEN_VIS')", {
					success: jQuery.proxy(function(response) {
						resolve(response);
					}, oController),
					error: jQuery.proxy(function(response) {
						reject(response);
					}, oController)
				});
			});

		},

		requestCreateVisa: function(_oThis, _oOdataUtility, _oPayLoad) {
			var oController = _oThis;
			// var oBaseUtility = new BaseUtility();
			var oOdataUtility = _oOdataUtility;
			var oModel = oController.getView().getModel();

			oModel.create("/VISASet", _oPayLoad[0], {
				success: jQuery.proxy(function(mResponse) {

					oOdataUtility.requestCreateAccessArea(oController, _oPayLoad[1], [mResponse.Passportid,
						true, mResponse.Visaid]);

					// var oViewApp = oController.getView().getModel("appView");
					// oViewApp.setProperty("/configDetailProfile/businessCardVisible", true);
				}, oController),
				error: jQuery.proxy(function(error) {
					// console.log("---> error");
				}, oController)
			});
		},
		requestCreateAccessArea: function(_oThis, _oPayLoad, _oValues) {
			var oController = _oThis;
			var oBaseUtility = new BaseUtility();
			var oModel = oController.getView().getModel("CATALOGS");
			var oValues = _oValues;

			var mPayloadAccess = {};
			mPayloadAccess.IterAreasDesc = _oPayLoad;
			mPayloadAccess.HeaderArea = "";
			mPayloadAccess.Siteid = oController.getModel("oModelVisaClone").getProperty("/Siteid");
			mPayloadAccess.Visaid = oValues[2];

			oModel.create("/AreasSet", mPayloadAccess, {
				// filters: filters,
				success: jQuery.proxy(function(mResponse) {
					console.log("success");
					console.log(mResponse);
					oBaseUtility.showCustomMessageBox(oController, "information",
						// oController.getResourceBundle().getText("messageEndCreate"),
						oController.getResourceBundle().getText("messageEndCreate") + " Visa id: " + oValues[2],
						oController.getResourceBundle().getText("messageTitleInfo"),
						"closeConfirmCreate", oValues
					);

					// resolve(mResponse);
				}, oController),
				error: jQuery.proxy(function(mResponse) {
					console.log("error");
					console.log(mResponse);
					this.onChanceValueBusyIndicator(false);
					console.log("---> error");
					// reject(mResponse);
				}, oController)
			});
		},
		requestCreateAccessAreaOld: function(_oThis, _oPayLoad, _oValues) {
			var oController = _oThis;
			var oBaseUtility = new BaseUtility();
			var oModel = oController.getView().getModel();
			var oValues = _oValues;

			var oPayLoad = _oPayLoad;
			// oPayLoad.Siteid = "PEN";
			oPayLoad.Siteid = oController.getModel("oModelVisaClone").getProperty("/Siteid");
			oPayLoad.Visaid = oValues[2];

			oModel.create("/AreasByVisaSet", oPayLoad, {
				success: jQuery.proxy(function(mResponse) {

					oBaseUtility.showCustomMessageBox(oController, "information",
						oController.getResourceBundle().getText("messageEndCreate") + " Visa id: " + oValues[2],
						oController.getResourceBundle().getText("messageTitleInfo"),
						"closeConfirmCreate", oValues
					);
					// var oViewApp = oController.getView().getModel("appView");
					// oViewApp.setProperty("/configDetailProfile/businessCardVisible", true);
				}, oController),
				error: jQuery.proxy(function(error) {
					this.onChanceValueBusyIndicator(false);
					// console.log("---> error");
				}, oController)
			});
		},

		requestUpdateProfile: function(_oThis, _inactivateProfile) {
			var oController = _oThis;
			var oValidatorForm = new ValidatorForm();
			var oBaseUtility = new BaseUtility();
			if (oValidatorForm.validateFormProfile(oController)) {
				var formatDate = new Date(oController.byId("inputBirthdate").getValue());
				var oViewApp = oController.getView().getModel("appView");
				var oSex = oController.getValueToBindingModel("Sex");
				var oIsEjidatario = oController.getValueToBindingModel("ChkEjido");
				var mPayload = {
					NameFirst: oController.byId("inputNameFirst").getValue().toUpperCase(),
					Namemiddle: oController.byId("inputNamemiddle").getValue().toUpperCase(),
					NameLast: oController.byId("inputNameLast").getValue().toUpperCase(),
					NameLast2: oController.byId("inputNameLast2").getValue().toUpperCase(),
					Nickname: oController.byId("inputNickname").getValue().toUpperCase(),
					Sex: oSex,
					Birthdate: formatDate,
					Street: oController.byId("inputStreet").getValue().toUpperCase(),
					HouseNum1: oController.byId("HouseNum1").getValue().toUpperCase(),
					Country: oController.byId("inputCountry").getSelectedKey(),
					City1: oController.byId("inputCity1").getValue().toUpperCase(),
					PostCode1: oController.byId("inputPostCode1").getValue().toUpperCase(),
					Email: oController.byId("inputEmail").getValue().toUpperCase(),
					TelNumber: oController.byId("inputTelNumber").getValue().toUpperCase(),
					UidData: oController.byId("inputUidData").getValue().toUpperCase(),
					Imss: oController.byId("inputImss").getValue().toUpperCase(),
					DriverLicense: oController.byId("inputDriverLicense").getValue().toUpperCase(),
					Status: oViewApp.getProperty("/statusActiveModel_"),
					ChkEjido: oIsEjidatario,
					ChkPersonal: oController.byId("inputPersonalEjido").getSelected() ? "X" : "",
					ChkConyugue: oController.byId("inputConyugueEjido").getSelected() ? "X" : "",
					ChkFamiliar: oController.byId("inputFamiliarEjido").getSelected() ? "X" : "",
					NombreConyugue: oController.byId("inputNombreConyugue").getValue().toUpperCase(),
					NombreFamiliar: oController.byId("inputNombreFamiliar").getValue().toUpperCase(),
					Parentesco: oController.byId("inputParentesco").getValue().toUpperCase(),
					Empresa: oController.byId("inputEmpresa").getValue().toUpperCase(),
					ChkTrabajaMina: oController.getValueToBindingModel("ChkTrabajaMina"),
					IdComunidades: oController.byId("inputSelectEjido").getSelectedKey(),
					ContactRelationship: oController.byId("inputContactRelationship").getValue(),
					ContactName: oController.byId("inputContactName").getValue(),
					ContactPhone: oController.byId("inputContactPhone").getValue(),
					LanguP: oController.byId("inputLanguP").getSelectedKey()
				};
				if (oController.flagTakeProfile) {
					// oController.flagTakeProfile = false;
					// actionRequest = "create";
					mPayload.Type = "V";
					mPayload.IssuedBy = oViewApp.getProperty("/issuedBy");
				}
				var sUrl = "/PROFILESet('" + oController.getView().byId("objectHeader").getNumber() + "')";
				var oModel = oController.getView().getModel();
				oModel.update(sUrl, mPayload, {
					merger: true,
					success: jQuery.proxy(function(mResponse) {
						// MessageBox.information("Actualizaci\xF3n exitosa.", {
						// 	title: "Informaci\xF3n",
						// 	onClose: this.closeConfirmUpdate.bind(oController),
						// 	textDirection: sap.ui.core.TextDirection.Inherit
						// });
						// oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
						// oBaseUtility.showCustomMessageBox(oController, "information",
						// 	oController.getResourceBundle().getText("messageEndUpdate"),
						// 	oController.getResourceBundle().getText("messageTitleInfo"),
						// 	"closeConfirmUpdate"

						// );
						if (_inactivateProfile) {
							oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
							oBaseUtility.showCustomMessageBox(oController, "information",
								oController.getResourceBundle().getText("messageEndUpdate"),
								oController.getResourceBundle().getText("messageTitleInfo"),
								"closeConfirmUpdateWithDesactivateProfile"

							);
							// delete oController.getView().getModel().getProperty("/PROFILESet('" + oController.getView().byId("objectHeader").getNumber() + "')");
						} else {

							// var oList = this.byId("list")
							// oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
							// oBaseUtility.showCustomMessageBox(oController, "information",
							// 	oController.getResourceBundle().getText("messageEndUpdate"),
							// 	oController.getResourceBundle().getText("messageTitleInfo"),
							// 	"closeConfirmUpdate"
							oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
							oBaseUtility.showCustomMessageBox(oController, "information",
								oController.getResourceBundle().getText("messageEndUpdate"),
								oController.getResourceBundle().getText("messageTitleInfo"),
								"closeConfirmUpdate");
						}
					}, oController),
					error: jQuery.proxy(function(mResponse) {
						// console.log("error", mResponse);
					}, oController)
				});
			} else {
				oController.onChanceValueBusyIndicator(false);
			}
		},

		requestCreateProfile: function(_oThis) {

			var oController = _oThis;
			// oController.onChanceValueBusyIndicator(true);
			var oValidatorForm = new ValidatorForm();
			var oBaseUtility = new BaseUtility();
			if (oValidatorForm.validateFormProfile(oController)) {
				var oViewApp = oController.getView().getModel("appView");
				var oSex = oController.getValueToBindingModel("Sex");
				var oFormatDate = oController.transformDateGWType(oController.byId("inputBirthdate").getValue());
				var oIsEjidatario = oController.getValueToBindingModel("ChkEjido");
				var mPayload = {
					NameFirst: oController.byId("inputNameFirst").getValue().toUpperCase(),
					Namemiddle: oController.byId("inputNamemiddle").getValue().toUpperCase(),
					NameLast: oController.byId("inputNameLast").getValue().toUpperCase(),
					Uidtype: oViewApp.getProperty("/optionNal"),
					Sex: oSex,
					Type: "",
					IssuedBy: oViewApp.getProperty("/issuedBy"),
					// IssuedBy: "CLOPEZ03",
					NameLast2: oController.byId("inputNameLast2").getValue().toUpperCase(),
					Nickname: oController.byId("inputNickname").getValue().toUpperCase(),
					Birthdate: oFormatDate,
					Street: oController.byId("inputStreet").getValue().toUpperCase(),
					HouseNum1: oController.byId("HouseNum1").getValue().toUpperCase(),
					Country: oController.byId("inputCountry").getSelectedKey(),
					City1: oController.byId("inputCity1").getValue().toUpperCase(),
					PostCode1: oController.byId("inputPostCode1").getValue().toUpperCase(),
					Email: oController.byId("inputEmail").getValue().toUpperCase(),
					TelNumber: oController.byId("inputTelNumber").getValue().toUpperCase(),
					UidData: oController.byId("inputUidData").getValue().toUpperCase(),
					Imss: oController.byId("inputImss").getValue().toUpperCase(),
					DriverLicense: oController.byId("inputDriverLicense").getValue().toUpperCase(),
					Status: oViewApp.getProperty("/statusActiveModel_"),
					ChkEjido: oIsEjidatario,
					ChkPersonal: oController.byId("inputPersonalEjido").getSelected() ? "X" : "",
					ChkConyugue: oController.byId("inputConyugueEjido").getSelected() ? "X" : "",
					ChkFamiliar: oController.byId("inputFamiliarEjido").getSelected() ? "X" : "",
					NombreConyugue: oController.byId("inputNombreConyugue").getValue().toUpperCase(),
					NombreFamiliar: oController.byId("inputNombreFamiliar").getValue().toUpperCase(),
					Parentesco: oController.byId("inputParentesco").getValue().toUpperCase(),
					Empresa: oController.byId("inputEmpresa").getValue().toUpperCase(),
					ChkTrabajaMina: oController.getValueToBindingModel("ChkTrabajaMina"),
					IdComunidades: oController.byId("inputSelectEjido").getSelectedKey(),
					ContactRelationship: oController.byId("inputContactRelationship").getValue(),
					ContactName: oController.byId("inputContactName").getValue(),
					ContactPhone: oController.byId("inputContactPhone").getValue(),
					LanguP: oController.byId("inputLanguP").getSelectedKey()
				};
				if (oController.getResourceBundle().getText("appVersion") === "1") {
					mPayload.Type = "V";
				} else {
					mPayload.Type = "C";
				}
				var oModel = oController.getView().getModel();
				oModel.create("/PROFILESet", mPayload, {
					success: jQuery.proxy(function(mResponse) {
						// var oViewModel = oController.getModel("detailView");
						// oViewModel.setProperty("/busy", true);
						// oController.onAddPO(mResponse.Passportid, true);
						oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText(
							"messageReadOnlyWithCommentsToCreatevisa"));
						oBaseUtility.showCustomMessageBox(oController, "information",
							oController.getResourceBundle().getText("messageEndCreate") + " Passport id: " + mResponse.Passportid,
							oController.getResourceBundle().getText("messageTitleInfo"),
							"closeConfirmCreate", [mResponse.Passportid]

						);

					}, oController),
					error: jQuery.proxy(function() {
						// console.log("---> error");
					}, oController)
				});
			} else {
				oController.onChanceValueBusyIndicator(false);
			}
		},
		requestVisaSetFromNavigationTovisa: function(_oThis, _oPath) {
			var oController = _oThis;
			var oPath = _oPath;
			return new Promise(function(resolve, reject) {
				var oModel = oController.getView().getModel();
				oModel.read(oPath + "/ToVisa", {
					success: jQuery.proxy(function(response) {
						resolve(response);
					}, oController),
					error: jQuery.proxy(function(response) {
						reject(response);
					}, oController)
				});
			});

		},
		requestIssuedBySet: function(_oThis) {
			var oController = _oThis;

			return new Promise(function(resolve, reject) {
				var oIssuedBySet = this.requestIssuedBySetInit(oController);
				oIssuedBySet.then(this.requestUserBulkLoad.bind(oController))

				oIssuedBySet.then(function(res) {
					// console.log(res)
					// if (res === "ok") {
					resolve();
					// } else {
					// 	reject();
					// }

				})
			}.bind(this));

		},
		requestIssuedBySetInit: function(_oThis) {
			var oController = _oThis;
			return new Promise(function(resolve, reject) {
				// var sUrl = ;
				var oModel = oController.getView().getModel();
				oModel.read("/IssuedByVisitorSet", {
					success: jQuery.proxy(function(response) {
						var oViewApp = this.getView().getModel("appView");
						if (response.results[0].IssuedId === "") {
							oController.getRouter().getTargets().display("detailNoObjectsAvailable");
						}
						oViewApp.setProperty("/issuedBy", response.results[0].IssuedId);

						resolve(response);
					}, oController),
					error: jQuery.proxy(function(response) {
						reject(response);
					}, oController)
				});
			});

		},

		requestUserBulkLoad: function(oResults) {
			var oController = this;
			var oModel = oController.getView().getModel("CATALOGS");
			var oSite = oController.getModel("oModelVisaClone").getProperty("/Siteid");
			return new Promise(function(resolve, reject) {

				oModel.read("/ListUsersCNSet", {
					// filters: oFilters,
					success: jQuery.proxy(function(mResponse) {

						var oCurrentIssuedBy = oController.getView().getModel("appView").getProperty("/issuedBy");

						var showBullkLoad = _.filter(mResponse.results, function(value) {
							if (value.Userid === oCurrentIssuedBy) {
								return true
							}
						});
						if (showBullkLoad.length === 1) {

							var validatePhone = oController.getModel("device").getProperty("/system/desktop");

							if (validatePhone) {
								oController.getView().getModel("appView").setProperty("/bulkProfile", true);
							}

						}

						resolve(mResponse);
					}, oController),
					error: jQuery.proxy(function(mResponse) {
						console.log("error", mResponse);
						reject(mResponse);
					}, oController)
				});

			});

		},
		requestUidSet: function(_oData) {
			var oData = _oData;
			var oController = this;
			return new Promise(function(resolve, reject) {
				// /UIDSet(IType='P',IUid='K00000000')
				// (ITypeUid='P',IUid='100005P03',ITypePspt='C')
				// var sUrl = "/UIDSet(IType='C',IUid='LOCC840902HDFOMR05' ,ITypePspt='V')";
				var sUrl = "/UIDSet(ITypeUid='" + oData.optionNal + "',IUid='" + oData.uidData +
					"',ITypePspt='V')";
				var oModel = oController.getView().getModel();
				oModel.read(sUrl, {
					success: jQuery.proxy(function(response) {
						// var msgCurp = "";
						// var _logNo = response.EReturn.LogNo;

						resolve(response);

					}, oController),
					error: jQuery.proxy(function(response) {
						reject("Service Error: " + response);
						// console.log("error", mResponse);
					}, oController)
				});
			});

		},
		requestUidSetBulk: function(_oData, _this) {
			var oData = _oData;
			var oController = _this;
			return new Promise(function(resolve, reject) {
				// /UIDSet(IType='P',IUid='K00000000') (ITypeUid='P',IUid='100005P01',ITypePspt='C')
				var sUrl = "/UIDSet(ITypeUid='" + oData.optionNal.toUpperCase() + "',IUid='" + oData.uidData.toUpperCase() +
					"',ITypePspt='C')";
				var oModel = oController.getView().getModel();
				oModel.read(sUrl, {
					success: jQuery.proxy(function(response) {
						resolve(response);
					}, oController),
					error: jQuery.proxy(function(response) {
						reject("Service Error: " + response);
					}, oController)
				});
			});

		},
		requestCreateProfileBulk: function(_count) {

			var oController = this;
			return new Promise(function(resolve, reject) {
				var oModelBulk = oController.getView().getModel("DataBulk");
				var oModel = oController.getView().getModel();
				var currentEval = oController.getView().getModel("DataBulkFlag").getProperty("/CurrentEval");
				var itemToCreate = oModelBulk.getProperty("/oJS/" + currentEval);

				var mPayload = {
					Uidtype: ((itemToCreate.Uidtype) ? itemToCreate.Uidtype.toUpperCase() : ""), //itemToCreate.Uidtype.toUpperCase(),
					UidData: ((itemToCreate.UidData) ? itemToCreate.UidData.toUpperCase() : ""), // itemToCreate.UidData.toUpperCase(),
					ContLocat: ((itemToCreate.ContLocat) ? itemToCreate.ContLocat.toUpperCase() : ""), // itemToCreate.UidData.toUpperCase(),
					DriverLicense: ((itemToCreate.DriverLicense) ? itemToCreate.DriverLicense.toUpperCase() : ""), //itemToCreate.DriverLicense.toUpperCase(),
					Imss: ((itemToCreate.Imss) ? itemToCreate.Imss.toUpperCase() : ""), //itemToCreate.Imss.toUpperCase(),
					NameFirst: ((itemToCreate.NameFirst) ? itemToCreate.NameFirst.toUpperCase() : ""), //itemToCreate.NameFirst.toUpperCase(),
					Namemiddle: ((itemToCreate.Namemiddle) ? itemToCreate.Namemiddle.toUpperCase() : ""), //itemToCreate.Namemiddle.toUpperCase(),
					NameLast: ((itemToCreate.NameLast) ? itemToCreate.NameLast.toUpperCase() : ""), //itemToCreate.NameLast.toUpperCase(),
					NameLast2: ((itemToCreate.NameLast2) ? itemToCreate.NameLast2.toUpperCase() : ""), // itemToCreate.NameLast2.toUpperCase(),
					Nickname: ((itemToCreate.Nickname) ? itemToCreate.Nickname.toUpperCase() : ""), //itemToCreate.Nickname.toUpperCase(),
					Sex: ((itemToCreate.Sex) ? itemToCreate.Sex.toUpperCase() : ""), //itemToCreate.Sex.toUpperCase(),
					Birthdate: ((itemToCreate.Birthdate) ? new Date(itemToCreate.Birthdate) : null), //new Date(itemToCreate.Birthdate),
					LanguP: ((itemToCreate.LanguP) ? itemToCreate.LanguP.toUpperCase() : ""), //itemToCreate.LanguP.toUpperCase(),
					Street: ((itemToCreate.Street) ? itemToCreate.Street.toUpperCase() : ""), //itemToCreate.Street.toUpperCase(),
					HouseNum1: ((itemToCreate.HouseNum1) ? itemToCreate.HouseNum1.toUpperCase() : ""), //itemToCreate.HouseNum1.toUpperCase(),
					Country: ((itemToCreate.Country) ? itemToCreate.Country.toUpperCase() : ""), //itemToCreate.Country.toUpperCase(),
					City1: ((itemToCreate.City1) ? itemToCreate.City1.toUpperCase() : ""), //itemToCreate.City1.toUpperCase(),
					PostCode1: ((itemToCreate.PostCode1) ? itemToCreate.PostCode1.toUpperCase() : ""), //itemToCreate.PostCode1.toUpperCase(),
					Email: ((itemToCreate.Email) ? itemToCreate.Email.toUpperCase() : ""), //itemToCreate.Email.toUpperCase(),
					TelNumber: ((itemToCreate.TelNumber) ? itemToCreate.TelNumber.toUpperCase() : ""), //itemToCreate.TelNumber.toUpperCase(),
					ContactRelationship: ((itemToCreate.ContactRelationship) ? itemToCreate.ContactRelationship.toUpperCase() :
						""), //itemToCreate.ContactRelationship.toUpperCase(),
					ContactName: ((itemToCreate.ContactName) ? itemToCreate.ContactName.toUpperCase() : ""), //itemToCreate.ContactName.toUpperCase(),
					ContactPhone: ((itemToCreate.ContactPhone) ? itemToCreate.ContactPhone.toUpperCase() : ""), // itemToCreate.ContactPhone.toUpperCase(),
					IssuedBy: ((itemToCreate.IssuedBy) ? itemToCreate.IssuedBy.replace("GC", "") : ""), //itemToCreate.IssuedBy.replace("GC", ""),
					Type: "C",
					Status: "A",
					Rfc: ((itemToCreate.Rfc) ? itemToCreate.Rfc.toUpperCase() : ""),
					Legajo: ((itemToCreate.Legajo) ? itemToCreate.Legajo.toUpperCase() : "")
				};

				// console.log()
				// resolve(mPayload);

				oModel.create("/PROFILESet", mPayload, {
					success: jQuery.proxy(function(mResponse) {
						resolve(mResponse);
					}, oController),
					error: jQuery.proxy(function(err) {
						console.log("---> error " + err);
					}, oController)
				});
			});

		},
		getEmptyVisaModel: function() {
			return {
				results: [{
					Supervisor: "",
					ApprovalUser: "",
					ApprovalDate: "",
					ChkComputer: "",
					ApprovalTime: "",
					Department: "",
					Flight: "",
					Username: "",
					Host: "",
					Udate: "",
					TranspDescrip: "",
					VisitReasonId: "",
					Approver: "",
					FlightDescrip: "",
					Visaid: "",
					ComputerEquip: "",
					TypeWork: "",
					UrgentVisa: "",
					RegularityVisit: "",
					Siteid: "",
					Passportid: "",
					ReqSupervision: "",
					VisaType: "",
					VisitReason: "",
					Status: "",
					LunchService: "",
					CampService: "",
					Transportation: "",
					InductionCourse: "",
					SysStatus: "",
					IssuedDate: "",
					ExpiryDate: "",
					SiteHost: "",
					PurchDoc: ""

					// Host: "",
					// TranspDescrip: "",
					// Flight: "",
					// VisitReasonId: "",
					// ChkComputer: "",
					// FlightDescrip: "",
					// Visaid: "",
					// Approver: "",
					// ComputerEquip: "",
					// TypeWork: "",
					// UrgentVisa: "",
					// Siteid: "",
					// RegularityVisit: "",
					// Passportid: "",
					// ReqSupervision: "",
					// VisaType: "",
					// VisitReason: "",
					// Status: "",
					// LunchService: "-",
					// CampService: "-",
					// Transportation: "",
					// InductionCourse: "",
					// SysStatus: "",
					// IssuedDate: "",
					// ExpiryDate: "",
					// SiteHost: "",
					// PurchDoc: ""
					}]
			};
		},
		getEmptyProfileModel: function() {
			return {
				ChkEjido: "",
				ChkPersonal: "",
				NombreConyugue: "",
				Client: "",
				ChkConyugue: "",
				Passportid: "",
				ChkFamiliar: "",
				Personid: "",
				Addressid: "",
				NombreFamiliar: "",
				Parentesco: "",
				Type: "",
				ChkTrabajaMina: "",
				IssuedBy: "",
				Empresa: "",
				IssuedDate: "",
				ExpiryDate: "",
				Status: "",
				Uidtype: "",
				Uidexpiry: "",
				UidData: "",
				DateFrom: "",
				DateTo: "",
				NameCo: "",
				City1: "",
				City2: "",
				CityCode: "",
				CitypCode: "",
				HomeCity: "",
				CityhCode: "",
				Chckstatus: "",
				Regiogroup: "",
				PostCode1: "",
				PostCode2: "",
				PostCode3: "",
				Pcode1Ext: "",
				Pcode2Ext: "",
				Pcode3Ext: "",
				PoBox: "",
				DontUseP: "",
				PoBoxNum: "",
				PoBoxLoc: "",
				CityCode2: "",
				PoBoxReg: "",
				PoBoxCty: "",
				Postalarea: "",
				Transpzone: "",
				Street: "",
				DontUseS: "",
				Streetcode: "",
				Streetabbr: "",
				HouseNum1: "",
				HouseNum2: "",
				HouseNum3: "",
				StrSuppl1: "",
				StrSuppl2: "",
				StrSuppl3: "",
				Location: "",
				Building: "",
				Floor: "",
				Roomnumber: "",
				Country: "",
				Langu: "",
				Region: "",
				Sort1: "",
				Sort2: "",
				SortPhn: "",
				Addrorigin: "",
				Extension1: "",
				Extension2: "",
				TimeZone: "",
				Taxjurcode: "",
				AddressId: "",
				Remark: "",
				LanguCrea: "",
				PoBoxLobby: "",
				DeliServType: "",
				DeliServNumber: "",
				CountyCode: "",
				County: "",
				TownshipCode: "",
				Township: "",
				TitleP: "",
				NameFirst: "minamefirst",
				NameLast: "",
				Name2P: "",
				Namemiddle: "",
				NameLast2: "",
				NameText: "",
				Converted: "",
				TitleAca1: "",
				TitleAca2: "",
				Prefix1: "",
				Prefix2: "",
				TitleSppl: "",
				Nickname: "",
				Initials: "",
				Nameformat: "",
				Namcountry: "",
				Profession: "",
				Sex: "",
				LanguP: "",
				Sort1P: "",
				Sort2P: "",
				SortPhnP: "",
				Persorigin: "",
				LanguCrP: "",
				SoKey: "",
				DefltComm: "",
				AddrPers: "",
				AdPersHd: "",
				Email: "",
				Birthdate: "",
				Imss: ""
			};
		}
	});

});