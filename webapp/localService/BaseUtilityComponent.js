sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"sap/ui/base/Object",
	"sap/m/MessageBox",
	// "sap/m/IconTabBar",
	"sap/ui/model/json/JSONModel"
], function(Object, momentjs, MessageBox, ValidatorForm, JSONModel) {
	"use strict";
	return Object.extend("com.gc.passportemployee.js.libs.BaseUtilityComponent", {
		constructor: function() {

		},
		createTabIcon: function(_oId, _oActionEvent, _oStyleClass) {
			var oIconTabBar = new sap.m.IconTabBar(_oId, {
				expandable: false,
				expanded: true,
				select: _oActionEvent
			});
			oIconTabBar.addStyleClass("sapUiResponsiveContentPadding " + _oStyleClass);

			return oIconTabBar;
		},
		createTreeTable: function(_oId, _oStyleClass, _oModel) {

			var oTreeTable = new sap.ui.table.TreeTable(_oId, {
				enableSelectAll: false,
				collapseRecursive: false,
				ariaLabelledBy: "title",
				selectionMode: "MultiToggle"
			});
			oTreeTable.addStyleClass(_oStyleClass);
			oTreeTable.setModel(_oModel);

			return oTreeTable;
		},

		createColumn: function(_oLabel, _oTemplate) {

			var oColumn = new sap.ui.table.Column({
				label: _oLabel,
				template: _oTemplate,
				width: "100%"
			});

			return oColumn;
		},

		// (oContext.getObject().DataName + "-P", "panelTile", oContext.getObject().DataPrettyName, oHBox);
		createPanel: function(_oId, _oStyleClass, _oText, _oContent, _backG) {
			var backG = "Transparent";
			if (_backG) {
				backG = _backG;
			}

			var oPanel = new sap.m.Panel("panel" + _oId, {
				backgroundDesign: backG,
				headerText: _oText,
				content: _oContent
			});
			oPanel.addStyleClass(_oStyleClass);

			return oPanel;
		},

		createStandardListItem: function(_oId, _oText, _oAction) {

			var oItemList = new sap.m.StandardListItem(_oId, {
				// backgroundDesign: "Transparent",
				// title: _oText.toLowerCase(),
				title: _oText,
				type: "Active",
				press: _oAction
					// select: _oAction
			});
			// oItemList.addStyleClass(_oStyleClass);

			return oItemList;
		},

		createStandardListItemDeletetion: function(_oId, _oText) {

			var oItemList = new sap.m.StandardListItem(_oId, {
				// backgroundDesign: "Transparent",
				title: _oText,
				// type: "Active",
				// select: _oAction
			});
			// oItemList.addStyleClass(_oStyleClass);

			return oItemList;
		},
		createList: function(_oId, _oStyleClass, _oHeaderText, _oContent) {

			var oList = new sap.m.List("list" + _oId, {
				backgroundDesign: "Transparent",
				headerText: _oHeaderText,
				content: _oContent,
				noDataText: "No areas found",

				// mode: "Delete",
				// delete: _oAction
				// itemPress: _oItemPress
			});
			oList.addStyleClass(_oStyleClass);

			return oList;
		},
		createListDelete: function(_oId, _oStyleClass, _oHeaderText, _oAction) {

			var oList = new sap.m.List("list" + _oId, {
				backgroundDesign: "Transparent",
				headerText: _oHeaderText,
				// content: _oContent,
				noDataText: "No areas found",
				mode: "Delete",
				delete: _oAction
			});
			oList.addStyleClass(_oStyleClass);

			return oList;
		},
		createIconTabFilter: function(_oId, _oIcon, _oText, _oContent) {

			var oIconTabFilter = new sap.m.IconTabFilter("tab-" + _oId, {
				icon: _oIcon,
				text: _oText,
				key: "iconTab-" + _oId,
				content: _oContent

			})

			return oIconTabFilter;
		},

		createHBox: function(_oId, _oStyleClass) {
			var oHBox = new sap.m.HBox(_oId, {

			});
			oHBox.addStyleClass(_oStyleClass);
			return oHBox;
		},

		createVBox: function(_oId, _oDirction, _oSryleClass) {
			var oVBox = new sap.m.VBox(_oId, {
				direction: _oDirction
			});
			oVBox.addStyleClass(_oSryleClass);

			return oVBox
		},

		createCustomTileScoreCard: function(_oContent, _oSryleClass) {
			var oTile = new sap.m.CustomTile({
				content: _oContent
			});
			oTile.addStyleClass(_oSryleClass);
			return oTile;
		},

		createLabel: function(_oId, _oText) {
			var oLabel = new sap.m.Label(_oId, {
				width: "100%",
				wrapping: true,
				displayOnly: true,
				textAlign: "Right",
				vAlign: "Middle",
				text: _oText
			});
			return oLabel;
		},

		createLabelWitoutId: function(_oText) {
			var oLabel = new sap.m.Label({
				// width: "100%",
				// wrapping: true,
				// displayOnly: true,
				// textAlign: "Right",
				// vAlign: "Middle",
				text: _oText
			});
			return oLabel;
		}

	});

});