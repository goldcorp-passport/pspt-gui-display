jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 PROFILESet in the list

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/gc/passportvisits/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/gc/passportvisits/test/integration/pages/App",
	"com/gc/passportvisits/test/integration/pages/Browser",
	"com/gc/passportvisits/test/integration/pages/Master",
	"com/gc/passportvisits/test/integration/pages/Detail",
	"com/gc/passportvisits/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.gc.passportvisits.view."
	});

	sap.ui.require([
		"com/gc/passportvisits/test/integration/MasterJourney",
		"com/gc/passportvisits/test/integration/NavigationJourney",
		"com/gc/passportvisits/test/integration/NotFoundJourney",
		"com/gc/passportvisits/test/integration/BusyJourney"
	], function () {
		QUnit.start();
	});
});