/*global history */
sap.ui.define([
	"com/gc/passportvisits/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"com/gc/passportvisits/model/formatter",
	"com/gc/passportvisits/js/libs/OdataUtility",
	"jquery.sap.global",
	"sap/ui/core/mvc/Controller",
	"com/gc/passportvisits/js/vendor/xlsx",
	"com/gc/passportvisits/js/vendor/jszip",
	"com/gc/passportvisits/js/libs/ValidatorForm",
		"sap/m/MessageToast",
	"jquery.sap.global",

	"sap/ui/core/mvc/Controller"
], function(BaseController, JSONModel, Filter, FilterOperator, GroupHeaderListItem, Device, formatter,
	OdataUtility, XLSX, jszip, ValidatorForm, MessageToast) {
	"use strict";
	return BaseController.extend("com.gc.passportvisits.controller.Master", {
		formatter: formatter,
		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */
		/**
		 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
		 * @public
		 */
		onInit: function() {
			// Control state model
			var oList = this.byId("list"),
				oViewModel = this._createViewModel(),
				// Put down master list's original value for busy indicator delay,
				// so it can be restored later on. Busy handling on the master list is
				// taken care of by the master list itself.
				iOriginalBusyDelay = oList.getBusyIndicatorDelay();
			this._oList = oList;
			// keeps the filter and search state
			this._oListFilterState = {
				aFilter: [],
				aSearch: []
			};

			this._oSearchParameter = "NameText";
			this.setModel(oViewModel, "masterView");
			// Make sure, busy indication is showing immediately so there is no
			// break after the busy indication for loading the view's meta data is
			// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
			oList.attachEventOnce("updateFinished", function() {
				// Restore original busy indicator delay for the list
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			});
			this.getView().addEventDelegate({
				onBeforeFirstShow: function() {
					this.getOwnerComponent().oListSelector.setBoundMasterList(oList);
				}.bind(this)
			});

			this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
			this.getRouter().attachBypassed(this.onBypassed, this);
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */
		/**
		 * After list data is available, this handler method updates the
		 * master list counter and hides the pull to refresh control, if
		 * necessary.
		 * @param {sap.ui.base.Event} oEvent the update finished event
		 * @public
		 */
		onUpdateFinished: function(oEvent) {
			// update the master list object counter after new data is loaded
			this._updateListItemCount(oEvent.getParameter("total"));
			// hide pull to refresh if necessary
			this.byId("pullToRefresh").hide();
			// console.log("-->");
			// console.log(oEvent);

		},
		/**
		 * Event handler for the master search field. Applies current
		 * filter value and triggers a new search. If the search field's
		 * 'refresh' button has been pressed, no new search is triggered
		 * and the list binding is refresh instead.
		 * @param {sap.ui.base.Event} oEvent the search event
		 * @public
		 */
		onSearch: function(oEvent) {
			if (oEvent.getParameters().refreshButtonPressed) {
				// Search field's 'refresh' button has been pressed.
				// This is visible if you select any master list item.
				// In this case no new search is triggered, we only
				// refresh the list binding.
				this.onRefresh();
				return;
			}
			var sQuery = oEvent.getParameter("query");
			var userId = this.getModel("appView").getProperty("/issuedBy");

			sQuery = sQuery.toUpperCase();

			if (this.getResourceBundle().getText("appVersion") === "1") {
				if (sQuery) {
					this._oListFilterState.aSearch = [new Filter(this._oSearchParameter, sap.ui.model.FilterOperator.Contains,
						sQuery), new Filter("IssuedBy", sap.ui.model.FilterOperator.EQ, userId)];
				} else {
					this._oListFilterState.aSearch = [new Filter("IssuedBy", sap.ui.model.FilterOperator.EQ, userId)];
				}
			} else {
				if (sQuery) {
					this._oListFilterState.aSearch = [new Filter(this._oSearchParameter, sap.ui.model.FilterOperator.Contains,
						sQuery)];
				} else {
					this._oListFilterState.aSearch = [];
				}
			}

			this._applyFilterSearch();
		},
		/**
		 * Event handler for refresh event. Keeps filter, sort
		 * and group settings and refreshes the list binding.
		 * @public
		 */
		onRefresh: function() {
			this._oList.getBinding("items").refresh();
		},
		/**
		 * Event handler for the list selection event
		 * @param {sap.ui.base.Event} oEvent the list selectionChange event
		 * @public
		 */
		onSelectionChange: function(oEvent) {
			var oViewApp = this.getView().getModel("appView");
			if (oViewApp.getProperty("/blockForms")) {
				// get the list item, either from the listItem parameter or from the event's source itself (will depend on the device-dependent mode).
				this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
				this.onChanceValueBusyIndicator(true);
			} else {
				sap.m.MessageBox.error(this.getResourceBundle().getText("messageCantChanceSelect"), {
					title: "Error",
					textDirection: sap.ui.core.TextDirection.Inherit
				});
			}
		},
		/**
		 * Event handler for the bypassed event, which is fired when no routing pattern matched.
		 * If there was an object selected in the master list, that selection is removed.
		 * @public
		 */
		onBypassed: function() {
			this._oList.removeSelections(true);
		},
		/**
		 * Used to create GroupHeaders with non-capitalized caption.
		 * These headers are inserted into the master list to
		 * group the master list's items.
		 * @param {Object} oGroup group whose text is to be displayed
		 * @public
		 * @returns {sap.m.GroupHeaderListItem} group header with non-capitalized caption.
		 */
		createGroupHeader: function(oGroup) {
			return new GroupHeaderListItem({
				title: oGroup.text,
				upperCase: false
			});
		},
		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */
		_createViewModel: function() {
			return new JSONModel({
				isFilterBarVisible: false,
				filterBarLabel: "",
				delay: 0,
				title: this.getResourceBundle().getText("masterTitleCount", [0]),
				noDataText: this.getResourceBundle().getText("masterListNoDataText"),
				sortBy: "NameFirst",
				groupBy: "None"
			});
		},
		onFailGenericPromise: function(oReason) {
			console.log("--> Reason: ");
			this.onChanceValueBusyIndicator(false);
			console.log(oReason);
		},
		/**
		 * If the master route was hit (empty hash) we have to set
		 * the hash to to the first item in the list as soon as the
		 * listLoading is done and the first item in the list is known
		 * @private
		 */
		_onMasterMatched: function(oEvent) {
			// console.log("-->");
			// console.log(this.getView().getModel().oData);

			var oViewApp = this.getView().getModel("appView");
			var oOdataUtility = new OdataUtility();
			var requestIssuedBy;
			var oController = this;
			if (oViewApp.getProperty("/issuedBy") === "") {

				requestIssuedBy = oOdataUtility.requestIssuedBySet(this);
				requestIssuedBy.catch(this.onFailGenericPromise.bind(this));
			}
			if (typeof(requestIssuedBy) === "object") {
				requestIssuedBy.then(oOdataUtility.requestFindUserByVisit.bind(this, oController))
					.then(function(res) {
						if (res.Parva !== "X") {
							// this.byId("idAppControl").hideMaster();
							oViewApp.setProperty("/availableVisibleMaster", false);
							oController.getRouter().getTargets().display("detailObjectNotFound");
						}
					});
			}

			// var requestFindUserByVisit = oOdataUtility.requestFindUserByVisit(this);
			// requestFindUserByVisit
			// 	.then(function(res) {
			// 		if (res.Parva !== "X" || res.Parva !== "x") {
			// 			oController.getRouter().getTargets().display("detailObjectNotFound");
			// 		}
			// 	});
			var oEventId = oEvent.id;

			// if(oEventId){

			// }

			this.getOwnerComponent().oListSelector.oWhenListLoadingIsDone.then(function(mParams) {
				if (mParams.list.getMode() === "None") {
					return;
				}
				if (oEventId === undefined) {
					var sObjectId = mParams.firstListitem.getBindingContext().getProperty("Passportid");
					this.getRouter().navTo("object", {
						objectId: sObjectId
					}, true);
				}
				this.onChanceValueBusyIndicator(false);

			}.bind(this), function(mParams) {
				if (mParams.error) {
					return;
				}
				this.onChanceValueBusyIndicator(false);

				// if (oEventId === undefined) {
				// 	// var sObjectId = mParams.firstListitem.getBindingContext().getProperty("Passportid");
				// 	this.getRouter().navTo("object", {
				// 		objectId: 0
				// 	}, true);
				// }

				var validatePhone = this.getModel("device").getProperty("/system/desktop");

				if (validatePhone) {

					this.getRouter().getTargets().display("detailNoObjectsAvailable");
				}

				// 
			}.bind(this));
		},
		/**
		 * Shows the selected item on the detail page
		 * On phones a additional history entry is created
		 * @param {sap.m.ObjectListItem} oItem selected Item
		 * @private
		 */
		_showDetail: function(oItem) {
			var bReplace = !Device.system.phone;
			this.getRouter().navTo("object", {
				objectId: oItem.getBindingContext().getProperty("Passportid")
			}, bReplace);
		},
		/**
		 * Sets the item count on the master list header
		 * @param {integer} iTotalItems the total number of items in the list
		 * @private
		 */
		_updateListItemCount: function(iTotalItems) {
			var sTitle;
			// only update the counter if the length is final
			if (this._oList.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("masterTitleCount", [iTotalItems]);
				this.getModel("masterView").setProperty("/title", sTitle);
			}
		},
		/**
		 * Internal helper method to apply both filter and search state together on the list binding
		 * @private
		 */
		_applyFilterSearch: function() {
			var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
				oViewModel = this.getModel("masterView");
			this._oList.getBinding("items").filter(aFilters, "Application");
			// changes the noDataText of the list in case there are no filter results
			if (aFilters.length !== 0) {
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText(
					"masterListNoDataWithFilterOrSearchText"));
			} else if (this._oListFilterState.aSearch.length > 0) {
				// only reset the no data text to default when no new search was triggered
				oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
			}
		},
		/**
		 * Internal helper method to apply both group and sort state together on the list binding
		 * @param {sap.ui.model.Sorter[]} aSorters an array of sorters
		 * @private
		 */
		_applyGroupSort: function(aSorters) {
			this._oList.getBinding("items").sort(aSorters);
		},
		/**
		 * Internal helper method that sets the filter bar visibility property and the label's caption to be shown
		 * @param {string} sFilterBarText the selected filter value
		 * @private
		 */
		_updateFilterBar: function(sFilterBarText) {
			var oViewModel = this.getModel("masterView");
			oViewModel.setProperty("/isFilterBarVisible", this._oListFilterState.aFilter.length > 0);
			oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [
				sFilterBarText]));
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Master
		 */
		onCreateProfile: function() {
			// var oViewApp = this.getView().getModel("appView");
			// var oViewApp = this.getView().getModel("appView");
			// var issuedBy = "";
			// oViewApp.setProperty("/titleDetail", this.getResourceBundle().getText("titleCreateProfile"));

			// // var sUrl = "/IssuedBySet";
			// // var oModel = this.getView().getModel();
			// // oModel.read(sUrl, {
			// // 	success: jQuery.proxy(function(mResponse) {
			// // 		console.log(mResponse);
			// // 	}, this),
			// // 	error: jQuery.proxy(function(mResponse) {
			// // 		console.log(mResponse);
			// // 	}, this)
			// // });

			// if (oViewApp.getProperty("/issuedBy") === "") {
			// 	$.each(this.getView().getModel().oData, function(i) {
			// 		if (i.indexOf("IssuedBySet") !== -1) {
			// 			// console.log("--> Id " + i);
			// 			issuedBy = i;
			// 			return false;
			// 		}
			// 	});
			// 	issuedBy = issuedBy.replace("IssuedBySet('", "");
			// 	issuedBy = issuedBy.replace("')", "");
			// 	oViewApp.setProperty("/issuedBy", issuedBy); // console.log(oViewApp.getProperty("/issuedBy"));
			// }
			// oViewApp.setProperty("/blockForms", false);
			// oViewApp.setProperty("/configDetailProfile/businessCardVisible", true);
			// oViewApp.setProperty("/configDetailCreteProfile/businessCardVisible", false);
			// oViewApp.setProperty("/messages/messageToastSelect", "Por favor capture los datos iniciales.");
			this.onChanceValueBusyIndicator(true);
			var bReplace = !Device.system.phone;
			this.getRouter().navTo("object", {
				objectId: "0"
			}, bReplace); //This code was generated by the layout editor.
		},
		// onAfterRendering: function() {
		// 	console.log("-->Termino de renderizar");
		// },
		/**
		 *@memberOf com.gc.passportvisits.controller.Master
		 */
		chanceValue: function(oEvent) {

			var oViewApp = this.getView().getModel("appView");
			if (oEvent.getSource().getText().trim() === "USUARIO DE LECTURA") {
				oViewApp.setProperty("/availableEdit", false);
			} // console.log("cambio");
			//This code was generated by the layout editor.
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Master
		 */
		onChoseObjectSearch: function(oEvent) {
			var oButton = oEvent.getSource().getSelectedButton();
			// console.log("-->select?");

			if (oButton.includes("btnSearchId")) {
				this._oSearchParameter = "Passportid";
			} else {
				this._oSearchParameter = "NameText";
			}
			//This code was generated by the layout editor.
		},

		onBulkProfilesInViews: function(_oResponse) {

			this.fragmentBulkProfile = "";
			this.onChanceValueBusyIndicator(true);
			if (this.fragmentBulkProfile === "") {
				// this._oDialog = sap.ui.xmlfragment("sap.m.sample.SelectDialog.Dialog", this);
				var frgStr = "com.gc.passportvisits.view.fragments.BulkProfile";
				this.fragmentBulkProfile = sap.ui.xmlfragment(this.getView().getId(), frgStr, this);
				this.fragmentBulkProfile.setModel(this.getView().getModel());
				this.getView().addDependent(this.fragmentBulkProfile);
			}
			this.fragmentBulkProfile.open();
			this.fragmentBulkProfile.attachAfterClose(function(oEvent) {
				this.onCloseDialogBulkProfile();
			}.bind(this));
		},
		onCloseDialogBulkProfile: function() {
			var oViewApp = this.getView().getModel("appView");
			oViewApp.setProperty("/showBulkData", false);
			this.fragmentBulkProfile.close();
			this.fragmentBulkProfile.destroy();
			this.fragmentBulkProfile = "";
			// console.log("cerando")
			this.onChanceValueBusyIndicator(false);
		},
		handleValueChange: function(oEvent) {
			var oController = this;
			var oViewApp = oController.getView().getModel("appView");
			oViewApp.setProperty("/showBulkData", false);
			oController.onChanceValueBusyIndicator(true);
			var oFile = oEvent.getParameters().files[0];

			var reader = new FileReader();

			reader.onload = function(e) {
				var data = e.target.result;
				// var XLS = new XLSX();

				try {
					var cfb = XLS.CFB.read(data, {
						type: 'binary'
					});
					var wb = XLS.parse_xlscfb(cfb);

					wb.SheetNames.forEach(function(sheetName) {
						// var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
						var oJS = XLS.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);
						// console.log(sCSV)
						// console.log("----------------")

						// console.log(oJS)
						oController.onChanceValueBusyIndicator(false);
						oViewApp.setProperty("/showBulkData", true);
						oViewApp.setProperty("/showCountRowsInTable", (oJS.length));
						var bulkModel = new JSONModel({
							"oJS": oJS
						});

						oController.setModel(bulkModel, "DataBulk");

						// var bulkModel = new JSONModel({
						// 	NameFirst: "namefirs"

						// });

						// oController.setModel(bulkModel, "dataBulk");
					});
				} catch (err) {
					// return
					sap.m.MessageToast.show("El formato debe ser excel 97-2003", {
						duration: 5000,
						width: "25%",
						animationTimingFunction: "ease-in",
						animationDuration: 4000
					});
					// console.log(err);
				}

			};

			reader.readAsBinaryString(oFile);

		},
		handleUploadPress: function(oEvent) {
			this.onChanceValueBusyIndicator(true);
			var oViewApp = this.getView().getModel("appView");
			oViewApp.setProperty("/showBulkData", false);
			var oModel = this.getView().getModel();
			oModel.read("/CountrySet", {
				success: jQuery.proxy(function(mResponse) {
					this.setModel(mResponse, "CountryClone");
					this.onCloseDialogBulkProfile();
					this.onShowBulkData();
					// console.log(mResponse);
				}, this),
				error: jQuery.proxy(function(mResponse) {
					// console.log("error", mResponse);
				}, this)
			});

			// console.log("show information");
			// var oFileUploader = this.byId("fileUploader");
			// oFileUploader.upload();
		},

		onShowBulkData: function() {

			this.fragmentShowBulkProfile = "";
			this.onChanceValueBusyIndicator(true);
			if (this.fragmentShowBulkProfile === "") {
				var frgStr = "com.gc.passportvisits.view.fragments.ShowBulkProfile";
				this.fragmentShowBulkProfile = sap.ui.xmlfragment(this.getView().getId(), frgStr, this);
				// this.byId("tableBlulk").setModel(this.getModel("DataBulk"));
				this.fragmentShowBulkProfile.setModel(this.getModel("DataBulk"));
				this.fragmentShowBulkProfile.setModel(this.getModel(), "CoreModel");
				// this.fragmentShowBulkProfile.setModel(this.getModel("DataBulk"));

				this.getView().addDependent(this.fragmentShowBulkProfile);
			}

			// this.fragmentShowBulkProfile.attachAfterClose(function(oEvent) {
			// 	this.onCloseDialogfragmentShowBulkProfile();
			// }.bind(this));

			// this.byId("tableBlulk").setAlternateRowColors(true);

			this.fragmentShowBulkProfile.attachAfterClose(function(oEvent) {
				this.onCloseDialogfragmentShowBulkProfile();
			}.bind(this));
			// this.onChanceValueBusyIndicator(false);
			this.setModel(new JSONModel({
				"EndProcess": false
			}), "DataBulkFlag");
			this.fragmentShowBulkProfile.open();
			this.onChanceValueBusyIndicator(false);
		},
		saveModelDataBulk: function() {
			this.onChanceValueBusyIndicator(true);
			this.byId("dialogTableBulk").setBusy(true);

			// MessageToast.show("Validando duplicidad", {
			// 	duration: 5000,
			// 	width: "25%",
			// 	animationTimingFunction: "ease-in",
			// 	animationDuration: 4000
			// });

			var oValidatorForm = new com.gc.passportvisits.js.libs.ValidatorForm()
				// var oValidatorForm = new ValidatorForm()

			if (oValidatorForm.validateFormProfileBulkData(this)) {

				this.validateDuplicity(0);
				// this.onChanceValueBusyIndicator(false);
			} else {
				this.byId("dialogTableBulk").setBusy(false);
				this.onChanceValueBusyIndicator(false);
			}

		},

		onCloseDialogfragmentShowBulkProfile: function() {
			this.fragmentShowBulkProfile.close();
			this.fragmentShowBulkProfile.destroy();
			this.fragmentShowBulkProfile = "";
			// console.log("cerando")
			this.onChanceValueBusyIndicator(false);
		},
		validateDuplicity: function(_cont) {

			// var oController = this;
			var contIni = _cont;

			var oModelDatabulk = this.getModel("DataBulk");
			var itemEvalute = oModelDatabulk.getProperty("/oJS/" + contIni);
			var endValidate = oModelDatabulk.getProperty("/oJS").length;
			var oOdataUtility = new OdataUtility();
			if (contIni !== endValidate) {
				// var oOdataUtility = new OdataUtility();
				var validatorUidSet;
				// var oController = this;

				var requestJsonArr = {
					"correct": false,
					"uidData": itemEvalute.UidData,
					"optionNal": itemEvalute.Uidtype,
					"message": ""
				};

				this.getModel("DataBulkFlag").setProperty("/" + contIni, requestJsonArr);
				this.getModel("DataBulkFlag").setProperty("/CurrentEval", contIni);

				validatorUidSet = oOdataUtility.requestUidSetBulk(requestJsonArr, this)
					.then(this.evalResponseUidSet.bind(this))
					.then(oOdataUtility.requestCreateProfileBulk.bind(this))
					.then(function(results) {
						var count = this.getModel("DataBulkFlag").getProperty("/CurrentEval")

						this.getModel("DataBulk").setProperty("/oJS/" + count + "/correct", this.getModel("DataBulkFlag").getProperty(
							"/" + count + "/correct"));

						if (this.getModel("DataBulk").getProperty("/oJS/" + count + "/correct")) {
							this.getModel("DataBulk").setProperty("/oJS/" + count + "/Passportid-Log", results.Passportid);

							this.getModel("DataBulkFlag").setProperty("/" + count + "/createdItem", results);
						} else {
							this.getModel("DataBulk").setProperty("/oJS/" + count + "/Passportid-Log", this.getModel("DataBulkFlag").getProperty(
								"/" + count + "/message"));
							this.getModel("DataBulk").setProperty("/oJS/" + count + "/correct", this.getModel("DataBulkFlag").getProperty(
								"/" + count + "/correct"));
							this.getModel("DataBulk").setProperty("/oJS/" + count + "/classStyle", "itemTableRed");
						}

						count++;

						// var oOdataUtility = new OdataUtility();

						// var visa_Payload = {
						// 	Flight: "R",
						// 	Siteid: "CN",
						// 	LunchService: "R",
						// 	CampService: "R",
						// 	Transportation: "R",
						// 	InductionCourse: "R",
						// 	IssuedDate: new Date(),
						// 	ExpiryDate: new Date("9999-12-31"),
						// 	Passportid:results.Passportid
						// };
						// oOdataUtility.requestCreateVisaBulk(this, oOdataUtility, visa_Payload);
						this.validateDuplicity(count);
					}.bind(this))
					.catch(function(result) {

						this.getModel("DataBulk").setProperty("/oJS/" + result + "/Passportid-Log", this.getModel("DataBulkFlag").getProperty(
							"/" + result + "/message"));
						this.getModel("DataBulk").setProperty("/oJS/" + result + "/correct", this.getModel("DataBulkFlag").getProperty(
							"/" + result + "/correct"));
						this.getModel("DataBulk").setProperty("/oJS/" + result + "/classStyle", "itemTableRed");
						result++;
						this.validateDuplicity(result);

						console.log("--> error rejected duplicity")
						console.log(result)
					}.bind(this));
			} else {

				// this.byId("tableBlulk")
				this.blockTable();
				this.getModel("DataBulkFlag").setProperty("/EndProcess", true);
				this.byId("dialogTableBulk").setBusy(false);
				this.onChanceValueBusyIndicator(false);
			}

		},
		evalResponseUidSet: function(_oResponse) {

			var oResponse = _oResponse;
			var currentEval = this.getModel("DataBulkFlag").getProperty("/CurrentEval");
			var oController = this;
			return new Promise(function(resolve, reject) {
				switch (oResponse.EReturn.LogNo) {
					case "0":
					case "":
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/correct", true);
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/message", 0);
						resolve(currentEval);
						break;
					case "1":

						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/correct", false);
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/message", oController.getResourceBundle()
							.getText("activeDuplicatedA") + " " + oResponse.EProfile.Passportid);
						// frgStr = "com.gc.passportv2.view.fragments.PanelDuplicatedProfile";
						reject(currentEval);

						break;
					case "2":
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/correct", false);
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/message", oController.getResourceBundle()
							.getText("activeDuplicatedI") + " " + oResponse.EProfile.Passportid);
						// frgStr = "com.gc.passportv2.view.fragments.PanelDuplicatedProfileUpdate";
						reject(currentEval);

						break;
					case "3":
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/correct", false);
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/message", oController.getResourceBundle()
							.getText("activeDuplicatedE") + " " + oResponse.EProfile.Passportid);
						// msgCurp = this.getResourceBundle().getText("messageErrorCauseEmployee");
						reject(currentEval);

						break;
					case "99":
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/correct", false);
						oController.getModel("DataBulkFlag").setProperty("/" + currentEval + "/message", this.getResourceBundle().getText(
							"activeDuplicatedBl") + " " + oResponse.EProfile.Passportid);
						// msgCurp = this.getResourceBundle().getText("messageErrorCauseBlackList");
						reject(currentEval);

						break;
				}
				// currentEval++;

			});

		},
		onChangeCountry: function(oEvent) {
			var formatCountry = "";
			var country = this.getModel("DataBulk").getProperty(oEvent.getSource().getBindingContext()
				.getPath() + "/Country");
			if (country) {
				if (country.length >= 2) {
					if (country.length > 2) {
						formatCountry = this.formaterCountry(country);
						this.getModel("DataBulk").setProperty(oEvent.getSource().getBindingContext().getPath() + "/Country",
							formatCountry);
					}
				}
			}

			// 			oEvent.getSource().getSelectedKey()
			// "MX"
			// this.getModel("DataBulk").getProperty(oEvent.getSource().getBindingContext().getPath()+"/Country")

			// if (oEvent.getSource().getSelectedKey() !== "") {
			// 	this.getModel("DataBulk").setProperty(oEvent.getSource().getBindingContext().getPath() + "/Country", oEvent.getSource()
			// 		.getSelectedKey());
			// }
			// console.log("Evento " + oEvent);

		},
		formaterCountry: function(_value) {
			// this.getModel("CountryClone");
			var valueItem = _value;

			var itemCountry = _.filter(this.getModel("CountryClone").results, function(value, index) {
				var valueCountry = value.Landx.toUpperCase().trim().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
				var valueEval = valueItem.toUpperCase().trim().normalize('NFD').replace(/[\u0300-\u036f]/g, "");

				if (valueCountry === valueEval) {
					return true;
				}
			});

			// console.log(itemCountry);
			// console.log(itemCountry[0].Land1);

			if (itemCountry[0]) {
				return itemCountry[0].Land1;
			} else {
				return "";
			}
			// console.log(itemCountry[0]);

		},
		blockTable: function() {
			_.each(this.byId("tableBlulk").getRows(), function(value, key, list) {
				_.each(value.getCells(), function(value, key, list) {
					value.setEnabled(false);
				});
			});
			// this.byId("tableBlulk").getRows();
		},

	});
});