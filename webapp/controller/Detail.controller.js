/*global location */
sap.ui.define([
	"com/gc/passportvisits/controller/BaseController",
	"com/gc/passportvisits/js/libs/BaseUtility",
	"com/gc/passportvisits/js/libs/BaseUtilityComponent",
	"com/gc/passportvisits/js/vendor/underscore",
	"sap/ui/model/json/JSONModel",
	"com/gc/passportvisits/model/formatter",
	"sap/m/MessageToast",
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/ui/Device",
	"com/gc/passportvisits/js/libs/ValidatorForm",
	"com/gc/passportvisits/js/libs/OdataUtility",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(BaseController, BaseUtility, BaseUtilityComponent, underscore, JSONModel, formatter, MessageToast,
	Controller, MessageBox, Device,
	ValidatorForm, OdataUtility, Filter, FilterOperator) {
	"use strict";
	return BaseController.extend("com.gc.passportvisits.controller.Detail", {
		formatter: formatter,
		fragmentCreateAndReadVisa: "",
		fragmentShowDuplicatedProfile: "",
		flagTakeProfile: false,
		fragmentDialogCreateNewRequest: "",
		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */
		onInit: function() {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			// var oViewModel = new JSONModel({
			// 	busy: true,
			// 	delay: 0
			// });
			this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this); // var appViewBusy = this.getView().getModel("appViewBusy");
			// appViewBusy.setProperty("/busy", true);
			// this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded
			// .bind(this));
		},
		onAfterRendering: function() {
			this.byId("inputBirthdate").attachBrowserEvent("keypress", function() {
				var idComplete = this.getId() + "-icon";
				var idCompleteInner = this.getId() + "-inner";
				$("#" + idCompleteInner).css("pointer-events", "none");
				$("#" + idComplete).on("click", function() {
					$("#" + idComplete).off("click");
				});
				$("#" + idComplete).click();
			});
			this.byId("inputBirthdate").setMaxDate(new Date());
			this.getView().getModel().setSizeLimit(10000000);
			this.getView().getModel().refresh(); // console.log("-->OnBefoeRenderinf");
		},
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */
		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */
		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function(oEvent) {
			var sObjectId;
			sObjectId = oEvent.getParameter("arguments").objectId;
			// var oViewApp = this.getView().getModel("appView");
			// // if (this.getResourceBundle().getText("appVersion") === "0") {
			// if (oViewApp.getProperty("/issuedBy") === "") {
			// 	var oOdataUtility = new OdataUtility();
			// 	var requestIssuedBy = oOdataUtility.requestIssuedBySet(this);
			// 	requestIssuedBy.catch(this.onFailGenericPromise.bind(this));
			// }
			var oViewApp = this.getView().getModel("appView");
			var oOdataUtility = new OdataUtility();
			var requestIssuedBy;
			var oController = this;
			if (oViewApp.getProperty("/issuedBy") === "") {
				requestIssuedBy = oOdataUtility.requestIssuedBySet(this);
				requestIssuedBy.catch(this.onFailGenericPromise.bind(this));
			}
			if (typeof requestIssuedBy === "object") {
				requestIssuedBy.then(oOdataUtility.requestFindUserByVisit.bind(this, oController)).then(function(res) {
					if (res.Parva !== "X") {
						oViewApp.setProperty("/availableVisibleMaster", false);
						oController.getRouter().getTargets().display("detailObjectNotFound");
					}
				});
			}
			// this.byId("inputBirthdate").attachBrowserEvent("onfocus", function(oEvent) {
			// 	// do whatever
			// 	console.log("--> gime the calendar");
			// });
			// }
			this.getModel().metadataLoaded().then(this.initDetailProfile.bind(this, sObjectId)).then(this._bindView.bind(
				this)).then(this._onMetadataLoaded.bind(this)).catch(this.onFailGenericPromise.bind(this));
		},
		initDetailProfile: function(_sObjectId) {
			var sObjectId = _sObjectId;
			var oController = this;
			return new Promise(function(resolve, reject) {
				var oViewApp = oController.getView().getModel("appView");
				var oOdataUtility = new OdataUtility();
				// oViewApp.setProperty("/titleDetail", oController.getResourceBundle().getText("titleCreateProfile"));
				if (sObjectId === "0") {
					oViewApp.setProperty("/titleDetail", oController.getResourceBundle().getText("titleCreateProfile"));
					oViewApp.setProperty("/availableButtonCreateProfile", false);
					oViewApp.setProperty("/blockForms", false);
					oController.getView().getModel().oData["PROFILESet('0')"] = oOdataUtility.getEmptyProfileModel(); // oController.setFormsToCreateProfile();
				} else {
					oViewApp.setProperty("/titleDetail", oController.getResourceBundle().getText("titleDisplayProfile"));
					oViewApp.setProperty("/availableButtonCreateProfile", true);
					oViewApp.setProperty("/blockForms", true);
				}
				if (oController.flagTakeProfile) {
					oController.onCloseDialogDuplicated();
					oController.closeConfirmEdit("OK");
				}
				oViewApp.setProperty("/configDetailProfile/businessCardVisible", true);
				oViewApp.setProperty("/configDetailCreteProfile/businessCardVisible", false);
				oViewApp.setProperty("/messages/messageToastSelect", "Por favor capture los datos iniciales.");
				// var sObjectPath = oController.getModel().createKey("PROFILESet", {
				// 	Passportid: sObjectId
				// });
				var sObjectPath;
				if (oController.getModel().getProperty("/PROFILESet('" + sObjectId + "')")) {
					sObjectPath = oController.getModel().createKey("PROFILESet", {
						Passportid: sObjectId
					});
					resolve("/" + sObjectPath);
				} else {
					if (oController.flagTakeProfile) {
						sObjectPath = oController.getModel().createKey("PROFILESet", {
							Passportid: sObjectId
						});
						resolve("/" + sObjectPath);
					} else {
						reject("0");
					}
				} // resolve("/" + sObjectPath);
			});
		},
		_onMetadataLoaded: function() {
			// var appViewBusy = this.getView().getModel("appViewBusy");
			// appViewBusy.setProperty("/busy", false);
			this.onChanceValueBusyIndicator(false); // appViewBusy.setProperty("/delay", 0);
		},
		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function(_oObjectPath) {
			var oController = this;
			var oObjectPath = _oObjectPath;
			return new Promise(function(resolve, reject) {
				// Set busy indicator during view binding
				// var oViewModel = oController.getModel("detailView");
				// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
				// oViewModel.setProperty("/busy", false);
				var oSettingData = oController.setBindingView(oObjectPath);
				oSettingData.then(oController.getToVisaData.bind(oController)).then(oController.setToVisaData.bind(
					oController)).then(resolve).catch(oController.onFailGenericPromise.bind(oController));
			});
		},
		getToVisaData: function() {
			var oController = this;
			return new Promise(function(resolve, reject) {
				var oView = oController.getView(),
					oElementBinding = oView.getElementBinding();
				var oOdataUtility = new OdataUtility();
				var requestVisa;
				if (oElementBinding.getPath() === "/PROFILESet('0')") {
					// reject("This profile does not have /toVisa it is new profile");
					requestVisa = oOdataUtility.getEmptyVisaModel();
					resolve(requestVisa);
				} else {
					requestVisa = oOdataUtility.requestVisaSetFromNavigationTovisa(oController, oElementBinding.getPath());
					requestVisa.then(resolve).catch(oController.onFailGenericPromise.bind(oController));
				}
			});
		},
		setToVisaData: function(_oResults) {
			var oResults = _oResults;
			var oController = this;
			return new Promise(function(resolve, reject) {
				var oModel = new JSONModel(oResults);
				oController.setModel(oModel, "oModelVisaSet");
				resolve();
			});
		},
		setBindingView: function(oObjectPath) {
			var oController = this;
			var sObjectPath = oObjectPath;
			return new Promise(function(resolve, reject) {
				// var appViewBusy = oController.getModel("appViewBusy");
				// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
				// appViewBusy.setProperty("/busy", false);
				oController.getView().bindElement({
					path: sObjectPath,
					events: {
						change: oController._onBindingChange.bind(oController),
						dataRequested: function() {
							// appViewBusy.setProperty("/busy", true);
							oController.onChanceValueBusyIndicator(true);
						},
						dataReceived: function() {
							oController.onChanceValueBusyIndicator(false);
						}
					}
				});
				resolve("ok");
			});
		},
		changeValueState: function() {
			this.byId("typeUidDataGroup").setValueState("None");
			this.byId("typeSex").setValueState("None");
			this.byId("inputNameFirst").setValueState("None");
			this.byId("inputNamemiddle").setValueState("None");
			this.byId("inputNameLast").setValueState("None");
			this.byId("inputNameLast2").setValueState("None");
			this.byId("inputNickname").setValueState("None");
			this.byId("inputStreet").setValueState("None");
			this.byId("HouseNum1").setValueState("None");
			this.byId("inputBirthdate").setValueState("None");
			// this.byId("inputPOCombo").setValueState("None");
			// this.byId("inputCountry").setValueState("None");
			this.byId("inputCity1").setValueState("None");
			this.byId("inputPostCode1").setValueState("None");
			this.byId("inputEmail").setValueState("None");
			this.byId("inputTelNumber").setValueState("None");
			this.byId("inputUidData").setValueState("None");
			this.byId("inputImss").setValueState("None");
			this.byId("inputDriverLicense").setValueState("None");
			this.byId("inputContactRelationship").setValueState("None");
			this.byId("inputContactName").setValueState("None");
			this.byId("inputContactPhone").setValueState("None");
		},
		_onBindingChange: function() {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}
			this.changeValueState();
			var oViewApp = this.getView().getModel("appView");
			var sPath = oElementBinding.getPath(),
				oObject = oView.getModel().getObject(sPath),
				sObjectStatus = oObject.Status;
			if (sObjectStatus === "I") {
				oViewApp.setProperty("/statusActiveModel", false);
				oViewApp.setProperty("/statusActiveModel_", "I");
			}
			if (sObjectStatus === "A" || sObjectStatus === "") {
				oViewApp.setProperty("/statusActiveModel", true);
				oViewApp.setProperty("/statusActiveModel_", "A");
			}
			var valueModelChkEjido = oElementBinding.getModel().getProperty(oElementBinding.getPath() + "/ChkEjido");
			this.getOwnerComponent().oListSelector.selectAListItem(sPath);
			if (oElementBinding.getPath() === "/PROFILESet('0')") {
				this.setFormsToCreateProfile();
			} else {
				var dateFormat = new Date(oElementBinding.getModel().getProperty(oElementBinding.getPath() + "/Birthdate"));
				if (oElementBinding.getModel().getProperty(oElementBinding.getPath() + "/Birthdate") !== "") {
					if (dateFormat.toJSON().indexOf("T00:00:00.000Z") !== -1) {
						oElementBinding.getModel().setProperty(oElementBinding.getPath() + "/Birthdate", dateFormat.toJSON().replace(
							"T00:00:00.000Z", ""));
					} else {
						oElementBinding.getModel().setProperty(oElementBinding.getPath() + "/Birthdate", dateFormat.toJSON().replace(
							"T06:00:00.000Z", ""));
					}
				}
				if (oElementBinding.getModel().getProperty(oElementBinding.getPath() + "/Uidtype") === "C" || oElementBinding.getModel()
					.getProperty(oElementBinding.getPath() + "/Uidtype") === "") {
					this.getView().byId("typeUidDataGroup1").setSelectedIndex(0);
					oViewApp.setProperty("/optionNal", "C");
					oViewApp.setProperty("/configDetailProfile/labelUid", "CURP");
				} else {
					this.getView().byId("typeUidDataGroup1").setSelectedIndex(1);
					oViewApp.setProperty("/optionNal", "P");
					oViewApp.setProperty("/configDetailProfile/labelUid", this.getResourceBundle().getText("pasportId"));
				}
				if (oElementBinding.getModel().getProperty(oElementBinding.getPath() + "/Sex") === "1" || oElementBinding.getModel()
					.getProperty(oElementBinding.getPath() + "/Sex") === "") {
					this.getView().byId("typeSex").setSelectedIndex(0);
					this.setValueToBindingModel("Sex", "1");
				} else {
					this.getView().byId("typeSex").setSelectedIndex(1);
					this.setValueToBindingModel("Sex", "2");
				}
				if (valueModelChkEjido === "X") {
					oViewApp.setProperty("/isEjidatario", 0);
					this.setValueToBindingModel("ChkEjido", "X");
				} else {
					oViewApp.setProperty("/isEjidatario", 1);
					this.setValueToBindingModel("ChkEjido", "");
				}
				if (oElementBinding.getModel().getProperty(oElementBinding.getPath() + "/Country") !== "") {
					this.getView().byId("inputCountry").setSelectedKey(oElementBinding.getModel().getProperty(oElementBinding.getPath() +
						"/Country"));
				}
				oViewApp.setProperty("/configDetailProfile/businessCardVisible", true);
				oViewApp.setProperty("/configDetailProfile/showInitTab", "tabInfo");
				oViewApp.setProperty("/configDetailProfile/validatePersonalId", false);
				oViewApp.setProperty("/configDetailProfile/showSelectedPO", false);
				oViewApp.setProperty("/configDetailProfile/showSelectedPOAlter", true);
				this.getView().byId("tabPO").setVisible(true); // this.getView().byId("__component0---detail--tabPO").setVisible(true);
			}
			var tabIconBar = this.getView().byId("detailIconTabBar");
			// tabIconBar.setSelectedKey(oViewApp.getProperty(
			// "/configDetailProfile/showInitTab"));
			if (oViewApp.getProperty("/configDetailProfile/creating")) {
				oViewApp.setProperty("/configDetailProfile/creating", false);
				oViewApp.setProperty("/configDetailProfile/showInitTab", "tabPO");
			}
			// tabIconBar.setSelectedKey(oViewApp.getProperty("/configDetailProfile/showInitTab")); // console.log("-->icontabbar " + tabIconBar);
			tabIconBar.setSelectedKey(this.byId(oViewApp.getProperty("/configDetailProfile/showInitTab")).getId());
		},
		setFormsToCreateProfile: function() {
			var oViewApp = this.getView().getModel("appView");
			oViewApp.setProperty("/configDetailProfile/labelUid", "CURP");
			oViewApp.setProperty("/configDetailProfile/lengthToUuid", 18);
			oViewApp.setProperty("/optionNal", "C");
			oViewApp.setProperty("/isEjidatario", -1);
			this.getView().byId("typeUidDataGroup1").setSelectedIndex(0);
			oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
			oViewApp.setProperty("/configDetailProfile/showAllTabs", false);
			oViewApp.setProperty("/configDetailProfile/validatePersonalId", true);
			oViewApp.setProperty("/configDetailProfile/businessCardVisible", false);
			oViewApp.setProperty("/configDetailProfile/validatePO", false);
			oViewApp.setProperty("/configDetailProfile/showSelectedPO", true);
			oViewApp.setProperty("/configDetailProfile/showSelectedPOAlter", false);
			oViewApp.setProperty("/configDetailProfile/showSelectionList", "Detail");
			oViewApp.setProperty("/configDetailProfile/showInitTab", "tabIdPersonal");
			this.getView().byId("buttonSave").setVisible(false);
			this.getView().byId("buttonCancel").setVisible(true);
			this.getView().byId("buttonUpdate").setVisible(false);
			this.getView().byId("buttonEdit").setVisible(false);
			this.getView().byId("typeUidDataGroup").setSelectedIndex(0).setValueState("None");
			this.getView().byId("typeSex").setSelectedIndex(0).setValueState("None");
			this.byId("inputNameFirst").setValue("").setValueState("None");
			this.byId("inputNamemiddle").setValue("").setValueState("None");
			this.byId("inputNameLast").setValue("").setValueState("None");
			this.byId("inputNameLast2").setValue("").setValueState("None");
			this.byId("inputNickname").setValue("").setValueState("None");
			this.byId("inputStreet").setValue("").setValueState("None");
			this.byId("HouseNum1").setValue("").setValueState("None");
			this.byId("inputBirthdate").setValue("").setValueState("None");
			// this.byId("inputPOCombo").setSelectedKey("").setValueState("None");
			// this.byId("inputCountry").setSelectedKey("MX").setValueState("None");
			this.byId("inputCountry").setSelectedKey("MX");
			this.byId("inputCity1").setValue("").setValueState("None");
			this.byId("inputPostCode1").setValue("").setValueState("None");
			this.byId("inputEmail").setValue("").setValueState("None");
			this.byId("inputTelNumber").setValue("").setValueState("None");
			this.byId("inputUidData").setValue("").setValueState("None");
			this.byId("inputImss").setValue("").setValueState("None");
			this.byId("inputDriverLicense").setValue("").setValueState("None");
			this.byId("inputContactRelationship").setValue("").setValueState("None");
			this.byId("inputContactName").setValue("").setValueState("None");
			this.byId("inputContactPhone").setValue("").setValueState("None");
			this.byId("inputValidateUidType").setValue("");
			// this.byId("inputLanguP").setSelectedKey("").setValueState("None");
			this.setValueToBindingModel("Sex", "1");
			oViewApp.setProperty("/configDetailProfile/editing", true);
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onUpdateProfile: function() {
			// var oOdataUtility = new OdataUtility();
			// this.onChanceValueBusyIndicator(true);
			// oOdataUtility.requestUpdateProfile(this);
			var oOdataUtility = new OdataUtility();
			var oController = this;
			// oController.onChanceValueBusyIndicator(true);
			// oOdataUtility.requestUpdateProfile(this);
			var oViewApp = oController.getView().getModel("appView");
			oController.onChanceValueBusyIndicator(true);
			if (oViewApp.getProperty("/statusActiveModel_") === "I") {
				// var oBaseUtility = new BaseUtility();
				var msnBox = "";
				if (!oController.flagTakeProfile) {
					msnBox = oController.getResourceBundle().getText("messageDontShowProfileAnymore") + "\n \n" + oController.getView()
						.byId("objectHeader").getNumber() + " : " + oController.byId("inputNameFirst").getValue().toUpperCase() +
						" " + oController.byId("inputNameLast").getValue().toUpperCase();
				} else {
					msnBox = oController.getResourceBundle().getText("messageUpdateIssuedBy", oController.getView().byId(
						"objectHeader").getNumber());
				}
				MessageBox.confirm(msnBox, {
					title: oController.getResourceBundle().getText("messageConfirm"),
					onClose: function(oAction) {
						if (oAction !== "CANCEL") {
							if (oController.flagTakeProfile) {
								// oController.flagTakeProfile = false;
								oViewApp.setProperty("/statusActiveModel_", "A");
								oOdataUtility.requestUpdateProfile(oController);
							} else {
								oOdataUtility.requestUpdateProfile(oController, true);
							}
							return;
						}
						oController.onChanceValueBusyIndicator(false);
					},
					styleClass: "",
					initialFocus: null,
					textDirection: sap.ui.core.TextDirection.Inherit
				});
			} else {
				oOdataUtility.requestUpdateProfile(this);
			}
		},
		closeConfirmCreate: function(oValues) {
			// $(".sapMITBContent").css("pointer-events", "none");
			var oViewApp = this.getView().getModel("appView");
			oViewApp.setProperty("/configDetailProfile/editing", false);
			oViewApp.setProperty("/configDetailProfile/creating", true);
			oViewApp.setProperty("/blockForms", true);
			// console.log("--> Actualizacion completa...");
			oViewApp.setProperty("/titleDetail", this.getResourceBundle().getText("titleDisplayProfile"));
			this.getView().byId("buttonSave").setVisible(false);
			this.getView().byId("buttonCancel").setVisible(false);
			this.getView().byId("buttonUpdate").setVisible(false);
			this.getView().byId("buttonEdit").setVisible(true);
			// oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
			MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
				duration: 5000,
				width: "25%",
				animationTimingFunction: "ease-in",
				animationDuration: 800
			});
			if (oValues[1]) {
				this.onCancelEditAccess();
				var oSettingData = this.getToVisaData();
				oSettingData.then(this.setToVisaData.bind(this)).then(this.onChanceValueBusyIndicator(false)).catch(this.onFailGenericPromise
					.bind(this));
			}
			this.getRouter().navTo("object", {
				objectId: oValues[0]
			}, true);
			this.onChanceValueBusyIndicator(false);
		},
		closeConfirmUpdate: function(oAction) {
			// $(".sapMITBContent").css("pointer-events", "none");
			var oViewApp = this.getView().getModel("appView");
			oViewApp.setProperty("/configDetailProfile/editing", false);
			oViewApp.setProperty("/blockForms", true);
			oViewApp.setProperty("/availableButtonCreateProfile", true);
			oViewApp.setProperty("/configDetailProfile/validatePO", true);
			oViewApp.setProperty("/titleDetail", this.getResourceBundle().getText("titleDisplayProfile"));
			this.getView().byId("buttonSave").setVisible(false);
			this.getView().byId("buttonCancel").setVisible(false);
			this.getView().byId("buttonUpdate").setVisible(false);
			this.getView().byId("buttonEdit").setVisible(true);
			oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
			MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
				duration: 5000,
				width: "25%",
				animationTimingFunction: "ease-in",
				animationDuration: 800
			});
			if (this.flagTakeProfile) {
				this.flagTakeProfile = false;
				this.getOwnerComponent().oListSelector._oList.getBinding("items").refresh();
				this.getView().getModel().setProperty(this.getView().getElementBinding().getPath() + "/Status", "A");
				this.getRouter().navTo("object", {
					objectId: "0"
				}, true); // this.getRouter().navTo("object", {
				// 	objectId: this.getView().getElementBinding().getPath()
				// }, true);
				// this.getOwnerComponent().oListSelector.selectAListItem(this.getView().getElementBinding().getPath());
				// oViewApp.masterList.getBinding("items").refresh();
				// mPayload.IssuedBy = oViewApp.getProperty("/issuedBy");
			}
		},
		transformDateGWType: function(oDateItem) {
			var formatDate = new Date(oDateItem);
			return formatDate;
		},
		onCreateProfile: function() {
			var oOdataUtility = new OdataUtility();
			this.onChanceValueBusyIndicator(true);
			oOdataUtility.requestCreateProfile(this);
		},
		onAddPO: function(oPayLoad) {
			var oOdataUtility = new OdataUtility();
			this.onChanceValueBusyIndicator(true);
			oOdataUtility.requestCreateVisa(this, oOdataUtility, oPayLoad);
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onEditProfile: function() {
			MessageBox.confirm(this.getResourceBundle().getText("messageEditProfile") + "\n \n" + this.getView().byId(
				"objectHeader").getNumber() + " : " + this.byId("inputNameFirst").getValue().toUpperCase() + " " + this.byId(
				"inputNameLast").getValue().toUpperCase(), {
				title: this.getResourceBundle().getText("messageConfirm"),
				onClose: this.closeConfirmEdit.bind(this),
				styleClass: "",
				initialFocus: null,
				textDirection: sap.ui.core.TextDirection.Inherit
			});
		},
		closeConfirmEdit: function(oAction) {
			if (oAction === "OK") {
				// this.getModel()
				var oViewApp = this.getView().getModel("appView");
				oViewApp.setProperty("/blockForms", false);
				oViewApp.setProperty("/availableButtonCreateProfile", false);
				oViewApp.setProperty("/titleDetail", this.getResourceBundle().getText("titleEditProfile"));
				this.getView().byId("buttonSave").setVisible(false);
				oViewApp.setProperty("/configDetailProfile/validatePO", false);
				this.getView().byId("buttonCancel").setVisible(true);
				this.getView().byId("buttonUpdate").setVisible(true);
				this.getView().byId("buttonEdit").setVisible(false);
				// $(".sapMITBContent").css("pointer-events", "all");
				oViewApp.setProperty("/configDetailProfile/editing", true);
				oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageEditUpdate"));
				MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
					duration: 5000,
					width: "25%",
					animationTimingFunction: "ease-in",
					animationDuration: 800
				});
				setTimeout(function() {
					oViewApp.setProperty("/configDetailProfile/validatePO", false);
				}, 1000);
			}
		},
		closeConfirmUpdateWithDesactivateProfile: function(oAction) {
			// $(".sapMITBContent").css("pointer-events", "none");
			var oViewApp = this.getView().getModel("appView");
			oViewApp.setProperty("/configDetailProfile/editing", false);
			oViewApp.setProperty("/blockForms", true);
			oViewApp.setProperty("/availableButtonCreateProfile", true);
			oViewApp.setProperty("/configDetailProfile/validatePO", true);
			oViewApp.setProperty("/titleDetail", this.getResourceBundle().getText("titleDisplayProfile"));
			this.getView().byId("buttonSave").setVisible(false);
			this.getView().byId("buttonCancel").setVisible(false);
			this.getView().byId("buttonUpdate").setVisible(false);
			this.getView().byId("buttonEdit").setVisible(true);
			oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
			MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
				duration: 5000,
				width: "25%",
				animationTimingFunction: "ease-in",
				animationDuration: 800
			});
			this.getRouter().getTargets().display("detailObjectWasInactivate");
		},
		onNavBackFromDetail: function() {
			// this
			this.onCancelEdit();
			this.onNavBack();
		},
		onCancelEdit: function() {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			if (this.flagTakeProfile) {
				this.flagTakeProfile = false;
				this.getRouter().getTargets().display("detailObjectNotFound");
			}
			if (oElementBinding.getPath() === "/PROFILESet('0')") {
				delete oElementBinding.getModel().oData["PROFILESet('0')"];
				var oObjectId;
				$.each(oElementBinding.getModel().oData, function(i) {
					if (i.indexOf("PROFILESet('") === 0) {
						oObjectId = i;
						return false;
					}
				});
				if (oObjectId) {
					oObjectId = oObjectId.replace("PROFILESet('", "");
					oObjectId = oObjectId.replace("')", "");
					this.getRouter().navTo("object", {
						objectId: oObjectId
					});
				} else {
					this.getRouter().navTo("detailNoObjectsAvailable");
				}
			}
			var oViewApp = this.getView().getModel("appView");
			oViewApp.setProperty("/configDetailProfile/showAllTabs", true);
			oViewApp.setProperty("/titleDetail", this.getResourceBundle().getText("titleDisplayProfile"));
			oViewApp.setProperty("/configDetailProfile/validatePO", true);
			oViewApp.setProperty("/blockForms", true);
			oViewApp.setProperty("/availableButtonCreateProfile", true);
			this.getView().byId("buttonSave").setVisible(false);
			this.getView().byId("buttonCancel").setVisible(false);
			this.getView().byId("buttonUpdate").setVisible(false);
			this.getView().byId("buttonEdit").setVisible(true);
			oViewApp.setProperty("/configDetailProfile/editing", false);
			this.changeValueState();
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		changeStatus: function(oEvent) {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			var oViewApp = this.getView().getModel("appView");
			if (oViewApp.getProperty("/statusActiveModel")) {
				oView.getModel().setProperty(oElementBinding.getPath() + "/Status", "A");
				oViewApp.setProperty("/statusActiveModel_", oView.getModel().getProperty(oElementBinding.getPath() + "/Status"));
			} else {
				oView.getModel().setProperty(oElementBinding.getPath() + "/Status", "I");
				oViewApp.setProperty("/statusActiveModel_", oView.getModel().getProperty(oElementBinding.getPath() + "/Status")); // oViewApp.setProperty("/statusActiveModel", true);
			}
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onSelectIconTab: function(oEvent) {
			console.log("-->");
			// var oViewApp = this.getView().getModel("appView");
			if (this.fragmentCreateAndReadVisa !== "") {
				this.onCancelEditAccess();
			} // var oView = this.getView(),
			// 	oElementBinding = oView.getElementBinding();
			// if (oElementBinding.getPath() !== "/PROFILESet('0')") {
			// 	if (oEvent.getParameters().selectedKey === "__component0---detail--tabPO") {
			// 		// console.log("--> TabPO");
			// 		// this.getView().byId("inputPO").setValue
			// 		var oModel = this.getView().getModel();
			// 		var sUrl = oElementBinding.getPath() + "/ToVisa";
			// 		// the merge is very important, otherwise the supplier and category relation gets broken...
			// 		oModel.read(sUrl, {
			// 			success: jQuery.proxy(function(mResponse) {
			// 				// console.log("success", mResponse);
			// 				if (mResponse.results.length !== 0) {
			// 					this.getView().byId("inputPO").setValue(mResponse.results[0].PurchDoc);
			// 				}
			// 			}, this),
			// 			error: jQuery.proxy(function(mResponse) {
			// 				// console.log("error", mResponse);
			// 			}, this)
			// 		});
			// 	}
			// }
			// $("#" + this.byId("labelSelectCountry")._oLabel.sId).addClass("requiredClass"); // console.log("-->Seleccionando tab?"); //This code was generated by the layout editor.
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onSelectOptionUidTypeGroup: function(oEvent) {
			//This code was generated by the layout editor.
			var oViewApp = this.getView().getModel("appView");
			this.getView().byId("inputValidateUidType").setValue("");
			this.getView().byId("typeUidDataGroup1").setSelectedIndex(oEvent.getParameters().selectedIndex);
			if (oEvent.getParameters().selectedIndex === 0) {
				oViewApp.setProperty("/configDetailProfile/lengthToUuid", 18);
				oViewApp.setProperty("/configDetailProfile/labelUid", "CURP");
			} else {
				oViewApp.setProperty("/configDetailProfile/lengthToUuid", 9);
				oViewApp.setProperty("/configDetailProfile/labelUid", this.getResourceBundle().getText("pasportId"));
			}
		},
		onSelectGuard: function(oEvent) {
			//This code was generated by the layout editor.
			if (oEvent.getParameters().key === "yes") {
				this.byId("labelGuard").setVisible(true);
			} else {
				this.byId("labelGuard").setVisible(false);
			}
		},
		excludeOnceTransportation: function(oEvent) {
			this.byId("inputLandDescr").setSelectedKey("not");
			this.byId("inputAirDescr").setSelectedKey("not");
			if (oEvent.getParameters().key === "yesL") {
				// this.byId("itemLand").setVisible(false);
				this.byId("itemsdestination").setVisible(true);
				this.byId("itemsdestinationFlights").setVisible(false);
			} else {
				this.byId("itemsdestinationFlights").setVisible(true);
				this.byId("itemsdestination").setVisible(false);
			}
			if (oEvent.getParameters().key === "no") {
				this.byId("itemsdestinationFlights").setVisible(false);
				this.byId("itemsdestination").setVisible(false);
			}
			// if (oEvent.getSource().getId().indexOf("inputLand")) {
			// 	if (oEvent.getParameters().key === "yes") {
			// 		// this.byId("itemLand").setVisible(false);
			// 		this.byId("itemAir").setVisible(false);
			// 	} else {
			// 		this.byId("itemAir").setVisible(true);
			// 	}
			// } else {
			// 	if (oEvent.getParameters().key === "yes") {
			// 		// this.byId("itemLand").setVisible(false);
			// 		this.byId("itemLand").setVisible(false);
			// 	} else {
			// 		this.byId("itemLand").setVisible(true);
			// 	}
			// }
			console.log("-->");
			console.log(oEvent);
		},
		onShowLands: function(oEvent) {
			//This code was generated by the layout editor.
			this.byId("inputLandDescr").setSelectedKey("not");
			if (oEvent.getParameters().key === "yes") {
				this.byId("itemsdestination").setVisible(true);
			} else {
				this.byId("itemsdestination").setVisible(false);
			}
		},
		onShowFlights: function(oEvent) {
			//This code was generated by the layout editor.
			this.byId("inputAirDescr").setSelectedKey("not");
			if (oEvent.getParameters().key === "yes") {
				this.byId("itemsdestinationFlights").setVisible(true);
			} else {
				this.byId("itemsdestinationFlights").setVisible(false);
			}
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onSendValidateUidType: function() {
			var oValidatorForm = new ValidatorForm();
			var oOdataUtility = new OdataUtility();
			var oViewApp = this.getView().getModel("appView");
			var validations = oViewApp.getProperty("/validateSpecificPolicy/uid");
			var validator = oValidatorForm.specificValidation(this, validations);
			this.onChanceValueBusyIndicator(true);
			validator.then(oOdataUtility.requestUidSet.bind(this)).then(this.evalResponseUidSet.bind(this)).catch(this.onFailGenericPromise
				.bind(this));
		},
		evalResponseUidSet: function(_oResponse) {
			var oResponse = _oResponse;
			var msgCurp = "";
			var showMessageBox = false;
			var frgStr, oModel;
			this.fragmentShowDuplicatedProfile = "";
			switch (oResponse.EReturn.LogNo) {
				case "0":
				case "":
					msgCurp = this.getResourceBundle().getText("messageSuccessFormatUidType");
					showMessageBox = true;
					break;
				case "1":
					frgStr = "com.gc.passportvisits.view.fragments.PanelDuplicatedProfile";
					oModel = new JSONModel([oResponse.EProfile]);
					showMessageBox = false;
					break;
					// case "1":
				case "2":
					frgStr = "com.gc.passportvisits.view.fragments.PanelDuplicatedProfileUpdate";
					oModel = new JSONModel([oResponse.EProfile]);
					showMessageBox = false;
					break;
				case "3":
					msgCurp = this.getResourceBundle().getText("messageErrorCauseEmployee");
					showMessageBox = true;
					break;
				case "99":
					msgCurp = this.getResourceBundle().getText("messageErrorCauseBlackList");
					showMessageBox = true;
					break;
			}
			if (showMessageBox) {
				MessageBox.information(msgCurp, {
					title: this.getResourceBundle().getText("titleMessageBoxInfo"),
					onClose: this.continueRegistry.bind(this, oResponse.EReturn.LogNo),
					textDirection: sap.ui.core.TextDirection.Inherit
				});
			} else {
				this.fragmentShowDuplicatedProfile = sap.ui.xmlfragment(this.getView().getId(), frgStr, this);
				this.fragmentShowDuplicatedProfile.setModel(oModel);
				this.getView().addDependent(this.fragmentShowDuplicatedProfile);
				this.fragmentShowDuplicatedProfile.open();
				this.fragmentShowDuplicatedProfile.attachAfterClose(function(oEvent) {
					this.onCloseDialogDuplicated();
				}.bind(this));
			}
		},
		onCloseDialogDuplicated: function() {
			this.fragmentShowDuplicatedProfile.close();
			this.fragmentShowDuplicatedProfile.destroy();
			this.fragmentShowDuplicatedProfile = "";
			// console.log("cerando")
			this.onChanceValueBusyIndicator(false);
		},
		onSelectionTakeProfile: function(oEvent) {
			// console.log("valores?");
			// oEvent.getParameter("listItem").getModel().getProperty(oEvent.getParameter("listItem").getBindingContext().sPath).Passportid
			// oEvent.getParameter("listItem").getModel().getProperty(oEvent.getParameter("listItem").getBindingContext().sPath)
			this.flagTakeProfile = true;
			this.getRouter().navTo("object", {
				objectId: oEvent.getParameter("listItem").getModel().getProperty(oEvent.getParameter("listItem").getBindingContext()
					.sPath).Passportid
			}, true);
			// var oViewApp = this.getView().getModel("appView");
			// oViewApp.setProperty("/configDetailProfile/showAllTabs", true);
			// var tabIconBar = oController.getView().byId("detailIconTabBar");
			var oViewApp = this.getView().getModel("appView");
			oViewApp.setProperty("/configDetailProfile/showInitTab", "__component0---detail--tabInfo");
			oViewApp.setProperty("/configDetailProfile/showAllTabs", true);
			var tabIconBar = this.getView().byId("detailIconTabBar");
			tabIconBar.setSelectedKey(oViewApp.getProperty("/configDetailProfile/showInitTab"));
		},
		onFailGenericPromise: function(oReason) {
			console.log("--> Reason 2: ");
			if (oReason === "0") {
				this.getRouter().navTo("detailObjectNotFound");
				// this.getRouter().getTargets().display("detailObjectNotFound");
				console.log(oReason);
			}
			this.onChanceValueBusyIndicator(false);
		},
		continueWithPO: function(oLogNo) {
			// if (oLogNo === "0" || oLogNo === "") {
			var oViewApp = this.getView().getModel("appView");
			this.getView().byId("inputPO").setValue();
			// oViewApp.setProperty("/configDetailProfile/showAllTabs", true);
			oViewApp.setProperty("/configDetailProfile/showInitTab", "tabPO");
			oViewApp.setProperty("/configDetailProfile/validatePersonalId", false);
			oViewApp.setProperty("/configDetailProfile/showSelectionList", "Detail");
			oViewApp.setProperty("/configDetailProfile/validatePO", true);
			var tabIconBar = this.getView().byId("detailIconTabBar");
			// tabIconBar.setSelectedKey(oViewApp.getProperty("/configDetailProfile/showInitTab")); // this.getView().byId("buttonSave").setVisible(true);
			tabIconBar.setSelectedKey(this.byId(oViewApp.getProperty("/configDetailProfile/showInitTab")).getId()); // } // this.onChanceValueBusyIndicator(false);
		},
		continueRegistry: function(oLogNo) {
			if (oLogNo === "0" || oLogNo === "") {
				var oViewApp = this.getView().getModel("appView");
				oViewApp.setProperty("/configDetailProfile/showAllTabs", true);
				oViewApp.setProperty("/configDetailProfile/showInitTab", "tabInfo");
				oViewApp.setProperty("/configDetailProfile/validatePersonalId", false);
				oViewApp.setProperty("/configDetailProfile/showSelectionList", "Inactive");
				oViewApp.setProperty("/configDetailProfile/showSelectedPO", false);
				oViewApp.setProperty("/configDetailProfile/showSelectedPOAlter", true);
				var tabIconBar = this.getView().byId("detailIconTabBar");
				// tabIconBar.setSelectedKey(oViewApp.getProperty("/configDetailProfile/showInitTab"));
				tabIconBar.setSelectedKey(this.byId(oViewApp.getProperty("/configDetailProfile/showInitTab")).getId());
				this.getView().byId("buttonSave").setVisible(true);
			}
			this.onChanceValueBusyIndicator(false);
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		// onChanceBirthDate: function() {
		// 	console.log("--> Cambio de fecha"); //This code was generated by the layout editor.
		// },
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onChanceSelect: function(oEvent) {
			// console.log("select");
			this.getView().byId("inputCityHide").setValue(oEvent.getSource().getSelectedItem().getKey()); //This code was generated by the layout editor.
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onChancePO: function(oEvent) {
			// console.log("--> A new PO");
			// console.log(oEvent);
			this.byId("inputPhoneHost").setValue();
			this.byId("inputEmailHost").setValue();
			this.byId("inputAreaHost").setValue();
			this.byId("inputSiteHost").setSelectedKey();
			this.byId("inputDepartureDate").setValue(moment(this.getModel().getProperty(oEvent.getParameters().selectedItem
				.getBindingContext().sPath).ExpiryDate).format("YYYY-MM-DD"));
			// this.getModel().getProperty(oEvent.getParameters().selectedItem.getBindingContext().sPath);
			var filters = [new Filter("PurchDoc", sap.ui.model.FilterOperator.EQ, oEvent.getSource().getSelectedKey())];
			this.byId("inputSiteHost").getBinding("items").filter(filters, "Application");
		},
		onChanceHost: function(oEvent) {
			console.log(oEvent);
			if (oEvent.getSource().getSelectedItem()) {
				var oContext = this.getModel().getProperty(oEvent.getSource().getSelectedItem().getBindingContext().sPath);
				// this.byId("inputPhoneHost").setValue(oContext.TelNumber);
				this.byId("inputEmailHost").setValue(oContext.SmtpAddr);
				this.byId("inputAreaHost").setValue(oContext.Department);
			} else {
				this.byId("inputEmailHost").setValue();
				this.byId("inputAreaHost").setValue();
			}
		},
		onChanceSupervisor: function(oEvent) {
			console.log(oEvent);
			if (oEvent.getSource().getSelectedItem()) {
				var oContext = this.getModel().getProperty(oEvent.getSource().getSelectedItem().getBindingContext().sPath);
				// this.byId("inputPhoneHost").setValue(oContext.TelNumber);
				this.byId("inputEmailSuper").setValue(oContext.SmtpAddr);
				this.byId("inputAreaSuper").setValue(oContext.Department);
			} else {
				this.byId("inputEmailSuper").setValue();
				this.byId("inputAreaSuper").setValue();
			}
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onDetailPressIcon: function(oEvent) {
			// console.log("--> A new PO");
			// this.getView().byId("inputPO").setValue(oEvent.getSource().getSelectedItem().getKey());
			// this.getView().getModel("oModelVisaSet")
			this.getView().getModel("oModelVisaSet").setProperty("/results/0/PurchDoc", oEvent.getSource().getSelectedItem()
				.getKey());
			MessageBox.confirm("La PO que desea agregar es:\n \n" + oEvent.getSource().getSelectedItem().getKey(), {
				title: "Confirmar PO",
				onClose: this.closeConfirmPO.bind(this),
				styleClass: "",
				initialFocus: null,
				textDirection: sap.ui.core.TextDirection.Inherit
			});
		},
		closeConfirmPO: function(oAction) {
			// console.log(oAction);
			if (oAction === "OK") {
				this.continueRegistry();
			}
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onSelectSex: function(oEvent) {
			var oSex = oEvent.getSource().getSelectedIndex();
			if (oSex === 0) {
				oSex = 1;
			} else {
				oSex = 2;
			}
			// oElementBinding.getModel().setProperty(sPath + "/Sex", oSex);
			//This code was generated by the layout editor.
			// console.log(this.getValueToBindingModel("Sex"));
			this.setValueToBindingModel("Sex", oSex.toString()); // console.log(this.getValueToBindingModel("Sex"));
		},
		getValueToBindingModel: function(oItem) {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			var sPath = oElementBinding.getPath();
			// oElementBinding.getModel().getProperty(sPath + "/" + oItem);
			return oElementBinding.getModel().getProperty(sPath + "/" + oItem);
		},
		setValueToBindingModel: function(oItem, oValue) {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			var sPath = oElementBinding.getPath();
			oElementBinding.getModel().setProperty(sPath + "/" + oItem, oValue);
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onSelectIsEjidatario: function(oEvent) {
			var oIsEjidatario = oEvent.getSource().getSelectedIndex();
			if (oIsEjidatario === 0) {
				oIsEjidatario = "X";
			} else {
				oIsEjidatario = "";
			}
			// oElementBinding.getModel().setProperty(sPath + "/Sex", oSex);
			//This code was generated by the layout editor.
			// console.log(this.getValueToBindingModel("Sex"));
			this.setValueToBindingModel("ChkEjido", oIsEjidatario.toString()); // console.log(this.getValueToBindingModel("Sex"));
			//This code was generated by the layout editor.
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onCheckBoxSelectedTypeEjidatario: function(oEvent) {
			var oIdItem = oEvent.getSource().getId();
			var oItem;
			var oItemValue = oEvent.getSource().getSelected();
			oItemValue = oItemValue ? "X" : "";
			if (oIdItem.includes("inputPersonalEjido")) {
				oItem = "ChkPersonal";
			}
			if (oIdItem.includes("inputFamiliarEjido")) {
				oItem = "ChkFamiliar";
			}
			if (oIdItem.includes("inputConyugueEjido")) {
				oItem = "ChkConyugue";
			}
			this.setValueToBindingModel(oItem, oItemValue); //This code was generated by the layout editor.
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onSelectWorkMine: function(oEvent) {
			var oWorkMine = oEvent.getParameters().key;
			if (oWorkMine === "yes") {
				oWorkMine = "X";
			} else {
				oWorkMine = "";
			}
			this.setValueToBindingModel("ChkTrabajaMina", oWorkMine); //This code was generated by the layout editor.
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		// validatePoPendingOrComplete: function() {
		// 	var oStatusVisa = _.filter(this.getModel("oModelVisaSet").getProperty("/results"), function(value) {
		// 		if (value.Status === "P" || value.Status === "C" || value.Status === "A") {
		// 			return true;
		// 		}
		// 	});
		// 	if (oStatusVisa.length === 0) {
		// 		return true;
		// 	}
		// 	return false; // console.log("validar")
		// },
		onCreateFragementAccess: function(oEvent) {
			var oViewApp = this.getView().getModel("appView");
			this.onChanceValueBusyIndicator(true);
			if (oEvent.getParameters().id.indexOf("btnNewRequest") !== -1) {
				oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText(
					"messageCantCreateAccess"));
				if (!this.validatePoPendingOrComplete()) {
					MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
						duration: 5000,
						width: "25%",
						animationTimingFunction: "ease-in",
						animationDuration: 800
					});
					this.onChanceValueBusyIndicator(false);
					// this.onCancelEditAccess();
					return;
				}
			}
			var frgStr = "com.gc.passportvisits.view.fragments.PanelAccessRequest";
			if (this.getResourceBundle().getText("appVersion") === "1") {
				frgStr = "com.gc.passportvisits.view.fragments.PanelAccessRequestVisit";
			}
			this.fragmentCreateAndReadVisa = sap.ui.xmlfragment(this.getView().getId(), frgStr, this);
			this.getView().byId("tabPO").addContent(this.fragmentCreateAndReadVisa);
			// this.byId("inputEntryDate").setValue();
			// this.byId("inputDepartureDate").setValue();
			// this.byId("inputDepartureDate").setMaxDate(maxDate);
			if (oEvent.getParameters().id.indexOf("btnNewRequest") !== -1) {
				oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageCreateAccess"));
				this.onCreateAccess(oEvent);
				this.applyRulesToUrgentAccess("no");
				this.disableDatePicker();
			} else {
				oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
				this.onDetailAccess(oEvent);
			}
			MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
				duration: 5000,
				width: "25%",
				animationTimingFunction: "ease-in",
				animationDuration: 800
			});
			// $(".sapMSwt2").css("width", "120px");
			// this.byId("itemsdestinationFlights").addStyleClass("requiredClass");
			this.byId("inputTruckShop").addStyleClass("sapMSwt2");
			this.byId("inputPit").addStyleClass("sapMSwt2");
			this.byId("inputSupplyArea").addStyleClass("sapMSwt2");
			this.byId("inputTrailingDam").addStyleClass("sapMSwt2");
			this.byId("inputCamp").addStyleClass("sapMSwt2");
			this.byId("inputProcessPlant").addStyleClass("sapMSwt2");
			this.byId("inputProcessWarehouse").addStyleClass("sapMSwt2");
			this.byId("inputProjectAreas").addStyleClass("sapMSwt2");
			this.byId("inputNewAdmin").addStyleClass("sapMSwt2");
			this.byId("inputOldAdmin").addStyleClass("sapMSwt2");
			this.byId("inputNPSC").addStyleClass("sapMSwt2");
			this.byId("inputRH").addStyleClass("sapMSwt2");
			this.byId("inputItSite").addStyleClass("sapMSwt2");
		},
		disableDatePicker: function() {
			this.byId("inputEntryDate").attachBrowserEvent("keypress", function() {
				var idComplete = this.getId() + "-icon";
				var idCompleteInner = this.getId() + "-inner";
				$("#" + idCompleteInner).css("pointer-events", "none");
				$("#" + idComplete).on("click", function() {
					$("#" + idComplete).off("click");
				});
				$("#" + idComplete).click();
			});
			this.byId("inputDepartureDate").attachBrowserEvent("keypress", function() {
				var idComplete = this.getId() + "-icon";
				var idCompleteInner = this.getId() + "-inner";
				$("#" + idCompleteInner).css("pointer-events", "none");
				$("#" + idComplete).on("click", function() {
					$("#" + idComplete).off("click");
				});
				$("#" + idComplete).click();
			});
		},
		changeState: function(e) {
			// if (e.getParameter('state')) {
			// 	$(this).find('.sapMSwtTextOff').css('display', 'none');
			// 	$(this).find('.sapMSwtTextOn').css('display', 'inherit');
			// } else {
			// 	$(this).find('.sapMSwtTextOn').css('display', 'none');
			// $(this).find('.sapMSwtTextOff').css('display', 'inherit');
			// }
		},
		onCreateAccess: function(oEvent) {
			var oViewApp = this.getView().getModel("appView");
			// if (this.byId("listVisas").getItems().length < 1) {
			oViewApp.setProperty("/configDetailProfile/showDetailAccess", true);
			oViewApp.setProperty("/configDetailProfile/showTableAccess", false);
			oViewApp.setProperty("/configDetailProfile/editDetailAccess", true);
			oViewApp.setProperty("/availableEdit", false);
			oViewApp.setProperty("/blockForms", false);
			// var oOdataUtility = new OdataUtility();
			// var oModelVisaEmpty = oOdataUtility.getEmptyVisaModel();
			// this.getModel("oModelVisaClone").setData(oModelVisaEmpty.results[0]);
			this.onChanceValueBusyIndicator(false);

			this.renderSelectHostCreate(
				this.getModel("oModelVisaClone").getProperty("/Siteid"),
				// this.getModel("oModelVisaClone").getProperty("/Approver"),
				this.getModel("oModelVisaClone").getProperty("/UrgentVisa")
			);
			this.renderSelectSupervisorCreate(this.getModel("oModelVisaClone").getProperty("/Siteid"));

			this.renderConfigAreas();
		},

		renderSelectSupervisorCreate: function(_oValueSite) {
			var oModel = this.getView().getModel();
			// var oValueFilter = _oValueFilter;
			var oValueSite = _oValueSite;
			var oViewApp = this.getView().getModel("appView");
			var filters = [new Filter("Siteid", sap.ui.model.FilterOperator.EQ, oValueSite)];
			oModel.read("/SupervisorsSet", {
				filters: filters,
				success: jQuery.proxy(function(mResponse) {
					console.log("los supervisores");
					console.log(mResponse);
				}, this),
				error: jQuery.proxy(function(mResponse) {
					console.log("error", mResponse);
				}, this)
			});
		},
		renderSelectHostCreate: function(_oValueSite, _oUrgentVisa) {
			var oModel = this.getView().getModel();
			// var oValueHost = _oValueHost;
			var oUrgentVisa = _oUrgentVisa;

			if (oUrgentVisa === "no") {
				oUrgentVisa = "-";

			} else {
				oUrgentVisa = "X";
			}
			var oValueSite = _oValueSite;
			var oViewApp = this.getView().getModel("appView");
			var _issuedBy = oViewApp.getProperty("/issuedBy");
			var filters = [new Filter({
				filters: [

							new Filter("Siteid", sap.ui.model.FilterOperator.EQ, oValueSite),
							new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, oUrgentVisa)
							// new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, oUrgentVisa)
							],
				and: true

			})];
			oModel.read("/VisitApproversSet", {
				filters: filters,
				success: jQuery.proxy(function(mResponse) {
					var filters = [new Filter({
						filters: [

							new Filter("Siteid", sap.ui.model.FilterOperator.EQ, oValueSite),
							new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, oUrgentVisa)
							// new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, oUrgentVisa)
							],
						and: true

					})];
					this.byId("inputApprover").getBinding("items").filter(filters, "Application");

					console.log("los aprovadores");
					console.log(mResponse);
				}, this),
				error: jQuery.proxy(function(mResponse) {
					console.log("error", mResponse);
				}, this)
			});

		},

		// 	onCreateAccess: function() {
		// 	var oViewApp = this.getView().getModel("appView");
		// 	// if (this.byId("listVisas").getItems().length < 1) {
		// 	oViewApp.setProperty("/configDetailProfile/showDetailAccess", true);
		// 	oViewApp.setProperty("/configDetailProfile/showTableAccess", false);
		// 	oViewApp.setProperty("/configDetailProfile/editDetailAccess", true);
		// 	oViewApp.setProperty("/availableEdit", false);
		// 	oViewApp.setProperty("/blockForms", false);
		// 	// var oOdataUtility = new OdataUtility();
		// 	// var oModelVisaEmpty = oOdataUtility.getEmptyVisaModel();
		// 	// this.getModel("oModelVisaClone").setData(oModelVisaEmpty.results[0]);
		// 	this.onChanceValueBusyIndicator(false);
		// },
		onDetailAccess: function(oEvent) {
			var oController = this;
			var oViewApp = oController.getView().getModel("appView");
			oViewApp.setProperty("/configDetailProfile/showDetailAccess", true);
			oViewApp.setProperty("/configDetailProfile/showTableAccess", false);
			oViewApp.setProperty("/configDetailProfile/editDetailAccess", false);
			oViewApp.setProperty("/availableEdit", true);
			oViewApp.setProperty("/blockForms", true);
			var validatePhone = this.getModel("device").getProperty("/system/desktop");
			var paramBindingContext = "";
			if (validatePhone) {
				paramBindingContext = oEvent.getParameter("listItem");
			} else {
				paramBindingContext = oEvent.getSource();
			}
			paramBindingContext.setSelected(false);
			// oEvent.getSource().setSelected(false);
			// this.getModel("oModelVisaClone").setData(this.getModel().getProperty(oEvent.getParameter("listItem").getBindingContext()
			// 	.getPath()));
			this.getModel("oModelVisaClone").setData(this.getModel().getProperty(paramBindingContext.getBindingContext().getPath()));
			if (this.getResourceBundle().getText("appVersion") === "1") {
				// var oContext = this.getModel().getProperty("/VisitApproversSet('" + this.getModel("oModelVisaClone").getProperty(
				// 	"/Approver") + "')");
				// if (oContext) {
				// 	this.byId("inputEmailHost").setValue(oContext.SmtpAddr);
				// 	this.byId("inputAreaHost").setValue(oContext.Department);
				// }
				// var oContextTwo = this.getModel().getProperty("/SupervisorsSet('" + this.getModel("oModelVisaClone").getProperty(
				// 	"/Supervisor") + "')");
				// if (oContextTwo) {
				// 	this.byId("inputEmailSuper").setValue(oContextTwo.SmtpAddr);
				// 	this.byId("inputAreaSuper").setValue(oContextTwo.Department);
				// }
				this.renderSelectHost(
					this.getModel("oModelVisaClone").getProperty("/Siteid"),
					this.getModel("oModelVisaClone").getProperty("/Approver"),
					this.getModel("oModelVisaClone").getProperty("/UrgentVisa")
				);
				this.renderSelectSupervisor(this.getModel("oModelVisaClone").getProperty("/Siteid"), this.getModel(
					"oModelVisaClone").getProperty("/Supervisor"));

			} else {
				var filters = [new Filter("PurchDoc", sap.ui.model.FilterOperator.EQ, this.getModel("oModelVisaClone").getProperty(
					"/PurchDoc"))];
				this.byId("inputSiteHost").getBinding("items").filter(filters, "Application");
				var oContext = this.getModel().getProperty("/ContractApproversSet('" + this.getModel("oModelVisaClone").getProperty(
					"/Host") + "')");
				if (oContext) {
					this.byId("inputEmailHost").setValue(oContext.SmtpAddr);
					this.byId("inputAreaHost").setValue(oContext.Department);
				}
			}
			this.renderSectionAccess(); // this.onChanceValueBusyIndicator(false);
		},
		renderSelectHost: function(_oValueSite, _oValueHost, _oUrgentVisa) {
			var oModel = this.getView().getModel();
			// var oValueFilter = _oValueFilter;
			var oValueHost = _oValueHost;
			var oUrgentVisa = _oUrgentVisa;
			var oValueSite = _oValueSite;
			var oUrgentVisa = _oUrgentVisa;

			// if (oUrgentVisa === "no") {
			// 	oUrgentVisa = "-";

			// } else {
			// 	oUrgentVisa = "X";
			// }
			var oViewApp = this.getView().getModel("appView");
			var _issuedBy = oViewApp.getProperty("/issuedBy");
			// oModel.read("/ContractApproversSet?$filter= PurchDoc eq '" + oValueFilter + "'", {
			var filters = [new Filter({
				filters: [

							new Filter("Siteid", sap.ui.model.FilterOperator.EQ, oValueSite),
							new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, oUrgentVisa)
							// new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, oUrgentVisa)

							],
				and: true

			})];
			oModel.read("/VisitApproversSet", {
				filters: filters,

				// and Siteid eq 'PEN' and IssuedBy eq '104272'
				success: jQuery.proxy(function(mResponse) {

					var oContext = _.filter(mResponse.results, function(value) {
						if (value.Passportid === oValueHost) {
							return true;
							// console.log(value.Passportid)
						}
					}.bind(this))

					if (oContext.length > 0) {
						this.byId("inputEmailHost").setValue(oContext[0].SmtpAddr);
						this.byId("inputAreaHost").setValue(oContext[0].Department);
					}
					console.log(mResponse);
				}, this),
				error: jQuery.proxy(function(mResponse) {
					console.log("error", mResponse);
				}, this)
			});

		},
		renderSelectSupervisor: function(_oValueSite, _oValueSupervisor) {
			var oModel = this.getView().getModel();
			// var oValueFilter = _oValueFilter;
			var oValueSite = _oValueSite;
			var oValueSupervisor = _oValueSupervisor;
			// var _siteId = this.getModel("oModelVisaClone").getProperty("/Siteid");
			var oViewApp = this.getView().getModel("appView");
			// oModel.read("/ContractApproversSet?$filter= PurchDoc eq '" + oValueFilter + "'", {
			// var filters = [new Filter({
			// 	filters: [
			// 				new Filter("Siteid", sap.ui.model.FilterOperator.EQ, oValueSite)
			// 				]

			// })];

			var filters = [new Filter("Siteid", sap.ui.model.FilterOperator.EQ, oValueSite)];
			oModel.read("/SupervisorsSet", {
				filters: filters,

				// and Siteid eq 'PEN' and IssuedBy eq '104272'
				success: jQuery.proxy(function(mResponse) {

					var oContext = _.filter(mResponse.results, function(value) {
						if (value.Passportid === oValueSupervisor) {
							return true;
							// console.log(value.Passportid)
						}
					}.bind(this))

					if (oContext.length > 0) {
						this.byId("inputEmailSuper").setValue(oContext[0].SmtpAddr);
						this.byId("inputAreaSuper").setValue(oContext[0].Department);
					}
					console.log(mResponse);
				}, this),
				error: jQuery.proxy(function(mResponse) {
					console.log("error", mResponse);
				}, this)
			});

		},
		renderSectionAccessOld: function() {
			// var oContextAreas = this.getModel().getProperty("/AreasByVisaSet(Siteid='" + this.getModel("oModelVisaClone").getProperty(
			// 	"/Siteid") + "',Visaid='" + this.getModel("oModelVisaClone").getProperty("/Visaid") + "')");
			// console.log(oContextAreas);
			var oModel = this.getView().getModel();
			oModel.read("/AreasByVisaSet", {
				success: jQuery.proxy(function(mResponse) {
					// MessageBox.information("Actualizaci\xF3n exitosa.", {
					// 	title: "Informaci\xF3n",
					// 	onClose: this.closeConfirmUpdate.bind(oController),
					// 	textDirection: sap.ui.core.TextDirection.Inherit
					// });
					var oContextAreas = this.getModel().getProperty("/AreasByVisaSet(Siteid='" + this.getModel(
						"oModelVisaClone").getProperty("/Siteid") + "',Visaid='" + this.getModel("oModelVisaClone").getProperty(
						"/Visaid") + "')");
					if (oContextAreas) {
						var mPayloadAccess;
						if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "MRO") {
							this.byId("inputMezz").setState(oContextAreas.Area014 === "X" ? true : false);
							this.byId("inputFloor").setState(oContextAreas.Area015 === "X" ? true : false);
						} else if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "COR") {
							this.byId("inputMezz").setState(oContextAreas.Area014 === "X" ? true : false);
						} else {
							this.byId("inputTruckShop").setState(oContextAreas.Area001 === "X" ? true : false);
							this.byId("inputPit").setState(oContextAreas.Area003 === "X" ? true : false);
							this.byId("inputSupplyArea").setState(oContextAreas.Area005 === "X" ? true : false);
							this.byId("inputTrailingDam").setState(oContextAreas.Area007 === "X" ? true : false);
							this.byId("inputCamp").setState(oContextAreas.Area004 === "X" ? true : false);
							this.byId("inputProcessPlant").setState(oContextAreas.Area002 === "X" ? true : false);
							this.byId("inputProcessWarehouse").setState(oContextAreas.Area008 === "X" ? true : false);
							this.byId("inputProjectAreas").setState(oContextAreas.Area009 === "X" ? true : false);
							this.byId("inputNewAdmin").setState(oContextAreas.Area010 === "X" ? true : false);
							this.byId("inputOldAdmin").setState(oContextAreas.Area011 === "X" ? true : false);
							this.byId("inputRH").setState(oContextAreas.Area012 === "X" ? true : false);
							this.byId("inputItSite").setState(oContextAreas.Area013 === "X" ? true : false);
							this.byId("inputNPSC").setState(oContextAreas.Area006 === "X" ? true : false);
						}
					}
					this.onChanceValueBusyIndicator(false); // this.getModel().getProperty("/AreasByVisaSet(Siteid='" + this.getModel("oModelVisaClone").getProperty("/Siteid") + "',Visaid='"+this.getModel("oModelVisaClone").getProperty("/Visaid")+"')");
					// console.log(oContextAreas);
					// console.log(mResponse)
				}, this),
				error: jQuery.proxy(function(mResponse) {
					// console.log("error", mResponse);
				}, this)
			});
		},
		onCancelEditAccess: function(oEvent) {
			var oViewApp = this.getView().getModel("appView");
			if (this.fragmentCreateAndReadVisa) {
				this.fragmentCreateAndReadVisa.destroy(true);
				this.fragmentCreateAndReadVisa = "";
			}
			this.getModel("oModelVisaClone").setData("");
			oViewApp.setProperty("/configDetailProfile/showDetailAccess", false);
			oViewApp.setProperty("/configDetailProfile/showTableAccess", true);
			oViewApp.setProperty("/availableEdit", true);
			oViewApp.setProperty("/blockForms", true); // 	},
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onCreatePo: function() {
			//This code was generated by the layout editor.
			var oValidatorForm = new ValidatorForm();
			if (oValidatorForm.validateFormAccess(this)) {
				if (this.validateAreasAccess()) {
					this.onChanceValueBusyIndicator(true);
					// console.log("-->next!!");
					this.onAddPO(this.dataCollectionForAccessInformation());
				} else {
					MessageToast.show("You have to capture an access area", {
						duration: 5000,
						width: "25%",
						animationTimingFunction: "ease-in",
						animationDuration: 800
					});
				} // this.onCancelEditAccess();
			} // else {
			// 	console.log("-->no go!!");
			// }
		},
		// validateAreasAccess: function() {
		// 	var mPayloadAccess = {
		// 		Area001: this.byId("inputTruckShop").getState() ? "X" : "",
		// 		Area003: this.byId("inputPit").getState() ? "X" : "",
		// 		Area005: this.byId("inputSupplyArea").getState() ? "X" : "",
		// 		Area007: this.byId("inputTrailingDam").getState() ? "X" : "",
		// 		Area004: this.byId("inputCamp").getState() ? "X" : "",
		// 		Area002: this.byId("inputProcessPlant").getState() ? "X" : "",
		// 		Area008: this.byId("inputProcessWarehouse").getState() ? "X" : "",
		// 		Area009: this.byId("inputProjectAreas").getState() ? "X" : "",
		// 		Area010: this.byId("inputNewAdmin").getState() ? "X" : "",
		// 		Area011: this.byId("inputOldAdmin").getState() ? "X" : "",
		// 		Area006: this.byId("inputNPSC").getState() ? "X" : "",
		// 		Area012: this.byId("inputRH").getState() ? "X" : "",
		// 		Area013: this.byId("inputItSite").getState() ? "X" : ""
		// 	};
		// 	var oGetX = _.filter(mPayloadAccess, function(value) {
		// 		return value === "X";
		// 	});
		// 	if (oGetX.length === 0) {
		// 		return false;
		// 	}
		// 	return true;
		// },
		validateAreasAccessOld: function() {
			var mPayloadAccess;
			if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "MRO") {
				mPayloadAccess = {
					Area014: this.byId("inputMezz").getState() ? "X" : "",
					Area015: this.byId("inputFloor").getState() ? "X" : ""
				};
			} else if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "COR") {
				mPayloadAccess = {
					Area014: this.byId("inputMezz").getState() ? "X" : "" // Area015: (this.byId("inputFloor").getState() ? "X" : "")
				};
			} else {
				mPayloadAccess = {
					Area001: this.byId("inputTruckShop").getState() ? "X" : "",
					Area003: this.byId("inputPit").getState() ? "X" : "",
					Area005: this.byId("inputSupplyArea").getState() ? "X" : "",
					Area007: this.byId("inputTrailingDam").getState() ? "X" : "",
					Area004: this.byId("inputCamp").getState() ? "X" : "",
					Area002: this.byId("inputProcessPlant").getState() ? "X" : "",
					Area008: this.byId("inputProcessWarehouse").getState() ? "X" : "",
					Area009: this.byId("inputProjectAreas").getState() ? "X" : "",
					Area010: this.byId("inputNewAdmin").getState() ? "X" : "",
					Area011: this.byId("inputOldAdmin").getState() ? "X" : "",
					Area006: this.byId("inputNPSC").getState() ? "X" : "",
					Area012: this.byId("inputRH").getState() ? "X" : "",
					Area013: this.byId("inputItSite").getState() ? "X" : ""
				};
			}
			var oGetX = _.filter(mPayloadAccess, function(value) {
				return value === "X";
			});
			if (oGetX.length === 0) {
				return false;
			}
			return true;
		},
		dataCollectionForAccessInformationOld: function() {
			var mPayload = {
				Host: "",
				TranspDescrip: "",
				Flight: "",
				ChkComputer: "",
				Visaid: "",
				FlightDescrip: "",
				TypeWork: "",
				ComputerEquip: this.byId("inputSerial").getValue(),
				UrgentVisa: "",
				Supervisor: "",
				// Siteid: "",
				Siteid: this.getModel("oModelVisaClone").getProperty("/Siteid"),
				RegularityVisit: "",
				Passportid: this.byId("objectHeader").getNumber(),
				ReqSupervision: "",
				VisaType: "V",
				VisitReason: "",
				VisitReasonId: "",
				Approver: "",
				Status: "P",
				LunchService: "",
				CampService: "",
				Transportation: "",
				InductionCourse: "",
				SysStatus: "",
				IssuedDate: this.transformDateGWType(this.byId("inputEntryDate").getValue()),
				ExpiryDate: this.transformDateGWType(this.byId("inputDepartureDate").getValue()),
				SiteHost: "",
				PurchDoc: ""
			};
			if (this.getResourceBundle().getText("appVersion") === "1") {
				mPayload.VisitReasonId = this.byId("inputVisitReason").getSelectedKey();
				mPayload.VisitReason = this.byId("inputVisitReasonOther").getValue();
				mPayload.Approver = this.byId("inputApprover").getSelectedKey();
				mPayload.Supervisor = this.byId("inputSupervisor").getSelectedKey();
				mPayload.UrgentVisa = this.byId("inputUrgent").getSelectedKey() === "yes" ? "X" : "-"; // mPayload.UrgentVisa = (this.byId("inputUrgent").getSelectedIndex() === 0 ? "X" : "");
			} else {
				mPayload.TypeWork = this.byId("inputTypeWork").getSelectedKey();
				mPayload.PurchDoc = this.byId("inputPOCombo").getSelectedKey();
				mPayload.RegularityVisit = this.byId("inputRegularity").getSelectedKey();
				mPayload.Host = this.byId("inputSiteHost").getSelectedKey();
				mPayload.SiteHost = this.byId("inputSiteHost").getSelectedKey();
			}
			if (this.byId("inputSiteAcomodation").getSelectedKey() === "yes") {
				mPayload.CampService = "R";
			}
			if (this.byId("inputSiteComedor").getSelectedKey() === "yes") {
				mPayload.LunchService = "R";
			}
			if (this.getView().byId("inputTransportation").getSelectedKey() === "yesL") {
				mPayload.Transportation = "R";
				mPayload.TranspDescrip = this.getView().byId("inputLandDescr").getSelectedKey();
			}
			if (this.getView().byId("inputTransportation").getSelectedKey() === "yesF") {
				mPayload.Flight = "R";
				mPayload.FlightDescrip = this.getView().byId("inputAirDescr").getSelectedKey();
			}
			// if (this.getView().byId("inputAir").getSelectedKey() === "yes") {
			// 	mPayload.Flight = "R";
			// 	mPayload.FlightDescrip = this.getView().byId("inputAirDescr").getSelectedKey();
			// }
			if (this.byId("inputGuard").getSelectedKey() === "yes") {
				mPayload.ChkComputer = "X";
			} else {
				mPayload.ChkComputer = "-";
			}
			if (this.byId("inputReqSuper").getSelectedKey() === "yes") {
				// if (this.byId("inputReqSuper").getSelectedIndex() === 0) {
				mPayload.ReqSupervision = "X";
			} else {
				mPayload.ReqSupervision = "-";
			}
			// if (this.byId("inputReqSuper").getSelectedIndex() === 0) {
			// 	mPayload.ReqSupervision = "X";
			// }
			var mPayloadAccess;
			if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "MRO") {
				mPayloadAccess = {
					Area014: this.byId("inputMezz").getState() ? "X" : "",
					Area015: this.byId("inputFloor").getState() ? "X" : ""
				};
			} else if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "COR") {
				mPayloadAccess = {
					Area014: this.byId("inputMezz").getState() ? "X" : "" // Area015: (this.byId("inputFloor").getState() ? "X" : "")
				};
			} else {
				mPayloadAccess = {
					Area001: this.byId("inputTruckShop").getState() ? "X" : "",
					Area003: this.byId("inputPit").getState() ? "X" : "",
					Area005: this.byId("inputSupplyArea").getState() ? "X" : "",
					Area007: this.byId("inputTrailingDam").getState() ? "X" : "",
					Area004: this.byId("inputCamp").getState() ? "X" : "",
					Area002: this.byId("inputProcessPlant").getState() ? "X" : "",
					Area008: this.byId("inputProcessWarehouse").getState() ? "X" : "",
					Area009: this.byId("inputProjectAreas").getState() ? "X" : "",
					Area010: this.byId("inputNewAdmin").getState() ? "X" : "",
					Area011: this.byId("inputOldAdmin").getState() ? "X" : "",
					Area006: this.byId("inputNPSC").getState() ? "X" : "",
					Area012: this.byId("inputRH").getState() ? "X" : "",
					Area013: this.byId("inputItSite").getState() ? "X" : ""
				};
			}
			var payLoad = [
				mPayload,
				mPayloadAccess
			];
			// console.log(oModel);
			return payLoad;
		},
		dataCollectionForAccessInformation: function() {
			var mPayload = {
				Host: "",
				TranspDescrip: "",
				Flight: "",
				ChkComputer: "",
				Visaid: "",
				FlightDescrip: "",
				TypeWork: "",
				ComputerEquip: this.byId("inputSerial").getValue(),
				UrgentVisa: "",
				// Siteid: "",
				Siteid: this.getModel("oModelVisaClone").getProperty("/Siteid"),
				RegularityVisit: "",
				Passportid: this.byId("objectHeader").getNumber(),
				ReqSupervision: "",
				VisaType: "V",
				VisitReason: "",
				VisitReasonId: "",
				Approver: "",
				Status: "P",
				LunchService: "",
				CampService: "",
				Transportation: "",
				InductionCourse: "",
				SysStatus: "",
				IssuedDate: this.transformDateGWType(this.byId("inputEntryDate").getValue()),
				ExpiryDate: this.transformDateGWType(this.byId("inputDepartureDate").getValue()),
				SiteHost: "",
				PurchDoc: ""
			};
			if (this.getResourceBundle().getText("appVersion") === "1") {
				mPayload.VisitReasonId = this.byId("inputVisitReason").getSelectedKey();
				mPayload.VisitReason = this.byId("inputVisitReasonOther").getValue();
				mPayload.Approver = this.byId("inputApprover").getSelectedKey();
				mPayload.Supervisor = this.byId("inputSupervisor").getSelectedKey();
				mPayload.UrgentVisa = this.byId("inputUrgent").getSelectedKey() === "yes" ? "X" : "-"; // mPayload.UrgentVisa = (this.byId("inputUrgent").getSelectedIndex() === 0 ? "X" : "");
			} else {
				mPayload.TypeWork = this.byId("inputTypeWork").getSelectedKey();
				mPayload.PurchDoc = this.byId("inputPOCombo").getSelectedKey();
				mPayload.RegularityVisit = this.byId("inputRegularity").getSelectedKey();
				mPayload.Host = this.byId("inputSiteHost").getSelectedKey();
				mPayload.SiteHost = this.byId("inputSiteHost").getSelectedKey();
			}
			if (this.byId("inputSiteAcomodation").getSelectedKey() === "yes") {
				mPayload.CampService = "R";
			}
			if (this.byId("inputSiteComedor").getSelectedKey() === "yes") {
				mPayload.LunchService = "R";
			}
			if (this.getView().byId("inputTransportation").getSelectedKey() === "yesL") {
				mPayload.Transportation = "R";
				mPayload.TranspDescrip = this.getView().byId("inputLandDescr").getSelectedKey();
			}
			if (this.getView().byId("inputTransportation").getSelectedKey() === "yesF") {
				mPayload.Flight = "R";
				mPayload.FlightDescrip = this.getView().byId("inputAirDescr").getSelectedKey();
			}
			// if (this.getView().byId("inputAir").getSelectedKey() === "yes") {
			// 	mPayload.Flight = "R";
			// 	mPayload.FlightDescrip = this.getView().byId("inputAirDescr").getSelectedKey();
			// }
			if (this.byId("inputGuard").getSelectedKey() === "yes") {
				mPayload.ChkComputer = "X";
			} else {
				mPayload.ChkComputer = "-";
			}
			if (this.byId("inputReqSuper").getSelectedKey() === "yes") {
				// if (this.byId("inputReqSuper").getSelectedIndex() === 0) {
				mPayload.ReqSupervision = "X";
			} else {
				mPayload.ReqSupervision = "-";
			}
			// if (this.byId("inputReqSuper").getSelectedIndex() === 0) {
			// 	mPayload.ReqSupervision = "X";
			// }
			var mPayloadAccess;
			// if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "MRO") {
			// 	mPayloadAccess = {
			// 		Area014: this.byId("inputMezz").getState() ? "X" : "",
			// 		Area015: this.byId("inputFloor").getState() ? "X" : ""
			// 	};
			// } else if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "COR") {
			// 	mPayloadAccess = {
			// 		Area014: this.byId("inputMezz").getState() ? "X" : "" // Area015: (this.byId("inputFloor").getState() ? "X" : "")
			// 	};
			// } else {
			// 	mPayloadAccess = {
			// 		Area001: this.byId("inputTruckShop").getState() ? "X" : "",
			// 		Area003: this.byId("inputPit").getState() ? "X" : "",
			// 		Area005: this.byId("inputSupplyArea").getState() ? "X" : "",
			// 		Area007: this.byId("inputTrailingDam").getState() ? "X" : "",
			// 		Area004: this.byId("inputCamp").getState() ? "X" : "",
			// 		Area002: this.byId("inputProcessPlant").getState() ? "X" : "",
			// 		Area008: this.byId("inputProcessWarehouse").getState() ? "X" : "",
			// 		Area009: this.byId("inputProjectAreas").getState() ? "X" : "",
			// 		Area010: this.byId("inputNewAdmin").getState() ? "X" : "",
			// 		Area011: this.byId("inputOldAdmin").getState() ? "X" : "",
			// 		Area006: this.byId("inputNPSC").getState() ? "X" : "",
			// 		Area012: this.byId("inputRH").getState() ? "X" : "",
			// 		Area013: this.byId("inputItSite").getState() ? "X" : ""
			// 	};
			// }

			var keysModel = _.keys(this.getModel("oModelItemAreasSelection").getProperty("/"));
			var stringData = "";
			_.map(keysModel, function(value, index, obj) {
				// console.log(value.split("-")[2] + ":" + value.split("-")[3])
				if (index === (obj.length - 1)) {
					stringData += value.split("-")[1] + ":" + value.split("-")[2];
				} else {
					stringData += value.split("-")[1] + ":" + value.split("-")[2] + ",";
				}

			})

			var payLoad = [mPayload, stringData];
			// console.log(oModel);
			return payLoad;
		},
		formatDate: function(oEvent) {
			var formatDate = "";
			// this.getModel("oModelVisaClone")
			if (oEvent) {
				formatDate = moment(oEvent).format("YYYY-MM-DD");
			}
			return formatDate;
		},
		onChanceIssuedDateVisit: function(oEvent) {
			// console.log(oEvent);
			var minDate;
			var maxDate;
			var dateType = new Date(oEvent.getSource()._getSelectedDate());
			// minDate = new Date(moment(new Date(oEvent.getParameters().value)).add(1, "day").format("YYYY-MM-DD"));
			minDate = oEvent.getSource()._getSelectedDate();
			dateType = dateType.setDate(dateType.getDate() + 14);
			maxDate = new Date(dateType);
			// maxDate = new Date(moment(minDate).add(14, "day").format("YYYY-MM-DD"));
			this.byId("inputDepartureDate").setMinDate(minDate);
			this.byId("inputDepartureDate").setMaxDate(maxDate);
		},
		applyRuleToVendorAccess: function() {
			this.byId("inputEntryDate").setMinDate(new Date(moment().add(1, "day").format("YYYY-MM-DD")));
		},
		applyRulesToUrgentAccessInDialogForm: function(oEvent) {
			// var filters;
			var oReference = oEvent.getParameters().key;
			var valueModel = "";
			// var filterParam = "-";
			var dateType = new Date();
			if (oReference === "yes") {
				dateType = dateType.setDate(dateType.getDate() + 6);
				this.byId("calendar").setMinDate(new Date());
				this.byId("calendar").setMaxDate(new Date(dateType));
				valueModel = "X";
			} else {
				dateType = dateType.setDate(dateType.getDate() + 6);
				this.byId("calendar").setMinDate(new Date(dateType));
				this.byId("calendar").setMaxDate(null);
				valueModel = "-";
			}
			if (this.byId("calendar").getSelectedDates()[0]) {
				this.byId("calendar").getSelectedDates()[0].setStartDate();
			}
			var oModelDialogAccess = this.getModel("oModelDialogAccess");
			oModelDialogAccess.setProperty("/UrgentVisa", valueModel);
			oModelDialogAccess.setProperty("/DateArrive", "");
		},
		applyRulesToUrgentAccess: function(oEvent) {
			var filters;
			// var oReference = typeof oEvent === "string" ? oEvent : oEvent.getSource().getSelectedKey();
			var oReference = typeof oEvent === "string" ? oEvent : oEvent.getParameters().key;
			var filterParam = "-";
			this.byId("inputEntryDate").setValue();
			this.byId("inputDepartureDate").setValue();
			this.byId("inputApprover").setSelectedKey();
			this.byId("inputSupervisor").setSelectedKey();
			this.byId("inputEmailHost").setValue();
			this.byId("inputAreaHost").setValue();
			if (oReference === "yes") {
				// this.byId("inputEntryDate").setMinDate(new Date(moment().add(1, "day").format("YYYY-MM-DD")));
				// this.byId("inputEntryDate").setMaxDate(new Date(moment().add(7, "day").format("YYYY-MM-DD")));
				filterParam = "X";
			} else {
				this.byId("inputEntryDate").setMinDate(new Date(moment().add(7, "day").format("YYYY-MM-DD")));
				this.byId("inputEntryDate").setMaxDate(null);
			}
			filters = [new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, filterParam)];
			this.byId("inputApprover").getBinding("items").filter(filters, "Application"); // filtersSupervisor = [new Filter("SiteId", sap.ui.model.FilterOperator.EQ, "PEN")];
			// this.byId("inputSupervisor").getBinding("items").filter(filtersSupervisor, "Application");
		},
		applyRulesToFiltersInComboVisaForm: function(oEvent) {
			var filters, filtersSupervisor;
			// var oReference = typeof oEvent === "string" ? oEvent : oEvent.getSource().getSelectedKey();
			// var oReference = typeof oEvent === "string" ? oEvent : oEvent.getParameters().key;
			var filterParam = "-";
			if (this.getModel("oModelVisaClone").getProperty("/UrgentVisa") === "X") {
				filterParam = "X";
			}
			filters = [new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, filterParam)];
			this.byId("inputApprover").getBinding("items").filter(filters, "Application");
			filtersSupervisor = [new Filter("Siteid", sap.ui.model.FilterOperator.EQ, this.getModel("oModelVisaClone").getProperty(
				"/Siteid"))];
			this.byId("inputSupervisor").getBinding("items").filter(filtersSupervisor, "Application");
		},
		formatDateVisaDetail: function(value) {
			// var formatDate = ""
			// 	// this.getModel("oModelVisaClone")
			// if (oEvent) {
			// 	formatDate = moment(oEvent).format("YYYY-MM-DD");
			// }
			// if (oEvent !== "") {
			// 	return oEvent.toJSON().replace("T00:00:00.000Z", "");
			// }
			// return "";
			try {
				if (value !== "") {
					if (typeof value === "object") {
						// var oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({
						// 	pattern: "yyyy-MM-dd",
						// 	calendarType: sap.ui.core.CalendarType.Gregorian
						// });
						var oFormatYyyymmdd = value.toJSON().replace("T00:00:00.000Z", "");
						// return oFormatYyyymmdd.format(value);
						return oFormatYyyymmdd;
					} else {
						return value;
					}
				}
				return "";
			} catch (err) {
				return "";
			}
		},
		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		onUpdateStartedListVisa: function(oEvent) {
			_.each(oEvent.getSource().getItems(), function(values) {
				// console.log(values.getAttributes()[0].getText());
				switch (values.getAttributes()[0].getText()) {
					case "C":
					case "A":

						// values.getAttributes()[1].setText("Complete");
						values.getAttributes()[1].setText(this.getResourceBundle().getText("completeStatus"));

						var todayFormat = moment(new Date());
						var dueDatePoFormat = moment(values.getBindingContext().getObject().ExpiryDate.toJSON().replace(
							"T00:00:00.000Z", ""));
						var diffDate = dueDatePoFormat.diff(todayFormat, "days");

						console.log("--> " + diffDate)

						if (diffDate < 0) {
							values.addStyleClass("classDue");
						} else {
							values.addStyleClass("classSuccess");
						}
						break;
					case "P":

						values.getAttributes()[1].setText(this.getResourceBundle().getText("pendingStatus"));
						var todayFormat = moment(new Date());
						var dueDatePoFormat = moment(values.getBindingContext().getObject().ExpiryDate.toJSON().replace(
							"T00:00:00.000Z", ""));
						var diffDate = dueDatePoFormat.diff(todayFormat, "days");

						console.log("--> " + diffDate)

						if (diffDate < 0) {
							values.addStyleClass("classDue");
						} else {
							values.addStyleClass("classPending");
						}
						break;
					case "R":
					case "N":
					case "I":
						values.addStyleClass("classRejected");
						values.getAttributes()[1].setText(this.getResourceBundle().getText("rejectedStatus"));
						break;
				}
			}.bind(this)); // if (oEvent.getSource().getItems()[0].getAttributes()[0].getText()) {
			// }
			// console.log("onstartted");
			//This code was generated by the layout editor.
		},
		onCreateFragementAccessWithDataFromPopOver: function(_parameter) {
			var oViewApp = this.getView().getModel("appView");
			this.onChanceValueBusyIndicator(true);
			var _validatePoPendingOrComplete = this.validatePoPendingOrComplete();
			var validatePhone = this.getModel("device").getProperty("/system/desktop");
			var paramBindingContext = "";
			if (_parameter === 0) {
				oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText(
					"messageCantCreateAccess"));
				if (!_validatePoPendingOrComplete[0]) {
					MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
						duration: 5000,
						width: "25%",
						animationTimingFunction: "ease-in",
						animationDuration: 800
					});
					this.onChanceValueBusyIndicator(false);
					// this.onCancelEditAccess();
					return;
				}
			} else {
				if (validatePhone) {
					paramBindingContext = _parameter.getParameter("listItem");
				} else {
					paramBindingContext = _parameter.getSource();
				}
				this.getModel("oModelVisaClone").setData(this.getModel().getProperty(paramBindingContext.getBindingContext().getPath()));
			}
			var frgStr = "com.gc.passportvisits.view.fragments.PanelAccessRequestVisit";
			if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "MRO") {
				frgStr = "com.gc.passportvisits.view.fragments.PanelAccessRequestMx";
			}
			if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "COR") {
				frgStr = "com.gc.passportvisits.view.fragments.PanelAccessRequestVan";
			}
			if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "PEN") {
				frgStr = "com.gc.passportvisits.view.fragments.PanelAccessRequestVisit";
			}
			/*
						if (this.getResourceBundle().getText("appVersion") === "1") {
							frgStr = "com.gc.passportemployee.view.fragments.PanelAccessRequestVisit";
						}*/
			this.fragmentCreateAndReadVisa = sap.ui.xmlfragment(this.getView().getId(), frgStr, this);
			this.getView().byId("tabPO").addContent(this.fragmentCreateAndReadVisa);
			this.applyRulesToFiltersInComboVisaForm();
			// this.fragmentCreateAndReadVisa_AddFormSupervisor = sap.ui.xmlfragment(this.getView().getId(),
			// 	"com.gc.passportvisits.view.fragments.FormVisaSupervisor", this);
			// this.getView().byId("idFrg").addContent(this.fragmentCreateAndReadVisa_AddFormSupervisor)
			if (_parameter === 0) {
				this.byId("inputDepartureDate").setValue();
				var dateReferEntry = new Date(this.byId("inputEntryDate").getValue());
				var afterFourteenDays = dateReferEntry;
				// oneYearMore = oneYearMore.setFullYear(dateReferEntry.getFullYear() + 1);
				afterFourteenDays = afterFourteenDays.setDate(afterFourteenDays.getDate() + 13);
				this.byId("inputDepartureDate").setMaxDate(new Date(afterFourteenDays));
				this.byId("inputDepartureDate").setMinDate(new Date(this.byId("inputEntryDate").getValue())); // this.byId("inputDepartureDate").setMaxDate(this.byId("inputEntryDate").getDateValue());
			}
			// this.byId("inputDepartureDate").setValue();
			if (_parameter === 0) {
				oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageCreateAccess"));
				if (_validatePoPendingOrComplete[0]) {
					this.applyRuleToVendorAccess();

					this.onCreateAccess(); // var resultLength = (_validatePoPendingOrComplete[1] ? _validatePoPendingOrComplete[1].length : 0);
					// if (resultLength > 0) {
					// 	this.setFilterVisasByStatus(_validatePoPendingOrComplete[1]);
					// }
				} else {
					MessageToast.show(this.getResourceBundle().getText("messageCantCreateAccess"), {
						duration: 5000,
						width: "25%",
						animationTimingFunction: "ease-in",
						animationDuration: 800
					});
					this.onChanceValueBusyIndicator(false);
					this.onCancelEditAccess();
					return;
				}
			} else {
				oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageReadOnly"));
				this.onDetailAccess(_parameter);
			}
			MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
				duration: 5000,
				width: "25%",
				animationTimingFunction: "ease-in",
				animationDuration: 800
			});
			// if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "MRO") {
			// 	this.byId("inputFloor").addStyleClass("sapMSwt2");
			// 	this.byId("inputMezz").addStyleClass("sapMSwt2");
			// } else if (this.getModel("oModelVisaClone").getProperty("/Siteid") === "COR") {
			// 	this.byId("inputMezz").addStyleClass("sapMSwt2");
			// } else {
			// 	this.byId("inputTruckShop").addStyleClass("sapMSwt2");
			// 	this.byId("inputPit").addStyleClass("sapMSwt2");
			// 	this.byId("inputSupplyArea").addStyleClass("sapMSwt2");
			// 	this.byId("inputTrailingDam").addStyleClass("sapMSwt2");
			// 	this.byId("inputCamp").addStyleClass("sapMSwt2");
			// 	this.byId("inputProcessPlant").addStyleClass("sapMSwt2");
			// 	this.byId("inputProcessWarehouse").addStyleClass("sapMSwt2");
			// 	this.byId("inputProjectAreas").addStyleClass("sapMSwt2");
			// 	this.byId("inputNewAdmin").addStyleClass("sapMSwt2");
			// 	this.byId("inputOldAdmin").addStyleClass("sapMSwt2");
			// 	this.byId("inputNPSC").addStyleClass("sapMSwt2");
			// 	this.byId("inputRH").addStyleClass("sapMSwt2");
			// 	this.byId("inputItSite").addStyleClass("sapMSwt2");
			// } // console.log("-->");
			// // console.log(oEvent);
		},
		onCreateFragementAccessDialog: function() {
			var oModelDialogAccess = new JSONModel({
				DateArrive: "",
				SiteValue: "",
				UrgentVisa: "no"
			});
			this.setModel(oModelDialogAccess, "oModelDialogAccess");
			this.onChanceValueBusyIndicator(true);
			if (this.fragmentDialogCreateNewRequest === "") {
				// this._oDialog = sap.ui.xmlfragment("sap.m.sample.SelectDialog.Dialog", this);
				var frgStr = "com.gc.passportvisits.view.fragments.DialogNewAcess";
				this.fragmentDialogCreateNewRequest = sap.ui.xmlfragment(this.getView().getId(), frgStr, this);
				this.fragmentDialogCreateNewRequest.setModel(this.getView().getModel());
				this.getView().addContent(this.fragmentDialogCreateNewRequest);
				// this.byId("siteSegment").setSelectedKey("0");
				var entryDate = new Date();
				entryDate = entryDate.setDate(entryDate.getDate() + 6);
				this.byId("calendar").setMinDate(new Date(entryDate));
				this.onSiteSegmentSelect();
			}
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this.fragmentDialogCreateNewRequest);
			this.fragmentDialogCreateNewRequest.open();
		},
		onSiteSegmentSelect: function(oEvent) {
			// console.log("site");
			// console.log(oEvent);
			var oSite = "";
			if (typeof oEvent !== undefined) {
				this.byId("pen").setSelected(true);
				oSite = "PEN";
			} else {
				this.byId("pen").setSelected(false);
				this.byId("cdmx").setSelected(false);
				this.byId("cor").setSelected(false);
				this.byId(oEvent.getSource().getId()).setSelected(true);
				if (oEvent.getSource().getId().indexOf("cor") !== -1) {
					oSite = "COR";
				}
				if (oEvent.getSource().getId().indexOf("cdmx") !== -1) {
					oSite = "MRO";
				}
				if (oEvent.getSource().getId().indexOf("pen") !== -1) {
					oSite = "PEN";
				}
			}
			var oModelDialogAccess = this.getModel("oModelDialogAccess");
			oModelDialogAccess.setProperty("/SiteValue", oSite);
		},
		onSaveDialogCreateAccess: function(oEvent) {
			// this.onChanceValueBusyIndicator(true);
			this.fragmentDialogCreateNewRequest.setBusy(true);
			// console.log("onsave");
			// console.log(oEvent);
			var oModelDialogAccess = this.getModel("oModelDialogAccess");
			var oViewApp = this.getView().getModel("appView");
			var oOdataUtility = new OdataUtility();
			if (oModelDialogAccess.getProperty("/UrgentVisa") !== "" && oModelDialogAccess.getProperty("/DateArrive") !==
				"" && oModelDialogAccess.getProperty("/SiteValue") !== "") {
				this.onChanceValueBusyIndicator(false);
				var oModelVisaEmpty = oOdataUtility.getEmptyVisaModel();
				this.getModel("oModelVisaClone").setData(oModelVisaEmpty.results[0]);
				this.getModel("oModelVisaClone").setProperty("/IssuedDate", oModelDialogAccess.getProperty("/DateArrive"));
				this.getModel("oModelVisaClone").setProperty("/Siteid", oModelDialogAccess.getProperty("/SiteValue"));
				this.getModel("oModelVisaClone").setProperty("/UrgentVisa", oModelDialogAccess.getProperty("/UrgentVisa"));
				// this._oDialog.close();
				this.onCloseDialogCreateAccess();
				this.onCreateFragementAccessWithDataFromPopOver(0); // this.onCreateFragementAccess(0);
			} else {
				oViewApp.setProperty("/messages/messageToastSelect", this.getResourceBundle().getText("messageFailValidation"));
				MessageToast.show(oViewApp.getProperty("/messages/messageToastSelect"), {
					duration: 5000,
					width: "25%",
					// at: "center",
					animationTimingFunction: "ease-in",
					animationDuration: 800
				});
				this.fragmentDialogCreateNewRequest.setBusy(false);
			} // this._oDialog.destroy(true);
		},
		onCloseDialogCreateAccess: function(oEvent) {
			// console.log("onclose");
			// console.log(oEvent);
			this.onChanceValueBusyIndicator(false);
			this.fragmentDialogCreateNewRequest.close();
			this.fragmentDialogCreateNewRequest.destroy(true);
			this.fragmentDialogCreateNewRequest = "";
		},
		onArrivalDate: function(oEvent) {
			// console.log("arrival");
			// console.log(oEvent);
			var oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({
				pattern: "yyyy-MM-dd",
				calendarType: sap.ui.core.CalendarType.Gregorian
			});
			var dateSelected = oFormatYyyymmdd.format(oEvent.getSource().getSelectedDates()[0].getStartDate());
			// console.log(dateSelected);
			var oModelDialogAccess = this.getModel("oModelDialogAccess");
			oModelDialogAccess.setProperty("/DateArrive", dateSelected);
		},
		validatePoPendingOrComplete: function() {
			var oStatusVisa = _.filter(this.getModel("oModelVisaSet").getProperty("/results"), function(value) {
				if (value.Status === "P" || value.Status === "C" || value.Status === "A") {
					var currentRequestSite = this.getModel("oModelDialogAccess") ? this.getModel("oModelDialogAccess").getProperty(
						"/SiteValue") : "";
					if (currentRequestSite === value.Siteid) {
						var issuedDate = this.formatDateVisaDetail(value.IssuedDate);
						var expiryDate = this.formatDateVisaDetail(value.ExpiryDate);
						var currentRequestArriveDate = this.getModel("oModelDialogAccess").getProperty("/DateArrive");
						if (currentRequestArriveDate >= issuedDate && currentRequestArriveDate <= expiryDate) {
							return [true];
						}
					} // this.
					// if()
				}
			}.bind(this));
			if (oStatusVisa.length === 0) {
				return [true];
			}
			return [false]; // console.log("validar")
		},
		formatSiteViewVisa: function(oEvent) {
			var site = "";
			switch (oEvent) {
				case "MRO":
					site = "Cd.Mx.";
					break;
				case "PEN":
					site = "Pe\xF1asquito";
					break;
				case "COR":
					site = "Vancouver";
					break;
			}
			return site;
		},
		onShowDialogToSelectInList: function() {
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment(this.getView().getId(),
					"com.gc.passportvisits.view.fragments.DialogListToShowSource", this);
				this._oDialog.setModel(this.getView().getModel());
			}
			var filterParam = "-";
			if (this.getModel("oModelVisaClone").getProperty("/UrgentVisa") === "X") {
				filterParam = "X";
			}
			var filters = [new Filter("IUrgentVisa", sap.ui.model.FilterOperator.EQ, filterParam)];
			this._oDialog.getBinding("items").filter(filters, "Application");
			// this._oDialog.getBinding("items").filter([]);
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
			this._oDialog.open();
		},
		onShowDialogToSelectInListSupervisor: function() {
			if (!this._oDialogSupervisor) {
				this._oDialogSupervisor = sap.ui.xmlfragment(this.getView().getId(),
					"com.gc.passportvisits.view.fragments.DialogListToShowSourceSuperVisor", this);
				this._oDialogSupervisor.setModel(this.getView().getModel());
			}
			var filtersSupervisor = [new Filter("Siteid", sap.ui.model.FilterOperator.EQ, this.getModel("oModelVisaClone").getProperty(
				"/Siteid"))];
			this._oDialogSupervisor.getBinding("items").filter(filtersSupervisor, "Application");
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogSupervisor);
			this._oDialogSupervisor.open();
		},
		handleCloseApprover: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				MessageToast.show(aContexts.map(function(oContext) {
					return oContext.getObject().Passportid + ": " + oContext.getObject().NameText;
				}).join(", "));
				this.getModel("oModelVisaClone").setProperty("/Approver", aContexts[0].getObject().Passportid);
				this.byId("inputEmailHost").setValue(aContexts[0].getObject().SmtpAddr);
				this.byId("inputAreaHost").setValue(aContexts[0].getObject().Department);
			}
		},
		handleCloseSupervisor: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				MessageToast.show(aContexts.map(function(oContext) {
					return oContext.getObject().Passportid + ": " + oContext.getObject().NameText;
				}).join(", "));
				this.getModel("oModelVisaClone").setProperty("/Supervisor", aContexts[0].getObject().Passportid);
			}
		},
		handleSearcDialogList: function(oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = [new Filter("NameText", sap.ui.model.FilterOperator.EQ, sValue)];
			oEvent.getSource().getBinding("items").filter(oFilter);
		},
		handleLoadItems: function(oControlEvent) {
			oControlEvent.getSource().getBinding("items").resume();
		},
		onBeforeOpenCombo: function() {
			console.log("onBefore");
		},
		handleWirteItem: function() {
			console.log("handleWirteItem");
		},

		renderConfigAreas: function() {

			var oOdataUtility = new OdataUtility();
			var oController = this;

			var oSite = oController.getModel("oModelVisaClone").getProperty("/Siteid");
			var filters = [new Filter("Siteid", sap.ui.model.FilterOperator.EQ, oSite)];
			var readAreas = oOdataUtility.requestAreas(oController, filters);

			readAreas
				.then(oController.createModelsTabsAndContent.bind(oController))
				.then(oController.createTabsAreas.bind(oController))
				.then(oController.createContentSelection.bind(oController))
				.catch(function(res) {
					console.log("-->Error Tabs Areas");
					console.log(res);
				})

		},

		createModelsTabsAndContent: function(oResults) {
			var results = oResults;
			var oController = this;
			return new Promise(function(resolve, reject) {

				var oModel = new JSONModel();
				oModel.setData(results);
				oController.getView().setModel(oModel, "oModelAreasSet");
				var oModelItemAreas = new JSONModel({
					"Areas": [],
					"RowVisibles": "",
					"NodeExpanded": {}
				})
				var oModelItemAreasSelection = new JSONModel({})

				_.map(oController.getView().getModel("oModelAreasSet").getProperty("/results"), function(value, index) {
					console.log(value.HeaderArea + " " + value.IterAreasDesc)
						// oModelItemAreas.setProperty("/" + value.HeaderArea, value.IterAreasDesc.split(","));
						// var oModelTemp = new JSONModel({});
					var areasNames = new JSONModel({
						"areasNames": []
					})

					var oHead = value.HeaderArea;

					_.map(value.IterAreasDesc.split(","), function(value) {
						console.log("nameArea:" + value);
						areasNames.getProperty("/areasNames").push({

							"nameArea": value.split(":")[1],
							"idArea": value.split(":")[0],
							"headArea": oHead

						})
					}.bind(this))

					// console.log(arrTemp);

					oModelItemAreas.getProperty("/Areas").push({
						"nameArea": value.DescHeaderAreas,
						"nameIdArea": value.HeaderArea,
						"categories": areasNames.getProperty("/areasNames"),
						"rootLevel": index
					})

					// oModelItemAreas.getProperty("/" + value.HeaderArea).push(value.IterAreasDesc.split(","));
				}.bind(oController))

				console.log(oModelItemAreas);
				oController.getView().setModel(oModelItemAreas, "oModelItemAreas");
				oController.getView().getModel("oModelItemAreas").setProperty("/RowVisibles", oController.getView().getModel(
					"oModelItemAreas").getProperty("/Areas").length);
				oController.getView().setModel(oModelItemAreasSelection, "oModelItemAreasSelection");

				console.log(oController.getView().getModel("oModelItemAreas"));

				resolve();
			});
		},
		createTabsAreas: function() {
			var oController = this;
			var oBase = new BaseUtilityComponent();
			return new Promise(function(resolve, reject) {
				var oTreeTable = oBase.createTreeTable("treeTable", "styelTreeTable", oController.getView().getModel(
					"oModelItemAreas"));

				oTreeTable.attachRowSelectionChange(oController.onPressListItem.bind(oController));
				oTreeTable.attachToggleOpenState(oController.onNodeExpand.bind(oController))
				if (oController.getModel("oModelVisaClone").getProperty("/Siteid") === "PEN") {
					oTreeTable.setVisibleRowCount(oController.getView().getModel("oModelItemAreas").getProperty("/RowVisibles"));
				} else {
					oTreeTable.setVisibleRowCount(3);

				}

				var areasS = oController.getResourceBundle().getText("areasS");
				var oLabel = oBase.createLabelWitoutId(areasS);
				var oText = new sap.m.Text();
				oText.bindProperty("text", "nameArea");
				var oColumn = oBase.createColumn(oLabel, oText);
				oTreeTable.addColumn(oColumn);
				oTreeTable.bindRows({
					path: '/Areas',
					parameters: {
						arrayNames: ['categories']
					}
				});
				var oPanel = oController.byId("panelAreas");
				oPanel.addContent(oTreeTable);

				console.log("->End Created icon tab bar with content panel and tiles<-");
				resolve();

			});

		},
		createContentSelection: function() {
			var oController = this;

			return new Promise(function(resolve, reject) {
				var oBase = new BaseUtilityComponent();
				oController.getView().byId("iconTabBarAreas");

				var oTokenizer = new sap.m.Tokenizer("TokenList");

				var oVBox = oBase.createVBox("vboxTokens", "Inherit");

				oVBox.addItem(oTokenizer)

				var areasSelected = oController.getResourceBundle().getText("areasSelected");

				var oPanelToken = oBase.createPanel("PanelTokens", "stylePanelToken", areasSelected, oVBox,
					"Solid");

				oTokenizer.bindAggregation(
					"tokens",
					"oModelItemAreasSelection>/",
					oController.onCreateTokenSelection.bind(oController)
				);
				var oPanel = oController.byId("panelAreas");

				oPanel.addContent(oPanelToken);
				oController.onChanceValueBusyIndicator(false);

				resolve();

			});
		},

		onPressListItem: function(oEvent) {
			var oController = this;
			// sap.ui.getCore().byId("treeTable").setBusy(true);
			oController.onChanceValueBusyIndicator(true);
			// console.log("add an item selection")
			// console.log(oEvent)
			// console.log(oEvent.getSource().getId())

			var _oEvent = oEvent;
			var _oEventSource = _oEvent.getSource();
			if (_oEvent.getParameters().userInteraction) {
				sap.ui.getCore().byId("treeTable").expandToLevel(1);

				setTimeout(function() {
					oController.getModel("oModelItemAreasSelection").setData({});

					if (_oEventSource.getBinding().getSelectedContexts().length > 0) {
						_.map(_oEventSource.getBinding().getSelectedContexts(), function(values) {
							if (values.getObject().idArea) {
								oController.getModel("oModelItemAreasSelection").setProperty("/select-" + values.getObject().headArea +
									"-" +
									values.getObject().idArea, values.getObject().nameArea);
							}
						}.bind(oController))
					}

					setTimeout(function() {
						// sap.ui.getCore().byId("treeTable").setBusy(false);
						sap.ui.getCore().byId("treeTable").collapseAll();
						sap.ui.getCore().byId("treeTable").expand(oController.getView().getModel("oModelItemAreas").getProperty(
							"/NodeExpanded"));
						setTimeout(function() {
							oController.onChanceValueBusyIndicator(false);
						}.bind(oController), 50)

					}.bind(oController), 100);
				}.bind(oController), 100);

			}
		},
		onNodeExpand: function(oEvent) {
			var oController = this;
			if (typeof oController.getView().getModel("oModelItemAreas").getProperty("/NodeExpanded") === "number") {
				if (oController.getView().getModel("oModelItemAreas").getProperty("/NodeExpanded") !== parseInt(oEvent.getParameters()
						.rowContext.getPath().replace("/Areas/", ""))) {
					sap.ui.getCore().byId("treeTable").collapseAll();
					oController.getView().getModel("oModelItemAreas").setProperty("/NodeExpanded",
						parseInt(oEvent.getParameters().rowContext.getPath().replace("/Areas/", "")));
					sap.ui.getCore().byId("treeTable").expand(oController.getView().getModel("oModelItemAreas").getProperty(
						"/NodeExpanded"));
				}
			} else {
				oController.getView().getModel("oModelItemAreas").setProperty("/NodeExpanded",
					parseInt(oEvent.getParameters().rowContext.getPath().replace("/Areas/", "")));
			}
		},

		onCreateTokenSelection: function(sId, oContext) {
			var oController = this;
			// var oBase = new BaseUtilityComponent();

			var oToken = new sap.m.Token(sId, {
				key: oContext.getPath(),
				text: oContext.getObject(),
				delete: oController.deleteToken.bind(oController)
			});
			console.log(oContext.getObject());

			return oToken;
		},
		deleteToken: function(oEvent) {
			console.log(oEvent)

			var itemKeyDel = oEvent.getSource().getText();
			var itemIndexDel = "";
			sap.ui.getCore().byId("treeTable").expandToLevel(1);
			setTimeout(function() {

				_.map(sap.ui.getCore().byId("treeTable").getBinding().getSelectedContexts(), function(values,
					index) {

					if (itemKeyDel === values.getObject().nameArea) {
						itemIndexDel = index;
					}
				}.bind(this));

				var indexSelected = sap.ui.getCore().byId("treeTable").getSelectedIndices()[itemIndexDel];
				// 

				sap.ui.getCore().byId("treeTable").removeSelectionInterval(indexSelected, indexSelected);
				setTimeout(function() {
						this.onChanceValueBusyIndicator(false);
						sap.ui.getCore().byId("treeTable").collapseAll();
					}.bind(this), 100)
					// 

				// sap.ui.getCore().byId("treeTable").refresh();

			}.bind(this), 100);

			delete this.getModel("oModelItemAreasSelection").oData[oEvent.getSource().getKey().replace("/", "")]

			this.getModel("oModelItemAreasSelection").refresh();

		},
		renderSectionAccess: function() {

			var oOdataUtility = new OdataUtility();
			var oController = this;
			var readAreas = oOdataUtility.requestAreasByData(oController);
			var oBase = new BaseUtilityComponent();

			readAreas.then(function(response) {
					console.log(response)
					oController.getView().byId("iconTabBarAreas");

					var oModel = new JSONModel({});
					var oModelItemAreas = new JSONModel({});

					oModel.setProperty("/itemsAreasRead", response.IterAreasDesc.split(","));
					_.map(oModel.getProperty("/itemsAreasRead"), function(value) {
						console.log(value)
						oModelItemAreas.setProperty("/" + value.split(":")[0], value.split(":")[1]);
					})
					var areasSelected = oController.getResourceBundle().getText("areasSelected");
					var oListSelection = oBase.createList("SelectionList", "styleBlock", areasSelected, oController.onPressDeleteListItem);
					oListSelection.setModel(oModelItemAreas, "oModelItemAreas")
					oListSelection.bindAggregation(
						"items",
						"oModelItemAreas>/",
						oController.onCreateContentListSelection.bind(oController)
					);
					// oListSelection.setBlocked(true)

					var oPanel = oController.byId("panelAreas");

					oPanel.addContent(oListSelection);
					oController.onChanceValueBusyIndicator(false);
				})
				.catch(function(res) {
					console.log("-->Error Tabs Areas");
					console.log(res);
				})

			// var oModel = this.getView().getModel("CATALOGS");
			// var oController = this;

		},
		validateAreasAccess: function() {
			if (this.getModel("oModelItemAreasSelection").getJSON().length > 2) {
				return true;
			} else {
				return false;
			}

		},

		onCreateContentListSelection: function(sId, oContext) {
			var oController = this;
			var oBase = new BaseUtilityComponent();

			var oListItem = oBase.createStandardListItemDeletetion("item-" + sId, oContext.getObject());
			console.log(oContext.getObject());

			return oListItem;

		},

		/**
		 *@memberOf com.gc.passportvisits.controller.Detail
		 */
		function: function(oEvent) {
			//This code was generated by the layout editor.
		}
	});
});