sap.ui.define([
	"com/gc/passportvisits/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function(BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("com.gc.passportvisits.controller.App", {

		onInit: function() {

			var oViewModel,
				oViewModelBusy,
				oInputModel,
				fnSetAppNotBusy,
				oVisaModel,
				oListSelector = this.getOwnerComponent().oListSelector;
			// iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();
			oListSelector._oAppVersion = this.getResourceBundle().getText("appVersion");
			oListSelector._oThis = this;
			oViewModelBusy = new JSONModel({
				busy: true,
				delay: 0

			});
			this.setModel(oViewModelBusy, "appViewBusy");
			oInputModel = new JSONModel({
				inputImss: 20,
				inputDriverLicense: 20,
				inputNameFirst: 40,
				inputNamemiddle: 40,
				inputNameLast: 40,
				inputNameLast2: 40,
				inputNickname: 40,
				inputStreet: 60,
				HouseNum1: 10,
				inputCity1: 40,
				inputPostCode1: 10,
				inputEmail: 241,
				inputTelNumber: 30,
				inputNombreFamiliar: 80,
				inputParentesco: 40,
				inputNombreConyugue: 80,
				inputEmpresa: 40,
				inputContactName: 80,
				inputContactRelationship: 30,
				inputContactPhone: 30
			});
			this.setModel(oInputModel, "oInputModel");

			oVisaModel = new JSONModel({

				TranspDescrip: "",
				Flight: "",
				ChkComputer: "",
				Visaid: "",
				FlightDescrip: "",
				TypeWork: "",
				ComputerEquip: "",
				Siteid: "",
				RegularityVisit: "",
				Passportid: "",
				ReqSupervision: "",
				VisaType: "",
				VisitReason: "",
				Status: "",
				LunchService: "",
				CampService: "",
				Transportation: "",
				InductionCourse: "",
				SysStatus: "",
				IssuedDate: "",
				ExpiryDate: "",
				SiteHost: "",
				PurchDoc: ""
			});

			this.setModel(oVisaModel, "oModelVisaClone");

			oViewModel = new JSONModel({
				bulkProfile: false,
				showBulkData: false,
				availableButtonCreateProfile: true,
				blockForms: true,
				firstLoad: true,
				statusActiveModel: true,
				statusActiveModel_: "A",
				issuedBy: "",
				titleDetail: this.getResourceBundle().getText("titleDisplayProfile"),
				optionNal: "C",
				optionNalId: 0,
				availableEdit: true,
				isEjidatario: -1,
				availableVisibleMaster: true,
				configDetailProfile: {
					showDetailAccess: false,
					showTableAccess: true,
					editDetailAccess: false,
					// availableEdition:false,
					createAccess: false,
					creating: false,
					editing: false,
					lengthToUuid: 18,
					businessCardVisible: true,
					validatePersonalId: false,
					validatePO: true,
					showAllTabs: true, //"{appView>/configDetailProfile/validatePersonalId}"
					showInitTab: "tabInfo",
					labelUid: "CURP", //	oViewApp.setProperty("/configDetailProfile/showAllTabs", true);
					showSelectedPO: true,
					showSelectedPOAlter: false,
					showSelectionList: "Inactive"

				},
				messages: {
					messageToastSelect: this.getResourceBundle().getText("messageReadOnly")
				},
				validateSpecificPolicy: {
					uid: {
						item: "inputValidateUidType",
						type: "Input",
						inputAcept: "AlfaNum",
						dependentFields: {
							pathItem: "typeUidDataGroup",
							prop: "getSelectedIndex"
						}
					}
				},
				validatePolicyAccessInformation: [
					{
						item: "inputTypeWork",
						// item: this.byId("inputNameFirst"),
						tab: "tabPO",
						type: "Select",
						inputAcept: "AlfaNumCode"
							// defaultValidate: true,
							// predefinedExpReg: /^([0-9]{11})+$/g
					},
					{
						item: "inputPOCombo",
						// item: this.byId("inputNameFirst"),
						tab: "tabPO",
						type: "Select",
						inputAcept: "AlfaNumCode"
							// defaultValidate: true,
							// predefinedExpReg: /^([0-9]{11})+$/g
					}
				],
				validatePolicyAccessInformationVisit: [
					{
						item: "inputVisitReason",
						tab: "tabPO",
						type: "Select",
						inputAcept: "AlfaNumCode"
					},
					{
						item: "inputVisitReasonOther",
						tab: "tabPO",
						type: "Input",
						inputAcept: "AlfaNumCode",
						dependentFields: {
							pathItem: "inputVisitReason",
							value: "5",
							prop: "getSelectedKey"
						}

					},
					{
						item: "inputEntryDate",
						tab: "tabPO",
						type: "Date",
						inputAcept: "AlfaNumCode",
						exceptValue: " "
					},
					{
						item: "inputDepartureDate",
						tab: "tabPO",
						type: "Date",
						inputAcept: "AlfaNumCode",
						exceptValue: " "
					},
					// {
					// 	item: "inputReqSuper",
					// 	tab: "tabPO",
					// 	type: "SegmentedButton",
					// 	// inputAcept: "AlfaNumCode",
					// 	exceptValue: "not"
					// 		// inputAcept: "AlfaNumCode"
					// },
					{
						item: "inputGuard",
						tab: "tabPO",
						// type: "Option",
						// exceptValue: -1
						type: "SegmentedButton",
						// inputAcept: "AlfaNumCode",
						exceptValue: "not"
							// inputAcept: "AlfaNumCode"

					},

					{
						item: "inputSerial",
						tab: "tabPO",
						type: "Input",
						// exceptValue: -1
						inputAcept: "AlfaNumCode",
						dependentFields: {
							pathItem: "inputGuard",
							value: "yes",
							prop: "getSelectedKey"
						}
					},
					{
						item: "inputApprover",
						tab: "tabPO",
						type: "Select",
						// exceptValue: -1
						inputAcept: "AlfaNumCode"
					},
					{
						item: "inputSupervisor",
						tab: "tabPO",
						type: "Select",
						// exceptValue: -1
						inputAcept: "AlfaNumCode"
					},

					{
						item: "inputSiteAcomodation",
						tab: "tabPO",
						// type: "Option",
						// exceptValue: -1
						type: "SegmentedButton",
						// inputAcept: "AlfaNumCode",
						exceptValue: "not"
							// inputAcept: "AlfaNumCode"

					},
					{
						item: "inputSiteComedor",
						tab: "tabPO",
						// type: "Option",
						// exceptValue: -1
						type: "SegmentedButton",
						// inputAcept: "AlfaNumCode",
						exceptValue: "not"
							// inputAcept: "AlfaNumCode"

					},
					// {
					// 	item: "inputLand",
					// 	tab: "tabPO",
					// 	// type: "Option",
					// 	// exceptValue: -1
					// 	type: "SegmentedButton",
					// 	// inputAcept: "AlfaNumCode",
					// 	exceptValue: "not"
					// 		// inputAcept: "AlfaNumCode"

					// },
					{
						item: "inputTransportation",
						tab: "tabPO",
						// type: "Option",
						// exceptValue: -1
						type: "SegmentedButton",
						// inputAcept: "AlfaNumCode",
						exceptValue: "not"
							// inputAcept: "AlfaNumCode"

					},

					{
						item: "inputLandDescr",
						tab: "tabPO",
						// type: "Option",
						// exceptValue: -1
						type: "SegmentedButton",
						// inputAcept: "AlfaNumCode",
						exceptValue: "not",
						dependentFields: {
							pathItem: "inputTransportation",
							value: "yesL",
							prop: "getSelectedKey"
						}
						// inputAcept: "AlfaNumCode"

					},

					// {
					// 	item: "inputAir",
					// 	tab: "tabPO",
					// 	// type: "Option",
					// 	// exceptValue: -1
					// 	type: "SegmentedButton",
					// 	// inputAcept: "AlfaNumCode",
					// 	exceptValue: "not"
					// 		// inputAcept: "AlfaNumCode"

					// },
					{
						item: "inputAirDescr",
						tab: "tabPO",
						// type: "Option",
						// exceptValue: -1
						type: "SegmentedButton",
						// inputAcept: "AlfaNumCode",
						exceptValue: "not",
						dependentFields: {
							pathItem: "inputTransportation",
							value: "yesF",
							prop: "getSelectedKey"
						}
						// inputAcept: "AlfaNumCode"

					},
				],
				validatePolicyAccessInformationVisitMxVan: [
					{
						item: "inputVisitReason",
						tab: "tabPO",
						type: "Select",
						inputAcept: "AlfaNumCode"
					},
					{
						item: "inputVisitReasonOther",
						tab: "tabPO",
						type: "Input",
						inputAcept: "AlfaNumCode",
						dependentFields: {
							pathItem: "inputVisitReason",
							value: "5",
							prop: "getSelectedKey"
						}

					},
					{
						item: "inputEntryDate",
						tab: "tabPO",
						type: "Date",
						inputAcept: "AlfaNumCode",
						exceptValue: " "
					},
					{
						item: "inputDepartureDate",
						tab: "tabPO",
						type: "Date",
						inputAcept: "AlfaNumCode",
						exceptValue: " "
					},
					// {
					// 	item: "inputReqSuper",
					// 	tab: "tabPO",
					// 	type: "SegmentedButton",
					// 	// inputAcept: "AlfaNumCode",
					// 	exceptValue: "not"
					// 		// inputAcept: "AlfaNumCode"
					// },
					{
						item: "inputGuard",
						tab: "tabPO",
						// type: "Option",
						// exceptValue: -1
						type: "SegmentedButton",
						// inputAcept: "AlfaNumCode",
						exceptValue: "not"
							// inputAcept: "AlfaNumCode"

					},

					{
						item: "inputSerial",
						tab: "tabPO",
						type: "Input",
						// exceptValue: -1
						inputAcept: "AlfaNumCode",
						dependentFields: {
							pathItem: "inputGuard",
							value: "yes",
							prop: "getSelectedKey"
						}
					},
					{
						item: "inputApprover",
						tab: "tabPO",
						type: "Select",
						// exceptValue: -1
						inputAcept: "AlfaNumCode"
					}

				],
				validatePolicyBasicInformationVisits: [
					{ //
						item: "inputNameFirst",
						// item: this.byId("inputNameFirst"),
						tab: "tabInfo",
						type: "Input",
						inputAcept: "String"
					}, {
						item: "inputNameLast",
						// item: this.byId("inputNameLast"),
						tab: "tabInfo",
						type: "Input",
						inputAcept: "String"
					}, {
						item: "inputBirthdate",
						// item: this.byId("inputBirthdate"),
						tab: "tabInfo",
						type: "Date",
						inputAcept: "AlfaNumCode"
							// validateMaskFunction: "validateAdult"
							// predefinedExpReg: /^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/g
					},

					{
						item: "isEjidatario",
						// item: this.byId("inputBirthdate"),
						tab: "tabInfo",
						type: "Option",
						exceptValue: -1
							// inputAcept: "String",
							// predefinedExpReg: /^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/g
					},
					{
						item: "inputLanguP",
						tab: "tabInfo",
						type: "Select",
						inputAcept: "AlfaNumCode"
					},

					{
						item: "inputEmail",
						tab: "tabContac",
						type: "Input",
						validateMaskFunction: "validateEmail"
					}, {
						item: "inputTelNumber",
						tab: "tabContac",
						type: "Input",
						inputAcept: "Number"
							// validateMaskFunction: "validateEmail"
					},
					{
						item: "inputContactName",
						tab: "tabContac",
						type: "Input",
						inputAcept: "String"
							// validateMaskFunction: "validateEmail"
					},
					{
						item: "inputContactRelationship",
						tab: "tabContac",
						type: "Input",
						inputAcept: "String"
							// validateMaskFunction: "validateEmail"
					}, {
						item: "inputContactPhone",
						tab: "tabContac",
						type: "Input",
						inputAcept: "Number"
							// validateMaskFunction: "validateEmail"
					}
				],
				validatePolicyBasicInformation: [{
						item: "inputImss",
						// item: this.byId("inputNameFirst"),
						tab: "tabInfo",
						type: "Input",
						inputAcept: "Number",
						defaultValidate: true,
						dependentFields: {
							pathItem: "typeUidDataGroup1",
							value: 0,
							prop: "getSelectedIndex"
						},
						predefinedExpReg: /^([0-9]{11})+$/g
					}, { //
						item: "inputNameFirst",
						// item: this.byId("inputNameFirst"),
						tab: "tabInfo",
						type: "Input",
						inputAcept: "String"
					}, {
						item: "inputNameLast",
						// item: this.byId("inputNameLast"),
						tab: "tabInfo",
						type: "Input",
						inputAcept: "String"
					}, {
						item: "inputBirthdate",
						// item: this.byId("inputBirthdate"),
						tab: "tabInfo",
						type: "Date",
						// inputAcept: "String",
						validateMaskFunction: "validateAdult"
							// predefinedExpReg: /^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/g
					},

					{
						item: "isEjidatario",
						// item: this.byId("inputBirthdate"),
						tab: "tabInfo",
						type: "Option",
						exceptValue: -1
							// inputAcept: "String",
							// predefinedExpReg: /^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/g
					},


					{
						item: "inputStreet",
						// item: this.byId("inputStreet"),
						tab: "tabAddress",
						type: "Input",
						inputAcept: "AlfaNumCode"
					}, {
						item: "HouseNum1",
						// item: this.byId("HouseNum1"),
						tab: "tabAddress",
						type: "Input",
						inputAcept: "AlfaNumCode"
					}, {
						item: "inputCountry",
						// item: this.byId("inputCountry"),
						tab: "tabAddress",
						type: "Select",
						inputAcept: "AlfaNum"
					}, {
						item: "inputPostCode1",
						tab: "tabAddress",
						type: "Input",
						inputAcept: "AlfaNum"
					},
					{
						item: "inputEmail",
						tab: "tabContac",
						type: "Input",
						validateMaskFunction: "validateEmail"
					}, {
						item: "inputTelNumber",
						tab: "tabContac",
						type: "Input",
						inputAcept: "Number"
							// validateMaskFunction: "validateEmail"
					},
					{
						item: "inputContactName",
						tab: "tabContac",
						type: "Input",
						inputAcept: "String"
							// validateMaskFunction: "validateEmail"
					}, {
						item: "inputContactPhone",
						tab: "tabContac",
						type: "Input",
						inputAcept: "Number"
							// validateMaskFunction: "validateEmail"
					}
				],
				validatePolicyBasicInformationBulkData: [
					{
						item: "bulkInputVendor",
						column: "columnBulkInputVendor",
						// item: this.byId("inputNameFirst"),
						tab: "tabInfo",
						type: "Input",
						inputAcept: "AlfaNumCode",
						predefinedExpReg: /^[GC]\w[0-9]{6}$/g

					},
					{
						item: "bulkInputContLocat",
						column: "columnBulkContLocat",
						// item: this.byId("inputNameFirst"),
						tab: "tabInfo",
						type: "Select",
						inputAcept: "String"

					},
					{
						item: "bulkInputLanguP",
						column: "columnBulkInputLanguP",
						// item: this.byId("inputNameFirst"),
						tab: "tabInfo",
						type: "Select",
						inputAcept: "String",
						aceptValues: ["EN", "FR", "ES"]
					},
					// {
					// 	item: "bulkInputNationality",
					// 	column: "columnBulkInputNationality",
					// 	// item: this.byId("inputNameFirst"),
					// 	tab: "tabInfo",
					// 	type: "Select",
					// 	inputAcept: "String"
					// },
					{
						item: "bulkInputUidData",
						column: "columnBulkInputUidData",
						tab: "tabInfo",
						type: "Input",
						dependentThanTwo: {
							pathItem: "bulkInputNationality",
							value: [0, 1, 2],
							prop: "getSelectedIndex"
						},
						validateMaskFunction: ["validateCurpBulk", "validateOtherIdBulk", "validateCniBulk"]
					},
					{
						item: "bulkTypeSex",
						column: "columnBulkTypeSex",
						// item: this.byId("inputNameFirst"),
						tab: "tabInfo",
						type: "Select",
						inputAcept: "AlfaNumCode",
						// dependentThanTwo: {
						// 	pathItem: "bulkInputNationality",
						// 	value: [0, 1],
						// 	prop: "getSelectedIndex"
						// },
						aceptValues: ["1", "2"]
					},

					// {
					// 	item: "bulkInputDriverLicense",
					// 	column:"columnBulkInputDriverLicense",
					// 	// item: this.byId("inputNameFirst"),
					// 	tab: "tabInfo",
					// 	type: "Input",
					// 	inputAcept: "AlfaNumCode",
					// 	// predefinedExpReg: /^([0-9]{11})+$/g
					// }, 	

					// {

					// 	item: "bulkInputImss",
					// 	column: "columnBulkInputImss",
					// 	tab: "tabInfo",
					// 	type: "Input",
					// 	inputAcept: "Number",
					// 	defaultValidate: true,
					// 	dependentFields: {
					// 		pathItem: "columnBulkInputNationality",
					// 		value: 0,
					// 		prop: "getSelectedIndex"
					// 	},
					// 	predefinedExpReg: /^([0-9]{11})+$/g

					// },

					// {

					// 	item: "bulkInputCuil",
					// 	column: "columnBulkInputCuil",
					// 	tab: "tabInfo",
					// 	type: "Input",
					// 	inputAcept: "Number",
					// 	defaultValidate: true,
					// 	dependentFields: {
					// 		pathItem: "columnBulkInputNationality",
					// 		value: 2,
					// 		prop: "getSelectedIndex"
					// 	},
					// 	predefinedExpReg: /^([2]{1})+([0,3,4,7]{1})+([-]{1})+([0-9]{8})+([-]{1})+([0-9]{1})$/

					// },
					{
						item: "bulkInputNameFirst",
						column: "columnBulkInputNameFirst",
						// item: this.byId("inputNameFirst"),
						tab: "tabInfo",
						type: "Input",
						inputAcept: "String"
					},
					{
						item: "bulkInputNameLast",
						column: "columnBulkInputNameLast",
						// item: this.byId("inputNameLast"),
						tab: "tabInfo",
						type: "Input",
						inputAcept: "String"
					},
					// {
					// 	item: "bulkInputBirthdate",
					// 	column: "columnBulkInputBirthdate",
					// 	// item: this.byId("inputBirthdate"),
					// 	tab: "tabInfo",
					// 	type: "Date",
					// 	dependentThanTwo: {
					// 		pathItem: "columnBulkInputNationality",
					// 		value: [0, 1],
					// 		prop: "getSelectedIndex"
					// 	},
					// 	validateMaskFunction: ["validateAdult", "validateAdult"]
					// 		// predefinedExpReg: /^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/g
					// },


					// {
					// 	item: "bulkInputStreet",
					// 	column: "columnBulkInputStreet",
					// 	// item: this.byId("inputStreet"),
					// 	tab: "tabAddress",
					// 	type: "Input",
					// 	inputAcept: "AlfaNumCode"
					// }, {
					// 	item: "bulkInputHouseNum1",
					// 	column: "columnBulkInputHouseNum1",
					// 	// item: this.byId("HouseNum1"),
					// 	tab: "tabAddress",
					// 	type: "Input",
					// 	inputAcept: "AlfaNumCode"
					// },
					{
						item: "bulkInputCountry",
						column: "columnBulkInputCountry",
						// item: this.byId("inputCountry"),
						tab: "tabAddress",
						type: "Select",
						inputAcept: "AlfaNum"
					},
					// {
					// 	item: "bulkInputPostCode1",
					// 	column: "columnBulkInputPostCode1",
					// 	tab: "tabAddress",
					// 	type: "Input",
					// 	inputAcept: "AlfaNum"
					// },
					// {
					// 	item: "bulkInputEmail",
					// 	column: "columnBulkInputEmail",
					// 	tab: "tabContac",
					// 	type: "Input",
					// 	validateMaskFunction: ["validateEmail"]
					// }, {
					// 	item: "bulkInputTelNumber",
					// 	column: "columnBulkInputTelNumber",
					// 	tab: "tabContac",
					// 	type: "Input",
					// 	inputAcept: "Number",
					// 	dependentThanTwo: {
					// 		pathItem: "columnBulkInputNationality",
					// 		value: [0, 1],
					// 		prop: "getSelectedIndex"
					// 	},
					// 	// validateMaskFunction: "validateEmail"
					// },
					// {
					// 	item: "bulkInputContactName",
					// 	column: "columnBulkInputContactName",
					// 	tab: "tabContac",
					// 	type: "Input",
					// 	inputAcept: "String"
					// 		// validateMaskFunction: "validateEmail"
					// },
					// {
					// 	item: "bulkInputContactPhone",
					// 	column: "columnBulkInputContactPhone",
					// 	tab: "tabContac",
					// 	type: "Input",
					// 	inputAcept: "Number",
					// 	dependentThanTwo: {
					// 		pathItem: "columnBulkInputNationality",
					// 		value: [0, 1],
					// 		prop: "getSelectedIndex"
					// 	},
					// 	// validateMaskFunction: "validateEmail"
					// }
				]

			});
			this.setModel(oViewModel, "appView");

			fnSetAppNotBusy = function() {
				// oViewModel.setProperty("/busy", false);
				// oViewModel.setProperty("/delay", iOriginalBusyDelay);
			};

			this.getOwnerComponent().getModel().metadataLoaded()
				.then(fnSetAppNotBusy);

			// Makes sure that master view is hidden in split app
			// after a new list entry has been selected.
			oListSelector.attachListSelectionChange(function() {
				this.byId("idAppControl").hideMaster();
			}, this);

			// apply content density mode to root view
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
		}

	});

});